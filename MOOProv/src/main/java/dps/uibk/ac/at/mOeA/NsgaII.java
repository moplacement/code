/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.ac.at.mOeA;


import dps.uibk.ac.at.mutation.ProvMutationFactory;
import dps.uibk.mooprov.crossover.ProvCrossoverFactory;
import java.util.HashMap;
import jmetal.core.Algorithm;
import jmetal.core.Operator;
import jmetal.core.Problem;
import jmetal.core.SolutionSet;
import jmetal.metaheuristics.nsgaII.NSGAII;
import jmetal.operators.crossover.CrossoverFactory;
import jmetal.operators.mutation.MutationFactory;
import jmetal.operators.selection.SelectionFactory;
import jmetal.qualityIndicator.QualityIndicator;
import jmetal.util.JMException;
import org.apache.log4j.Logger;

/**
 *
 * @author ennio
 */
public class NsgaII extends MoEAlgorithm {
    
    
    private final    Problem   problem   ; // The problem to solve
    private final    Algorithm algorithm ; // The algorithm to use
    private final    Operator  crossover ; // Crossover operator
    private final    Operator  mutation  ; // Mutation operator
    private final    Operator  selection ; // Selection operator
    
    private HashMap  parameters ; // Operator parameters
    
    private final    QualityIndicator indicators ; // Object to get quality indicators
    
    private final Logger logger_=Logger.getLogger(NsgaII.class);
    
    public NsgaII(Problem P) throws JMException{
        
        
       
         indicators=null;   
         problem=P; //new MoeaProvisioning("Int",myIaas.getgUest(),myIaas.gethOst());
         
         logger_.info("Mo Problem instantiated");
         
        // Solution solution1=new Solution(problem);
       // ((MoeaProvisioning)problem).setInitialSched(myIaas.getPlacement());
        //((MoeaProvisioning)problem).setComparator(new MigrationComparator());
        
       // ((MoeaProvisioning)problem).setComparator(new MigrationEnergyComparator(myIaas,new MigrationModel()));
    // else
          logger_.info("MoE Problem currectly set up");
          
          
          algorithm = new NSGAII(problem);
          //algorithm = new MOCell(problem);

    // Algorithm parameters
          algorithm.setInputParameter("populationSize",100);
          algorithm.setInputParameter("maxEvaluations",200000);
          
         // algorithm.setInputParameter("archiveSize", 100);
 
    // Mutation and Crossover for Real codification 
          parameters = new HashMap() ;
          parameters.put("probability", 0.9) ;
         
             //parameters.put("distributionIndex", 20.0) ;
             crossover = ProvCrossoverFactory.getCrossoverOperator("SinglePointCrossoverP", parameters);
        

         parameters = new HashMap() ;
        parameters.put("probability", 1.0/problem.getNumberOfVariables()) ;
        // parameters.put("probability", 0.0001) ;
        
             //parameters.put("distributionIndex", 20.0) ;
             mutation = ProvMutationFactory.getMutationOperator("BitFlipMutationP", parameters);
         

    // Selection Operator 
        parameters = null ;
                                  
             selection = SelectionFactory.getSelectionOperator("BinaryTournament2", parameters) ;
        

    // Add the operators to the algorithm
        algorithm.addOperator("crossover",crossover);
        algorithm.addOperator("mutation",mutation);
        algorithm.addOperator("selection",selection);

    // Add the indicator object to the algorithm
        algorithm.setInputParameter("indicators", indicators) ;
    
}
    @Override
    public SolutionSet getComputedPlacements() throws JMException, ClassNotFoundException{
    
        long initTime = System.currentTimeMillis();
        SolutionSet population;
        
        
             population = algorithm.execute();
        
         
        long estimatedTime = System.currentTimeMillis() - initTime;
    
    // Result messages 
        logger_.info("Total execution time: "+estimatedTime + "ms");
        logger_.info("Variables values have been writen to file VAR");
        population.printVariablesToFile("Pareto/VAR");    
        logger_.info("Objectives values have been writen to file FUN");
        population.printObjectivesToFile("Pareto/FUN");
    
        return population;
    }
    
    
    }
