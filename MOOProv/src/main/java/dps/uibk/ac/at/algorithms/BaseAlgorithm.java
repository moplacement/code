/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.ac.at.algorithms;

import jmetal.core.Algorithm;
import jmetal.core.Problem;
import jmetal.core.SolutionSet;
import jmetal.util.JMException;

/**
 *
 * @author ennio
 */
public abstract class BaseAlgorithm extends Algorithm{

    private SolutionSet population;

    public abstract SolutionSet getPopulation();

    public abstract void setPopulation(SolutionSet population) throws JMException;
    
    
    
    public BaseAlgorithm(Problem problem) {
        super(problem);
    }

    @Override
    public SolutionSet execute() throws JMException, ClassNotFoundException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
