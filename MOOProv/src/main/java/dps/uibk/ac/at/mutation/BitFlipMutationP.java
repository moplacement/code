/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.ac.at.mutation;

import dps.uibk.mooprov.solutionType.IntSolutionTypeP;
import dps.uibk.mooprov.solutionType.Placement;
import dps.uibk.mooprov.solutionType.PlacementII;
import java.util.Arrays;
import java.util.HashMap;
import jmetal.encodings.solutionType.BinaryRealSolutionType;
import jmetal.encodings.solutionType.BinarySolutionType;
import jmetal.encodings.solutionType.IntSolutionType;
import jmetal.operators.mutation.BitFlipMutation;

/**
 *
 * @author ennio
 */
public class BitFlipMutationP extends BitFlipMutation{

    public BitFlipMutationP(HashMap<String, Object> parameters) {
        super(parameters);
        
        VALID_TYPES=Arrays.asList(BinarySolutionType.class,
  		                                            BinaryRealSolutionType.class,
  		                                            IntSolutionType.class,
                                                            Placement.class,
                                                            PlacementII.class,
                                                             IntSolutionTypeP.class);
    }
    
}
