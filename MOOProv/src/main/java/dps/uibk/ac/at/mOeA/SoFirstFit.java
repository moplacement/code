/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.ac.at.mOeA;

import dps.uibk.ac.at.algorithms.FirstFit;
import dps.uibk.mooprov.problems.MoeaProvisioning;
import jmetal.core.Algorithm;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.util.JMException;

/**
 *
 * @author ennio
 */
public class SoFirstFit extends MoEAlgorithm{

    private final    Problem   problem   ; // The problem to solve
    private final    Algorithm algorithm ; // The algorithm to use
    
    public SoFirstFit(Problem p) {
        
        this.problem=p;
        algorithm=new FirstFit(p).Sort("decreasing");
        
    }

    
    
    @Override
    public SolutionSet getComputedPlacements() throws JMException, ClassNotFoundException {
        
        SolutionSet result=algorithm.execute();
        Solution sltn=result.get(0);
       
         if(((MoeaProvisioning)problem).getInitialSched()==null){ 
        	
		((MoeaProvisioning)problem).setInitialSched(sltn);
           //TODO using an algorithm to compute an initial
           // solution the solution  evaluation gives an null pointer exception
           // because the problem has not yet a initial placement for a
           // comparison.
           //it is necessary provide a way to let the algorithm know 
           // that it is going to compute the first solution.  
	}
        
        problem.evaluate(sltn);
        problem.evaluateConstraints(sltn);
        
        return result; //To change body of generated methods, choose Tools | Templates.
    }
  
    
}
