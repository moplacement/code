/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.ac.at.Factories;

import dps.uibk.mooprov.iaas.PhysicalMachine2;
import dps.uibk.mooprov.iaas.VirtualMachine;

/**
 *
 * @author ennio
 */
public class PmFactory {
    
     public static PhysicalMachine2 Pm(String type){
    
        VirtualMachine vm;
        
        String index=getindex();
        
        switch(type){
            case "Xeon-E-2690":
                return new PhysicalMachine2("Xeon-E-2690"+index,128*Math.pow(2, 30),2,10,true,164.2,382.0);
            case "Opteron-8356":
                return new PhysicalMachine2("Opteron-8356"+index,32*Math.pow(2, 30),8,4,false,501.0,840.0);
            
                
            default: return new PhysicalMachine2();
        }
        
       
    }
    
   private static String getindex(){
        int i=5;
        String index="";
        char s[]={'a','b','2','r','5','7','g','h','i','o','a','f','4','v','2','s','t','r','v','a','e','u','o'};
        while((i--)>0)
            index+=s[(int)((Math.random()*100)%23)];
        return index;
            
    }
}
