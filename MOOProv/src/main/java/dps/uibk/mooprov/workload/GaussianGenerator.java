/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.workload;

import org.apache.commons.math3.distribution.NormalDistribution;
/**
 *
 * @author ennio
 */
public class GaussianGenerator implements Workload{
    
    private final NormalDistribution MemGenerator;
    private final NormalDistribution CPUGenerator;

    public GaussianGenerator() {
        this.MemGenerator=new NormalDistribution();
        this.CPUGenerator=new NormalDistribution();
        
        
    }
    
    /**
     * 
     * @param AvgMemDem average memory demand
     * @param MemDev    memory demand deviation
     * @param AvgCPUDem average CPU demand
     * @param CPUDev    CPU demand variation
     */
    public GaussianGenerator(double AvgMemDem,double MemDev,double AvgCPUDem,double CPUDev) {
        this.MemGenerator=new NormalDistribution(AvgMemDem, MemDev);
        this.CPUGenerator=new NormalDistribution(AvgCPUDem, CPUDev);
    }
    
    
    @Override
     public double getMemoryDemand() {
        double sample;
        do
           sample=this.MemGenerator.sample(); 
        while(sample>1||sample<0);
        return sample;
    }
    
    @Override
    public double getCPUDemand() {
        double sample;
        do
           sample=this.CPUGenerator.sample(); 
        while(sample>1||sample<0);
        return sample;
    }
}
