/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.iaas;

import dps.uibk.mooprov.workload.GaussianGenerator;
import hu.mta.sztaki.lpds.cloud.simulator.Timed;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;

import org.apache.log4j.Logger;

/**
 *
 * @author ennio
 */
public class PhysicalMachine2 extends SimulatedMachine{
    
    /*
     * Machine Name
     */
    private String index;
    
    /*
     * CPU specifications
     */
    private final double 
            cPUs,
            cores,
            vCPUs;
    
    private final boolean hyperthreading;
    /*
     * MEMORY specifications
     */
    private final double memoryCapacity;
    private final long pageSize=(long)(4*Math.pow(2,10));//4KB
    
    /*
     * Power specifications
     */
    
    private final double idlePowerConsumption;
    private final double maxPowerConsumption;
    
     /*
     * Runtime resources informations
     */
   
    private double MemoryRequested,
            MemoryUtilization,
            CPUutilization;
    private double schedulingOvh;//this is the scheduling demand, it could not correspond to the actual due to overcommitment;
    private final ArrayList<Double> migrationOverhead;
    private double vCPUsRequested;
    private final Collection <ResourcesSharingMap> SharingMap= new ArrayList();
    private GaussianGenerator sChedulerWorkload; 
    
    /*
     * VMs allocated
     */
    
    private Collection <VirtualMachine> vMs;
    
    /*
     * VMs to be migrated
     */
    
    private ArrayList <VirtualMachine> vMsTobeMigrated;
    private HashMap <VirtualMachine,PhysicalMachine2> MigrationMap;
    
   
    
    
    static Logger logger = Logger.getLogger(PhysicalMachine2.class);

    @Override
    public double sentFile(Datastore.fileDescriptor fd, SimulatedMachine source, SimulatedMachine target) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void FileSent(Datastore.fileDescriptor fd, SimulatedMachine source, SimulatedMachine target) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void FileReceived(Datastore.fileDescriptor fd, SimulatedMachine source, SimulatedMachine target) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
    public static class MigrationException extends Exception{

        public MigrationException(Throwable cause) {
            super(cause);
        }
        
         public MigrationException(String s) {
            super(s);
        }
    
    }
    
    
    
    
    private final MachineEvents updateResourcesConsumption=new MachineEvents(5) {
                
        @Override
        protected void eventAction() {
            updateMemoryTarget(); //To change body of generated methods, choose Tools | Templates.
            //System.out.println("update memory fire time ="+ Timed.getFireCount());
            
            UpdateMemoryUtilization();
            UpdateCPUUtilization();
            updateSchedulingOverhead();
        }
        
       
        
    };
    
  

       

        
   
    
     private final MachineEvents updateWorkingSet=new MachineEvents(5){

        @Override
        protected void eventAction() {
            if (!SharingMap.isEmpty()){
                for(ResourcesSharingMap map:SharingMap){ //To change body of generated methods, choose Tools | Templates.
        
                //System.out.println("event running"+ Timed.getFireCount()+getMEMUtilization(map.vm));
                map.setWorkingSet(getMEMUtilization(map.vm));
               }
            }
        
        }
     
        
     
     };

    
     private double getSchedulingOverhead() {
         
       return schedulingOvh; //To change body of generated methods, choose Tools | Templates.
    }
     
     @Deprecated
     private double getRealSchedulingOverhead() throws SimulatedMachineException{
       
         if(State.ShutDown.equals(this.state)||State.Suspended.equals(this.state)){
         
             if(schedulingOvh>0){
             
                 
                 throw new SimulatedMachineException("scheduling overhead not coherent with the machine state : "+this.state);
                 
             }
         }
         
       double maxCpuShare=Double.MAX_VALUE;
        if(schedulingOvh>0&&getvCPUsRequested()>0){
            maxCpuShare=schedulingOvh*getVCPUs()
                    /(getvCPUsRequested()+((schedulingOvh+getMigrationOverhead())*getVCPUs()));
        }
        return maxCpuShare>schedulingOvh? schedulingOvh:maxCpuShare;
       //return schedulingOvh; //To change body of generated methods, choose Tools | Templates.
    }

   

    
    public class ResourcesSharingInfo{
    
        private final double CPUshare;
        private final double Memorytarget;
        private final double WorkingSet;
        private final double MemoryShare;
        private final VirtualMachine vm;
        
       private ResourcesSharingInfo(ResourcesSharingMap m){
        
            this.CPUshare=m.CPUshare;
            this.MemoryShare=m.MemoryShare;
            this.Memorytarget=m.Memorytarget;
            this.WorkingSet=m.WorkingSet;
            this.vm=m.vm;
        }
        
        @Override
        public String toString(){
        
           return "\n Virtual machine "+vm.getIndex()+
                   "CPU share"+CPUshare+"\nMemory share"+
                   MemoryShare+"\nWorking set"+
                   WorkingSet+"\nMemory target"+Memorytarget; 
        
        }

        public double getCPUshare() {
            return CPUshare;
        }

        public double getMemorytarget() {
            return Memorytarget;
        }

        public double getWorkingSet() {
            return WorkingSet;
        }

        public double getMemoryShare() {
            return MemoryShare;
        }
        
    };
    
    private class ResourcesSharingMap{
    
        private double CPUshare=1;
        private double Memorytarget=-1;
        private double WorkingSet;
        private double MemoryShare=1;

        private VirtualMachine vm=null;

        public ResourcesSharingMap(VirtualMachine vm) {
            this.vm=vm;
            setInitialMemoryTarget();
        }
        
        
        
        public double getCPUshare() {
            return (vm!=null)?(this.CPUshare):null;
        }

        public void setCPUshare(double CPUshare) {
            this.CPUshare = CPUshare;
        }

        public double getMemorytarget() {
            return Memorytarget;
        }

        public void setMemorytarget(double Memorytarget) {
            this.Memorytarget = Memorytarget;
        }
    
         public void setWorkingSet(double WorkingSet) {
            double alpha=0.6D;
            this.WorkingSet =alpha* this.WorkingSet+(1-alpha)*WorkingSet;
        }
    
         
        private void setInitialMemoryTarget(){
        
            this.Memorytarget=(MemoryRequested>memoryCapacity)?
                    ((MemoryShare*(double)vm.getVMemory()/(double)MemoryRequested)
                    *memoryCapacity):(double)vm.getVMemory();
            
        }
    
       
    }

    //default machine constructor
    public PhysicalMachine2(String index,double MemoryCapacity,
            int CPUs,int cores,boolean hp,
            double IdlePWR,double MaxPWR) throws SimulatedMachineException{
        
        this.memoryCapacity = MemoryCapacity;
        this.cPUs = CPUs;
        this.cores=cores;
        this.hyperthreading=hp;
        vCPUs=(hyperthreading) ? (CPUs*cores*2) : (CPUs*cores);
        
        vMs= new ArrayList();
        sChedulerWorkload=new GaussianGenerator(HypervisorMemoryutilizationModel(),0.1, HypervisorCPUutilizationModel(),0.1); // default load
        migrationOverhead=new ArrayList<>(2);
        
        migrationOverhead.add(0,0D);
        migrationOverhead.add(1,0D);
        
        registerEvents(updateResourcesConsumption,updateWorkingSet);//simulation features
        this.index=index;
        this.idlePowerConsumption=IdlePWR;
        this.maxPowerConsumption=MaxPWR;
    }
    
    public PhysicalMachine2(double MemoryCapacity, int CPUs,int cores,boolean hp) throws SimulatedMachineException{
        this("",MemoryCapacity,CPUs,cores,hp,50,200);
     
        index=getindex();
    }
    
    public PhysicalMachine2() {
        this("",256*Math.pow(2,30),16,2,true,50,200);//256 GB,16CPUs,2cores,Hyperthreading
        
        index=getindex();
    }

    public PhysicalMachine2(String index, double MemoryCapacity, int CPUs,int cores,boolean hp) {
        this(index,MemoryCapacity,CPUs,cores,hp,50,200);
        
    }
    
     
    
  public double getCPUUtilization(VirtualMachine vm) {
             
        double share=(double)vm.getVCPUs()/(double)vCPUs;
        return (share < 1 ) ? share*vm.getCPUdemand() : (double)vm.getCPUdemand() ; /* the user can ask for more vCPUs than the ones  physically present on the host,
        *    but I assume this will not affect the load on the PM
        */
        
    
    }
    
    /**
     * this function is used only to get the VM cpu utilization forecast at time t+1, to get the VM demand use getCPUUtilization(VirtualMachine ?)
     * @param vm
     * @return cpu utilization forecast
     */
    public double forecastCPUUtilization(VirtualMachine vm) {
             
        double share=(double)vm.getVCPUs()/(double)vCPUs;
        return (share < 1 ) ? share*vm.forecatsCPUusage() : (double)vm.forecatsCPUusage() ; /* the user can ask for more vCPUs than the ones  physically present on the host,
        *    but I assume this will not affect the load on the PM
        */
        
    
    }
    
    
    
    /**
     *
     * @param vm VM of interest
     * @return the current CPU utilization accordingly to current PM load. 
     */
    public double getRealCPUUtilization(VirtualMachine vm) {
    
         class CheckCorrectness{
        
            public double weightCheck(double TotalAddictionalDemand){
            
                double demand=0,weight=0, vMProportionalFairShare=0;
                
                for (VirtualMachine vmTemp:vMs){
                    
                        vMProportionalFairShare=ComputeCPUshares(vmTemp);
                        demand=vMProportionalFairShare-(getCPUUtilization(vmTemp));
                         if(demand<0){
            
        
                             weight+=(0-demand)/TotalAddictionalDemand;
                         }
                }
            
                return weight;
            }
            
            public double computeOverhead(){
            
                double schedulingOvhd=getSchedulingOverhead();
                double migrationOvhd=getMigrationOverhead();
        
                
                double OverheadSharing=((schedulingOvhd+migrationOvhd)*vCPUs);
        
                OverheadSharing=OverheadSharing/(vCPUsRequested+OverheadSharing);
            
                return OverheadSharing;
            }
            
            public double checkFairness(double MaxVMsComputedUtilization){
            
                double overhead=computeOverhead();
                
                overhead+=MaxVMsComputedUtilization;
                
                return overhead;
            }
            
            
            
        }
        
      // the method starts here !!!!
         if(State.ShutDown.equals(this.state)||State.Suspended.equals(this.state))
            return 0D;
        
        double totalCpuUsage=0;
        double vMProportionalFairShare;
        double notUsed=0,addictional=0;
        double demand,myUtilization,weight=0;
       // double vMcpuSharing=0;
        
        double TotalAddictionalDemand=0;
        
        double check=0;
        
        CheckCorrectness evaluate=new CheckCorrectness();
        
        evaluate.computeOverhead();
                
         if(!isThisAllocated(vm)){
              //return -Double.MAX_VALUE;
         
              throw new SimulatedMachineException("the Vm"+ vm +"is not allocated on the host"+this);
         }
        
        for (VirtualMachine vmTemp:vMs){
            
            //vMcpuSharing=ComputeCPUshares(vmTemp);
            
           // vMProportionalFairShare=vMcpuSharing-(OverheadSharing*(vMcpuSharing));
            //share settings, implements the proportional fair share
            //determining the share of the CPU according to the share parameter (see XEN credit scheduler)  
            vMProportionalFairShare=ComputeCPUshares(vmTemp);
            
            check+=vMProportionalFairShare;
            demand=vMProportionalFairShare-(this.getCPUUtilization(vmTemp));
            totalCpuUsage+=(this.getCPUUtilization(vmTemp));
            
            if(demand<0){
            
                //vCpuStarve++;
                TotalAddictionalDemand+=(-1)*demand;
            }
             
            else{
                notUsed+=(demand);
            }
            
        }
        
        //vMcpuSharing=ComputeCPUshares(vm);
        
        //vMProportionalFairShare=vMcpuSharing-(OverheadSharing*(vMcpuSharing));
        vMProportionalFairShare=ComputeCPUshares(vm);
        demand=vMProportionalFairShare-(this.getCPUUtilization(vm));
        
        //totalCpuUsage+=OverheadSharing;//scheduling overhead has to be considered
        
        
        if(demand<0){
            
        weight=(0-demand)/TotalAddictionalDemand;
        
        addictional=weight*notUsed;
      
        myUtilization=demand+addictional;
        
            if(myUtilization>=0){
        
                myUtilization=(this.getCPUUtilization(vm));        
             }
            else{
                     
                myUtilization=vMProportionalFairShare+addictional;
        
                }
            
        }
        else{
            
            myUtilization=this.getCPUUtilization(vm);
        
        }
        
        if (logger.isDebugEnabled()){
        logger.debug("\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n"
                + "Debug info for "+vm
                + "\n ******************************************************\n"+
                "CPUs share info->  "+vMProportionalFairShare+" n "+check+ "\n"+
                "Host scheduling ovh->  "+evaluate.computeOverhead()+"\n"+
                "total Host CPU utilization->  "+totalCpuUsage+"\n"+
                "Exceding Host CPUs demand (%) ->  "+TotalAddictionalDemand+"\n"+
                "not used Host CPU (%)->  "+notUsed+"\n"+
                "Host demand->  "+getCPUUtilization(vm)+"\n"+
                "Vm real utilization(%)->  "+myUtilization+"\n"+
                "Vm share->  "+vMProportionalFairShare+"\n"+
                "Vm weight->  "+weight+"\n"+
                "total weight info->  "+evaluate.weightCheck(TotalAddictionalDemand)+ "\n"+
                "Vm addictional->  "+addictional+
                "\n******************************************************************\n"+
                "\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n"
        
        
        );
        
        }
       
        
         
        //return (totalCpuUsage>1)?(share*this.getCPUUtilization(vm))/totalCpuUsage:getCPUUtilization(vm);
        check=evaluate.checkFairness(check);
       
        if(check <= 0.9990 || check > 1.00001){
            logger.error("the real utilization method does not provide correct results"
                    + " (the check value is "+check+" should be 0.99< check >1.00001)");
        }
        
        return myUtilization;
        /* proportional fair scheduler (somehow)
        */
        
    
    }
    
    private void updateMemoryTarget(){
    
        if (getMemoryRequested()<=getMemoryCapacity()){
           // change the memory target only if there is overcommitment
            return;
        }
        
      // System.out.println("\n\nEEEEEEEEEEEEEEEE ="+getMemoryRequested()+"\n\n");
        
        double totalMemoryUsage = 0,allocMem
                ,avgMemDem,updateMem
                ,myNewTarget,share;
        int vIndex=0;
        boolean update=false;
        
        double[] newComputedTarget=new double[vMs.size()];
        
        for (VirtualMachine vmTemp:vMs){
        
            share=getVMResourcesSharing(vmTemp).MemoryShare;
            allocMem=getVMResourcesSharing(vmTemp).Memorytarget/memoryCapacity;
            avgMemDem=getVMResourcesSharing(vmTemp).WorkingSet;
            
            updateMem=allocMem-avgMemDem;
          //  System.out.println("allocmem "+allocMem+"avgmemdem "+avgMemDem);
            
            if(updateMem<0 && !update ){
            
                update=true;
                logger.debug("update "+vmTemp.getIndex()+"memory required "+updateMem);
            }
                
            newComputedTarget[vIndex]=avgMemDem-allocMem;
            totalMemoryUsage+=(share*(avgMemDem-allocMem)*(memoryCapacity)+getVMResourcesSharing(vmTemp).Memorytarget);
            vIndex+=1; 
           
        }
        
        if(!update)//there is no need to update the VM memorytarget 
            return;
        
        if(logger.isDebugEnabled()){
        String array=this.index+"Memory demand[ ";
            for(double e:newComputedTarget)
                array+="|"+e;
            logger.debug(array+" ]");
       } 
        
        vIndex=0;
        for  (VirtualMachine vmTemp:vMs){
            
            share=getVMResourcesSharing(vmTemp).MemoryShare;
            
            myNewTarget=((newComputedTarget[vIndex]*share*(memoryCapacity)+
                    (getVMResourcesSharing(vmTemp).Memorytarget))
                    /totalMemoryUsage)*memoryCapacity;
            
            getVMResourcesSharing(vmTemp).setMemorytarget(myNewTarget);
            vIndex+=1;
        }    
    }
    
    public double getMEMUtilization(VirtualMachine vm){
        
        
        
        double share=vm.getVMemory()/memoryCapacity;
        return share*vm.getMEMdemand();
    }
    
    
    
    /**
     * this function is used only to get the VM memory utilization forecast at time t+1, to get the VM demand use getmemoryUtilization(VirtualMachine ?)
     * @param vm
     * @return memory utilization forecast
     */
    
    public double forecastMEMUtilization(VirtualMachine vm){
        
        
        
        double share=vm.getVMemory()/memoryCapacity;
        return share*vm.forecastMemusage();
    }
    
    
    
    
    /**
     *
     * @param vm VM of interest
     * @return the current memory allocated to the VM accordingly to current PM memory load. 
     */
      public double getRealMEMUtilization(VirtualMachine vm){
    
          if(!isThisAllocated(vm)){
              //return -Double.MAX_VALUE;
         
              throw new SimulatedMachineException("the Vm"+ vm +"is not allocated on the host"+this);
         }
          
           if(State.ShutDown.equals(this.state)||State.Suspended.equals(this.state))
            return 0D;
          
        double utilization=getMEMUtilization(vm);
       getVMResourcesSharing(vm).setWorkingSet(utilization);
        
        double maxUtilization=getVMResourcesSharing(vm).Memorytarget/memoryCapacity;
        
        //System.out.println("dsdsds"+maxUtilization+"sasa"+utilization);
        
        return (utilization>maxUtilization)?maxUtilization:utilization;
    }
    
      
      
      
      
      
    @Override
    public String toString(){
        
        String out= 
                     " PM info -  "+this.index+"\n"
                    +" Physical CPUs - "+ this.cPUs+"\n" 
                    +" Virtual CPUs - " + this.vCPUs+"\n"
                    +" Physical Memory - "+ this.memoryCapacity+"\n";
        
        
        return out;
    
    }
    
    
    
    
    
    
    private String getindex(){
        int i=7;
        String index="";
        char s[]={'a','b','2','r','5','7','g','h','i','o','a','f','4','v','2','s','t','r','v','a','e','u','o'};
        while((i--)>0)
            index+=s[(int)((Math.random()*100)%23)];
        return index;
            
    }
    
    
    
    
    
    
     public double getMemoryCapacity() {
        return memoryCapacity;
    }
     
     
     
     
     
     

    public double getVCPUs() {
        return vCPUs;
    }
    
    
    
    
    
    
    public double getSchedulingOverhead(int vCpus){ //used to predict the possible scheduling overload
        
        
        
        double PinnedCPU=2;
        double sharing=vCpus/vCPUs;
        double share=PinnedCPU/vCPUs; 
        double virtCPUWorkload=(1/(1+Math.exp(-((2*sharing)-2))));//return the supposed average CPU utilization 
        //return (1/(1+Math.exp(-((2*share)-2))));
        return share*virtCPUWorkload;
    }
    
    
    
    
    
    
     private void updateSchedulingOverhead(){//used to determine the runtime scheduling overload
        
        if(State.ShutDown.equals(this.state)||State.Suspended.equals(this.state)){
            
            this.schedulingOvh=0;
            return ;
        
        
        
        }
        double PinnedCPU=2;
        double share=PinnedCPU/vCPUs;
        
        //return (1/(1+Math.exp(-((2*share)-2))));
        this.schedulingOvh=share*sChedulerWorkload.getCPUDemand();
    }
    
     
     
     
     
    private ResourcesSharingMap getVMResourcesSharing(VirtualMachine vm){
    
        for (ResourcesSharingMap sm:SharingMap){
            
             if(sm.vm==vm)
                 return sm;        
        }
      return null;
    }
    
    
    
    
    
    
     protected void removeVMResourcesSharing(VirtualMachine vm){
    
        for (ResourcesSharingMap sm:SharingMap){
            
             if(sm.vm==vm){
                 SharingMap.remove(sm);
             
                 break;
             }        
        }
    }
     
     
     
     
     
     
     
     
    
    /**
     * allocateVMs function allocates the vMs on the current host updating all the data structures of the PM accordingly to the
     * new VMs are going to be allocated.
     * Note: the allocation of the new VMs does not account the VMs are currently allocated on the host. When new VMs has to be added to ones allocated it is necessary providing a list that merge the two set of VMs.    
     * 
     * @param vMs
     */
    public void allocateVMs(Collection<VirtualMachine> vMs){

        this.vMs=vMs;
        SharingMap.clear();
        vCPUsRequested=0l;
        MemoryRequested=0l;
        
        double virtCPUWorkload,virtMEMWorkload;
        
        for  (VirtualMachine vmTemp:vMs){
            vCPUsRequested+=vmTemp.getVCPUs();
            MemoryRequested+=vmTemp.getVMemory();
        
        } 
      
        
        
        for  (VirtualMachine vmTemp:vMs){
        
            
            SharingMap.add(new ResourcesSharingMap(vmTemp));//TODO verify its existence in the list ;
            
            getVMResourcesSharing(vmTemp).setWorkingSet(vmTemp.getMEMdemand());
            
            vmTemp.setMyHost(this);
        } 
        // TO DO //used to determine the runtime scheduling overload
        virtCPUWorkload=HypervisorCPUutilizationModel();
        virtMEMWorkload=HypervisorMemoryutilizationModel();
        
        sChedulerWorkload=new GaussianGenerator(virtMEMWorkload,0.1, virtCPUWorkload,0.1);
        
    
        
}
    
    
    
    
    public Collection<VirtualMachine> getAllocatedVMs(){
    
            return vMs;
    }
    
    
    
    
    
    public int getNumberofAllocatedVms(){
    
        return vMs.size();
    
    }
 
    
    
    
    
     public double getMemoryRequested() {
        return MemoryRequested;
    }

     
     
     
     
    public void setMemoryRequested(double MemoryRequested) {
        this.MemoryRequested = MemoryRequested;
    }

    
    
    
    
    public double getvCPUsRequested() {
        return vCPUsRequested;
    }
    
    
    
    
    
    
    public void setVMResourceSharing(VirtualMachine vm,double ... shares ) throws Exception{
    
        for (ResourcesSharingMap m:SharingMap){

            if(m.vm==vm){
            
                m.CPUshare=shares[0];
                m.MemoryShare=shares[1];
            
                return;
            }
        
           
        }
    
         throw new Exception("It is not possible to set the resource sharing,"
                    + " vm-"+vm.getIndex()+
                    "does not exist or it is not allocated on this PM");
    }
    
    
    
    
    
    
    
     public ResourcesSharingInfo getVMResourceSharing(VirtualMachine vm) throws Exception{
    
        
        for (ResourcesSharingMap m:SharingMap){

            if(m.vm==vm){
                     
                return new ResourcesSharingInfo(m) ;
            }
        
        }
    
        throw new Exception("It is not possible to get the resource sharing,"
                    + " vm-"+vm.getIndex()+
                    "does not exist or it is not allocated on this PM");
    }
   
     public String getIndex(){
     
         return index;
     }
     
     /* 
     *
     *
   */
    
     
     private double ComputeCPUshares(VirtualMachine vm){
     
         double share;
         double totalvCPUDemand=0;//getvCPUsRequested();
         /*
         if(CPUutilization>0.95){//stupid check, it should be removed ,p.s it is important for considering different cpu shares
          for (VirtualMachine vmTemp:vMs){
          
               share=getVMResourcesSharing(vmTemp).CPUshare;
               totalvCPUDemand+=share*vmTemp.getVCPUs();
          }
          
          if(totalvCPUDemand!=getvCPUsRequested()){
              System.err.println("totalvCPUDemand!=getvCPUsRequested"+totalvCPUDemand+" - "+getvCPUsRequested());
             // throw new UnsupportedOperationException("totalvCPUDemand!=getvCPUsRequested"+totalvCPUDemand+" - "+getvCPUsRequested());
         
              
          }
         
         }*/
         
         double OverheadSharing=(this.getSchedulingOverhead()+this.getMigrationOverhead())*vCPUs;
        
         totalvCPUDemand=getvCPUsRequested()+OverheadSharing;
         share=getVMResourcesSharing(vm).CPUshare;
          
          return (share*(vm.getVCPUs()))/(totalvCPUDemand);
     }
     
     
     
     
     
     
     
    @Override
     public void Shutdown()throws SimulatedMachineException{
         
        try{
         for(VirtualMachine vm:vMs){
            if(!vm.state.equals(State.Migration))
             vm.Shutdown();
         }
         
        for (MachineEvents e:this.getEventQueue()){
         
            if(e.getEventID().equals("MigrationDaemon")){
                this.state=State.Shutdowning;
                return;
            } 
             
         }
             
                this.getAllocatedVMs().clear();
                setMigrationOverhead(0D);
                UploadMigrationOverhead(0D);
                this.vMsTobeMigrated=null;
                this.MigrationMap=null;
                this.SharingMap.clear();
                
                schedulingOvh=0;
                MemoryRequested=vCPUsRequested=0;
                CPUutilization=MemoryUtilization=0D;
             
         
                this.deRegisterEvents();
                
                if(!this.unsubscribe()){
                
                   throw new SimulatedMachineException("The shutdown of the host" + this+"failed"); 
                
                }
                this.state= State.ShutDown;
         
         
          }
        catch(Exception e){
           throw new SimulatedMachineException(e);}
     }
     
     
     
     
     
     
     
     
    @Override
      public void Suspend()throws SimulatedMachineException{
      // try{
         for(VirtualMachine vm:vMs){
            if(!vm.state.equals(State.Migration))
             vm.Suspend();
         }
         
         /*for (MachineEvents e:this.getEventQueue()){
         
            if(e.getEventID().equals("MigrationDaemon")){
             
                 this.state=State.Suspension;
                 return;
             }
            }*/
         if(IsMigrationProcessAlive()){
         
             this.state=State.Suspension;
                 return;
         
         }
              if(!this.unsubscribe()){
                
                   throw new SimulatedMachineException("The suspension of the host" + this+"failed"); 
                
                }
             
             this.state= State.Suspended;
             this.SharingMap.clear();
             this.vMsTobeMigrated=null;
             this.MigrationMap=null;
             MemoryRequested=vCPUsRequested=0;
             sChedulerWorkload=null;
             setMigrationOverhead(0D);
             UploadMigrationOverhead(0D);
             schedulingOvh=0;
             this.CPUutilization=0D;
             this.MemoryUtilization=0D;
             //this.deRegisterEvents();
             //this.unsubscribe();
          
       //}catch(Exception e){
       //    throw new SimulatedMachineException(e);}
     }
      
      
      
      
      
      

    @Override
     public void run() throws SimulatedMachineException{
        
         if(!this.isSubscribed()&&!this.getEventQueue().isEmpty()){ //suspend
             
             for(VirtualMachine vm:vMs){
                 
            if(vm.state.equals(State.Suspended))
                 vm.run();
            
            else
                throw new SimulatedMachineException(new Exception(vm+"was not correctly suspended and cannot be correctly runedd again"));
            }
             
             allocateVMs(vMs);
             
             //this.subscribe(1);
             
             if(!this.subscribe(1)){
                
                   throw new SimulatedMachineException("host" + this+"activation failed"); 
                
                }
            //registerEvents(updateMemory,updateWorkingSet);
         }
         
         else if(!this.isSubscribed()&&this.getEventQueue().isEmpty()){ //shutdown
         
             allocateVMs(vMs);
             if(!this.subscribe(1)){
                
                   throw new SimulatedMachineException("host" + this+"activation failed"); 
                
                }
             registerEvents(updateResourcesConsumption,updateWorkingSet);
         }
      
         else throw new SimulatedMachineException(new Exception("the machine cannot be runned because is already"
                 + " running or it has not correctly stopped"));
     
          this.state= State.Running;
     
     }
     
     
     
     
     
     public boolean isRunning()throws SimulatedMachineException{
         if(State.Running.equals(this.state)!=this.isSubscribed()){
              
               if(!(State.Shutdowning.equals(this.state)||State.Suspension.equals(this.state))){
              
                   throw new SimulatedMachineException(new Exception("the machine:"+this+" is running (isSuscribed="+isSubscribed()+") but its state:"+state+" is wrong"));
               }
         }
         return this.isSubscribed(); 
     }

     
     
     
     
     
     public boolean isThisAllocated(VirtualMachine vm ){
     
           return vMs.contains(vm);
     
     }
     
     
     
     
     
     
     
     public void migrateVm(VirtualMachine vm,PhysicalMachine2 targetHost){
        try{
        
        
            vMs.remove(vm);
            removeVMResourcesSharing(vm);
            MemoryRequested-=vm.getVMemory();
            vCPUsRequested-=vm.getVCPUs();
            logger.debug("Vm"+vm.getIndex()+"has been migrated on host"+targetHost.index);
            
            targetHost.allocateVMs(Collections.singletonList(vm));
        
        }catch(Exception e){
           throw new SimulatedMachineException(e);}
     
     
     }
     
     
     
     
     
     
     
     
     public void migrateVms(HashMap<VirtualMachine,PhysicalMachine2> migrationmap) throws MigrationException{
        
         if(!(MigrationMap==null)){
                
                if(!(MigrationMap.isEmpty())){
                    //System.out.println("why you are not working");
                
                    throw new MigrationException("two simultaneous migration processes are not supported yet");
                }                
            }
         
         try{
           // System.out.println("migration of ");
            
           // System.out.println("migration of ");
            MigrationMap=migrationmap;
            vMsTobeMigrated=new ArrayList(MigrationMap.keySet());
            for(VirtualMachine vm:vMsTobeMigrated){
            
                vm.state=State.Migration;
            //    System.out.println("migration of "+vm);
            
            
            }
            
            
        registerEvents(new MachineEvents(5) {
        
        private final int NmaxRounds=10;
        private int Nrounds=0;
        private final double txRateMax=10E8,txRateMin=3E8,deltaTx=50E6;
        private double txRate=txRateMin;
        private long prevTime,succTime;
       
        
                @Override
                public String getEventID() {
                    return "MigrationDaemon"; //To change body of generated methods, choose Tools | Templates.
                }
        
        
        @Override
        protected void eventAction() {
           // updateMemoryTarget(); //To change body of generated methods, choose Tools | Templates.
            //System.out.println("update memory fire time ="+ Timed.getFireCount());
            
            double MinTime=1;//(sec)
            double memoryTxRate;
            VirtualMachine vmMigrating=null;
            
            if(getvMsTobeMigrated()==null){
                System.out.println("nothing to migrate");
                return;
                
            }
            if(!getvMsTobeMigrated().isEmpty()){
            
                vmMigrating=getvMsTobeMigrated().get(0);
                
                if(vmMigrating.getMigrationState().equals(VirtualMachine.MigrationState.none)
                        &&Nrounds>0){
                
                    Nrounds=0;
                }
                
            }else{
            
                succTime=Nrounds=-1;//TODO means that the process has been termined by outside.
            
            }
           
            
            prevTime=succTime;
            System.out.println("round =" +Nrounds+"new prev time="+prevTime);
            txRate=txRate+(Nrounds*deltaTx);
            
             if((Nrounds==0)
                     &&
                   vmMigrating.getMigrationState().equals(VirtualMachine.MigrationState.none)){
                 
                 System.out.println("InitialPhase");
                
                 vmMigrating.setMigrationState(VirtualMachine.MigrationState.initial);
                 //0.07
                 
                 setMigrationOverhead(0.2*(vmMigrating.getVMemory()/memoryCapacity));
                 MigrationMap.get(vmMigrating).UploadMigrationOverhead(0.2*(vmMigrating.getVMemory()/memoryCapacity));
                 
                 
                 succTime= Math.round((vmMigrating.getMEMdemand()*vmMigrating.getVMemory()*8)/txRate);
                 // System.out.println(succTime);
                 succTime=((succTime>0)?succTime:1);
                 // System.out.println(succTime);
                 System.out.println(migrationOverhead);
                 Nrounds=Nrounds+1;
                 this.updateFrequency(succTime);
                 return;
             }
            
            
              if((Nrounds>=1&&Nrounds<NmaxRounds&&succTime>MinTime)
                      &&
                  (vmMigrating.getMigrationState().equals(VirtualMachine.MigrationState.trasferring)
                      ||
                  vmMigrating.getMigrationState().equals(VirtualMachine.MigrationState.initial))){
                      
                  System.out.println("precopy phase");
                 
                  vmMigrating.setMigrationState(VirtualMachine.MigrationState.trasferring);
                  succTime=Math.round((preCopy(vmMigrating,prevTime)*8)/txRate);
                  succTime=((succTime>0)?succTime:1);
                  //migrationOverhead=0.09*(vmMigrating.getVMemory()/memoryCapacity);
                  setMigrationOverhead(0.2*(vmMigrating.getVMemory()/memoryCapacity));
                  MigrationMap.get(vmMigrating).UploadMigrationOverhead(0.2*(vmMigrating.getVMemory()/memoryCapacity));
                  
                  Nrounds=Nrounds+1;
                  this.updateFrequency(succTime);
                  return;
              }               
              
             if(((Nrounds==NmaxRounds)||(succTime==MinTime))
                     &&
                  ( vmMigrating.getMigrationState().equals(VirtualMachine.MigrationState.trasferring)
                     || vmMigrating.getMigrationState().equals(VirtualMachine.MigrationState.initial))){
                 
                 System.out.println("stopandcopy phase");
               
                 vmMigrating.setMigrationState(VirtualMachine.MigrationState.stopAndCopy);
                 succTime=Math.round((long)((preCopy(vmMigrating,prevTime)*8)/txRateMax));
                 succTime=((succTime>0)?succTime:1); //required for a correct simulation behaviour 
                 // migrationOverhead=0.12*(vmMigrating.getVMemory()/memoryCapacity);
                 
                 setMigrationOverhead(0.09*(vmMigrating.getVMemory()/memoryCapacity));
                 MigrationMap.get(vmMigrating).UploadMigrationOverhead(0.2*(vmMigrating.getVMemory()/memoryCapacity));
                  
                 Nrounds=NmaxRounds+1;
                 //Nrounds=Nrounds+1;
                 this.updateFrequency(succTime);
                 return;
             }
           
            
           if ((Nrounds>NmaxRounds)
                   &&
                   vmMigrating.getMigrationState().equals(VirtualMachine.MigrationState.stopAndCopy)){
                System.out.println("activation phase");
               
                 vmMigrating.setMigrationState(VirtualMachine.MigrationState.activation);
                Activation(vmMigrating,MigrationMap.get(vmMigrating));
                Nrounds=0;
                txRate=txRateMin;
              //  succTime=1;
              //  this.updateFrequency(succTime);
                
            }
            
           if(vmMigrating!=null){
           
                System.out.println(vmMigrating.getMigrationState());
                
               
           }
            System.out.println("migration last iter "+vmMigrating+"nu,mbero of round"+Nrounds);
           // System.out.println("new trasferring time="+succTime);
            
//             System.out.println(vmMigrating.getMigrationState());
                
              //  Nrounds=Nrounds+1;
               // 
                 System.out.println("new trasferring time="+(succTime+Timed.getFireCount()));
                
                
            
            
            if(getvMsTobeMigrated().isEmpty()){
             System.out.println("Migration process has been terminated");
                Nrounds=-1;
                deRegisterEvent(this);
                
                if(state.equals(State.Suspension)){
                    System.out.println("i want to suspend the machine"+PhysicalMachine2.this);
                    Suspend();
                    if(PhysicalMachine2.this.isSubscribed()){
                    
                        throw new SimulatedMachineException("suspension due to migration failed");
                    }
                }
                
                if(state.equals(State.Shutdowning)){
                     System.out.println("i want to stop the machines"+this);
                    Shutdown();
                }
            
            }
        }
       }         
            );
           /*
            for(VirtualMachine vm:vms){
            vMs.remove(vm);
            removeVMResourcesSharing(vm);
            MemoryRequested-=vm.getVMemory();
            vCPUsRequested-=vm.getVCPUs();
            logger.debug("Vm"+vm.getIndex()+"has been migrated on host"+targetHost.index);
            
            }
            
            targetHost.allocateVMs(vms);
        */
            
            
        }catch(Exception e){
           throw new SimulatedMachineException(e);}
     
     
     }
     
     
     
     
     
     
     public void deAllocateVms(Collection<VirtualMachine> vMs){
     
        
        
        for  (VirtualMachine vmTemp:vMs){
            
            if(this.isThisAllocated(vmTemp)){
                
            
                vCPUsRequested-=vmTemp.getVCPUs();
            
                MemoryRequested-=vmTemp.getVMemory();
            
                removeVMResourcesSharing(vmTemp);
            
            if(this.vMsTobeMigrated!=null){
                if(this.vMsTobeMigrated.contains(vmTemp)){
                
                    this.MigrationMap.remove(vmTemp);
                    this.vMsTobeMigrated.remove(vmTemp);
                    
                   // this.migrationOverhead=0D;
                    setMigrationOverhead(0D);
                    UploadMigrationOverhead(0D);
                }
                
                if(this.vMsTobeMigrated.isEmpty()&&IsMigrationProcessAlive()){//the migration daemon should be stopped
                
                    //TODO
           
                }
                
             }   
                
               // vmTemp.Shutdown();
            }
            else 
            {
                throw new SimulatedMachineException("Deallocation of VM:"+vmTemp+"is impossible: the machine is not running on "+this);
            
            }
            
            this.vMs.removeAll(vMs);
             
            double virtCPUWorkload=HypervisorCPUutilizationModel();
            double virtMEMWorkload=HypervisorMemoryutilizationModel();
        
            sChedulerWorkload=new GaussianGenerator(virtMEMWorkload,0.1, virtCPUWorkload,0.1);
            
        } 
       
        
        }
     
     
     
     
     
     
     private double preCopy(VirtualMachine vm,double time){
     
         double currentMemoryUtilization=vm.getMEMdemand()*vm.getVMemory();
         double memorydirtied=(vm.getDirtingrate()*time)/8D;
         
         return memorydirtied>currentMemoryUtilization?currentMemoryUtilization:memorydirtied;
     
     }
     
     
     
     
      private ArrayList <VirtualMachine> getvMsTobeMigrated() {
        return vMsTobeMigrated;
    }

      
      
      
    private void setvMsTobeMigrated(ArrayList <VirtualMachine> vMsTobeMigrated) {
        this.vMsTobeMigrated = vMsTobeMigrated;
    }
    
    
    
    
    
     public HashMap<VirtualMachine, PhysicalMachine2> getMigrationMap() {
        return MigrationMap;
    }

     
     
     
     
    public void setMigrationMap(HashMap<VirtualMachine, PhysicalMachine2> MigrationMap) {
        this.MigrationMap = MigrationMap;
    }
    
    
    
    
    
    
    
    private void Activation(VirtualMachine vm,PhysicalMachine2 pm){
        
        try{
        
        
            //vMs.remove(vm);
           // removeVMResourcesSharing(vm);
           // MemoryRequested-=vm.getVMemory();
           // vCPUsRequested-=vm.getVCPUs();
            logger.debug("Vm"+vm.getIndex()+"has been migrated from host :"+this.index+"on host"+pm.index);
           
            this.deAllocateVms(Collections.singletonList(vm));
            
           // MigrationMap.get(vm).allocateVMs(Collections.singletonList(vm));
            pm.vMs.add(vm); //add a VM to the list of already allocated VMs
            pm.allocateVMs(pm.vMs);
            //pm.setMigrationOverhead();//TODO 
            //vm.setMyHost(pm); not required
            
          //  MigrationMap.remove(vm);
          //  vMsTobeMigrated.remove(vm);
          //  migrationOverhead=0;
           // pm.setMigrationOverhead(0.2*(vm.getVMemory()/pm.memoryCapacity));
            
            vm.state=State.Running;
            vm.setMigrationState(VirtualMachine.MigrationState.none);
            
            pm.UploadMigrationOverhead(0D);
          //  double virtCPUWorkload=HypervisorCPUutilizationModel();
          //  double virtMEMWorkload=HypervisorMemoryutilizationModel();
        
         //   sChedulerWorkload=new GaussianGenerator(virtMEMWorkload,0.1, virtCPUWorkload,0.1);
            
        }catch(Exception e){
           throw new SimulatedMachineException(e);}
     
        
    
    
    }

    
    
    
    
    
    @Override
    protected boolean subscribe(long freq) {
        System.out.println(this+"is going to be subscribed at time"+Timed.getFireCount());
        return super.subscribe(freq); //To change body of generated methods, choose Tools | Templates.
    
    }
    
    
    
    
    
    
    
    private void UpdateMemoryUtilization(){
    
         double totalMem=0;
         for (VirtualMachine vTemp : getAllocatedVMs()) {
                    
                        totalMem+=getRealMEMUtilization(vTemp);
                    }
                    
                    //totalCpu+=getSchedulingOverhead()+migrationOverhead;
        this.MemoryUtilization=totalMem;
    
    }
    
    
    
    
    
    
     private void UpdateCPUUtilization(){
    
         double totalCpu=0;
         for (VirtualMachine vTemp : getAllocatedVMs()) {
                    
                        totalCpu+=getRealCPUUtilization(vTemp);
                        
                    }
            
                   // totalCpu+=(getRealSchedulingOverhead()+getRealMigrationOverhead());//*(vCPUs/getvCPUsRequested());
                    
                    totalCpu+=getMigrationOverhead()+getSchedulingOverhead();
                    
                    
          /*   if(totalCpu > 1.0000002){
                    logger.error("the cpu utilization is not correct"
                    + " ( value is "+totalCpu+" should be < 1.005)");
                    
             }*/
             
            this.CPUutilization=Math.min(1D,totalCpu);
    }
    
     
     
     
     
     
    
    public double getOverallMachineCPUUtilization(){
    
        
        return this.CPUutilization;
    }
    
    
    
    public double getOverallMachineMemoryUtilization(){
        
    
        return this.MemoryUtilization;
    }
    
    
    
    
    
    private double HypervisorCPUutilizationModel(){
    
        return (1/(1+Math.exp(-((2*vCPUsRequested/vCPUs)-2))));
    
    }
    
    
    
  
    private double HypervisorMemoryutilizationModel(){
    
        return 0D;
    }
    
    
    
    
    public State getMachineState(){
    
        return state;
    }
    
    
    
    
    
    public double getIdlePWR(){
    
        return this.idlePowerConsumption;
    
    }
    
    
    
     public double getMaxPWR(){
    
        return this.maxPowerConsumption;
    
    }
     
     
     
     
     
    private double getMigrationOverhead() {
        return migrationOverhead.get(0)
                +migrationOverhead.get(1);
    }
    
    
    
    
    
    @Deprecated
    private double getRealMigrationOverhead() throws SimulatedMachineException{
        
         if(State.ShutDown.equals(this.state)||State.Suspended.equals(this.state)){
         
             if(getMigrationOverhead()>0){
                
                 throw new SimulatedMachineException("migration overhead not coherent with the machine state : "+this.state);
                 
             }
         }
            
        double maxCpuShare=Double.MAX_VALUE;
        if(getMigrationOverhead()>0){
            maxCpuShare=(getMigrationOverhead()*getVCPUs())
                     /(getvCPUsRequested()+((schedulingOvh+getMigrationOverhead())*getVCPUs()));
        }
        return maxCpuShare>getMigrationOverhead() ? getMigrationOverhead():maxCpuShare;
        
       
    }
    
    
    
    

    private void setMigrationOverhead(double migrationOverhead) {
        this.migrationOverhead.set(0, migrationOverhead);
    }
    
    @Deprecated
    public void UploadMigrationOverhead(double ovh) {
        this.migrationOverhead.set(1, ovh);
    }
    
    public boolean IsMigrationProcessAlive(){
        
        for (MachineEvents e:this.getEventQueue()){
         
            if(e.getEventID().equals("MigrationDaemon")){
             
                 
                 return true;
             }
            }
    
        return false;
    }
}


