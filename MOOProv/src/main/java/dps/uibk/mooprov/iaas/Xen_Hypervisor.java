/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.iaas;

import static dps.uibk.mooprov.iaas.PhysicalMachine2.logger;
import dps.uibk.mooprov.iaas.SimulatedMachine.MachineEvents;
import dps.uibk.mooprov.iaas.SimulatedMachine.SimulatedMachineException;
import dps.uibk.mooprov.iaas.SimulatedMachine.State;
import dps.uibk.mooprov.workload.GaussianGenerator;
import dps.uibk.mooprov.workload.Workload;
import hu.mta.sztaki.lpds.cloud.simulator.Timed;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author ennio
 */
public abstract class Xen_Hypervisor extends Hypervisor{

    public Xen_Hypervisor() {
        
//        description.put("sharingMap", new ArrayList<ResourcesSharingMap>());
        description.put("VMs", new ArrayList<VirtualMachine>());
        description.put("vMsTobeMigrated", null);
        description.put("MigrationMap", null);
        description.put("memoryRequested", 0);
        description.put("vCPUsRequested", 0);
        description.put("memoryUtilization", 0);
        description.put("CPUsUtilization", 0);
        
    }
    
    
    

    @Override
    public void allocateVMs(Collection<VirtualMachine> vMs) {
        setAllocatedVMs((ArrayList)vMs);
     //   getResourcesSharingMap().clear();
        setvCPUsRequested(0);
        setMemoryRequested(0l);
        
        
        
        for  (VirtualMachine vmTemp:vMs){
            
            if(getvMsTobeMigrated().contains(vmTemp)){
            
                throw new SimulatedMachineException("the Vm"+ vmTemp +"you are"
                        + " trying to allocate is migrating to another host"+this);
            
            }
            setvCPUsRequested(getvCPUsRequested()+vmTemp.getVCPUs());
            setMemoryRequested(getMemoryRequested()+vmTemp.getVMemory());
        
        } 

        for  (VirtualMachine vmTemp:vMs){
        
            
             getResourcesSharingMap().add(new ResourcesSharingMap(vmTemp));//TODO verify its existence in the list ;
            
            getVMResourcesSharing(vmTemp).setWorkingSet(vmTemp.getMEMdemand());
            
           // vmTemp.setMyHost(getHost());
        } 
    }

    
    
    @Override
    public Collection<VirtualMachine> getAllocatedVMs() {
        return (ArrayList<VirtualMachine>) description.get("VMs"); //To change body of generated methods, choose Tools | Templates.
    }
    
    private void setAllocatedVMs(ArrayList<VirtualMachine> vmList) {
        description.put("VMs", vmList);
    }

    @Override
    public int getNumberofAllocatedVms() {
        return ((ArrayList)description.get("VMs")).size();
    }

    @Override
    public double getRealMEMUtilization(VirtualMachine vm) {
         if(!isThisAllocated(vm)){
              //return -Double.MAX_VALUE;
         
              throw new SimulatedMachineException("the Vm"+ vm +"is not allocated on the host"+this);
         }
          
           if(State.ShutDown.equals(getHost().state)||State.Suspended.equals(getHost().state))
            return 0D;
          
        double utilization=getMEMUtilization(vm);
       getVMResourcesSharing(vm).setWorkingSet(utilization);
        
        double maxUtilization=getVMResourcesSharing(vm).Memorytarget/getMemoryCapacity();
        
        //System.out.println("dsdsds"+maxUtilization+"sasa"+utilization);
        
        return (utilization>maxUtilization)?maxUtilization:utilization;
    }

    @Override
    public double getMEMUtilization(VirtualMachine vm) {
        
        //the percentage of resources demanded by the VM.
        double percentage=vm.getVMemory()/getMemoryCapacity();
        return (percentage < 1 )?percentage*vm.getMEMdemand():vm.getMEMdemand();      
        
    }

    @Override
    public double getMemoryRequested() {
        return(double)description.get("memoryRequested");
    
    }
    
    private void setMemoryRequested(double memory){
        
      description.put("memoryRequested",memory);   
        
    }

    @Override
    public double getRealCPUUtilization(VirtualMachine vm) {
        if(!isThisAllocated(vm)){
              //return -Double.MAX_VALUE;
         
              throw new SimulatedMachineException("the Vm"+ vm +"is not allocated on the host"+this);
         }
          
           if(State.ShutDown.equals(getHost().state)||State.Suspended.equals(getHost().state))
            return 0D;
          
        double utilization=getCPUsUtilization(vm);        
        double maxUtilization=getVMResourcesSharing(vm).cPUscredit;
        
        //System.out.println("dsdsds"+maxUtilization+"sasa"+utilization);
        
        return (utilization>maxUtilization)?maxUtilization:utilization;
    }

    @Override
    public double getvCPUsRequested() {
        return (double)description.get("vCPUSRequested"); 
    }

     private void setvCPUsRequested(double newUtil ) {
        description.put("vCPUSRequested",newUtil);
    }
     
    @Override
    public void setVMWeights(VirtualMachine vm, double... weights) throws Exception {
         for (ResourcesSharingMap m:getResourcesSharingMap()){

            if(m.vm==vm){
            
                m.CPUweight=weights[0];
                m.MemoryShare=weights[1];
            
                return;
            }
        
           
        }
    
         throw new Exception("It is not possible to set the resource sharing,"
                    + " vm-"+vm.getIndex()+
                    "does not exist or it is not allocated on this PM");
    }

    @Override
    public void deAllocateVms(Collection<VirtualMachine> vMs) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void LivemigrateVms(HashMap<VirtualMachine, ComputationalNode> migrationmap) throws MigrationException {
        if(!(getMigrationMap()==null)){
                
                if(!(getMigrationMap().isEmpty())){
                    //System.out.println("why you are not working");
                
                    throw new MigrationException("two simultaneous migration processes are not supported yet");
                }                
            }
         
         try{
           // System.out.println("migration of ");
            
           // System.out.println("migration of ");
            setMigrationMap(migrationmap);
            setvMsTobeMigrated(new ArrayList(getMigrationMap().keySet()));
            for(VirtualMachine vm:getvMsTobeMigrated()){
            
                vm.state=State.Migration;
            //    System.out.println("migration of "+vm);
            
            
            }
            
            
        getDom_0().registerI_O_driver(new I_O_Driver() {

            
            
                 private final int NmaxRounds=10;
        
                 private int Nrounds=0;
        
                 private final double txRateMax=10E8,txRateMin=3E8,deltaTx=50E6;
        
                 private double txRate=txRateMin;
        
                 private long prevTime,succTime;
            
                 
               

                @Override
                public double retrieve() {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public void receive() {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }
                
       
        
                
                public String getEventID() {
                    return "MigrationDaemon"; //To change body of generated methods, choose Tools | Templates.
                }
        
        
                @Override
                 public void action() {
                     // updateMemoryTarget(); //To change body of generated methods, choose Tools | Templates.
                     //System.out.println("update memory fire time ="+ Timed.getFireCount());
            
                    double MinTime=1;//(sec)
            double memoryTxRate;
            VirtualMachine vmMigrating=null;
            
            if(getvMsTobeMigrated()==null){
                System.out.println("nothing to migrate");
                return;
                
            }
            if(!getvMsTobeMigrated().isEmpty()){
            
                vmMigrating=getvMsTobeMigrated().get(0);
                
                if(vmMigrating.getMigrationState().equals(VirtualMachine.MigrationState.none)
                        &&Nrounds>0){
                
                    Nrounds=0;
                }
                
            }else{
            
                succTime=Nrounds=-1;//TODO means that the process has been termined by outside.
            
            }
           
            
            prevTime=succTime;
            System.out.println("round =" +Nrounds+"new prev time="+prevTime);
            txRate=txRate+(Nrounds*deltaTx);
           
            
             if((Nrounds==0)
                     &&
                   vmMigrating.getMigrationState().equals(VirtualMachine.MigrationState.none)){
                 
                 System.out.println("InitialPhase");
                
                 vmMigrating.setMigrationState(VirtualMachine.MigrationState.initial);
                 //0.07
                 
                 getDom_0().setMigrationOverhead(0.2*(vmMigrating.getVMemory()/getMemoryCapacity()));
                // getMigrationMap().get(vmMigrating).UploadMigrationOverhead(0.2*(vmMigrating.getVMemory()/getMemoryCapacity()));
                 
                 
                 succTime= Math.round((vmMigrating.getMEMdemand()*vmMigrating.getVMemory()*8)/txRate);
                 // System.out.println(succTime);
                 succTime=((succTime>0)?succTime:1);
                 // System.out.println(succTime);
                // System.out.println(migrationOverhead);
                 Nrounds=Nrounds+1;
                // this.updateFrequency(succTime);
                 return;
             }
            
            
              if((Nrounds>=1&&Nrounds<NmaxRounds&&succTime>MinTime)
                      &&
                  (vmMigrating.getMigrationState().equals(VirtualMachine.MigrationState.trasferring)
                      ||
                  vmMigrating.getMigrationState().equals(VirtualMachine.MigrationState.initial))){
                      
                  System.out.println("precopy phase");
                 
                  vmMigrating.setMigrationState(VirtualMachine.MigrationState.trasferring);
                  succTime=Math.round((preCopy(vmMigrating,prevTime)*8)/txRate);
                  succTime=((succTime>0)?succTime:1);
                  //migrationOverhead=0.09*(vmMigrating.getVMemory()/memoryCapacity);
                  setMigrationOverhead(0.2*(vmMigrating.getVMemory()/getMemoryCapacity()));
                  getMigrationMap().get(vmMigrating).UploadMigrationOverhead(0.2*(vmMigrating.getVMemory()/memoryCapacity));
                  
                  Nrounds=Nrounds+1;
                 // this.updateFrequency(succTime);
                  return;
              }               
              
             if(((Nrounds==NmaxRounds)||(succTime==MinTime))
                     &&
                  ( vmMigrating.getMigrationState().equals(VirtualMachine.MigrationState.trasferring)
                     || vmMigrating.getMigrationState().equals(VirtualMachine.MigrationState.initial))){
                 
                 System.out.println("stopandcopy phase");
               
                 vmMigrating.setMigrationState(VirtualMachine.MigrationState.stopAndCopy);
                 succTime=Math.round((long)((preCopy(vmMigrating,prevTime)*8)/txRateMax));
                 succTime=((succTime>0)?succTime:1); //required for a correct simulation behaviour 
                 // migrationOverhead=0.12*(vmMigrating.getVMemory()/memoryCapacity);
                 
                 setMigrationOverhead(0.09*(vmMigrating.getVMemory()/getMemoryCapacity()));
                 getMigrationMap().get(vmMigrating).UploadMigrationOverhead(0.2*(vmMigrating.getVMemory()/getMemoryCapacity()));
                  
                 Nrounds=NmaxRounds+1;
                 //Nrounds=Nrounds+1;
                // this.updateFrequency(succTime);
                 return;
             }
           
            
           if ((Nrounds>NmaxRounds)
                   &&
                   vmMigrating.getMigrationState().equals(VirtualMachine.MigrationState.stopAndCopy)){
                System.out.println("activation phase");
               
                 vmMigrating.setMigrationState(VirtualMachine.MigrationState.activation);
                Activation(vmMigrating,MigrationMap.get(vmMigrating));
                Nrounds=0;
                txRate=txRateMin;
              //  succTime=1;
              //  this.updateFrequency(succTime);
                
            }
            
           if(vmMigrating!=null){
           
                System.out.println(vmMigrating.getMigrationState());
                
               
           }
            System.out.println("migration last iter "+vmMigrating+"nu,mbero of round"+Nrounds);
           // System.out.println("new trasferring time="+succTime);
            
//             System.out.println(vmMigrating.getMigrationState());
                
              //  Nrounds=Nrounds+1;
               // 
                 System.out.println("new trasferring time="+(succTime+Timed.getFireCount()));
                
                
            
            
            if(getvMsTobeMigrated().isEmpty()){
             System.out.println("Migration process has been terminated");
                Nrounds=-1;
                deRegisterEvent(this);
                
                if(state.equals(State.Suspension)){
                    System.out.println("i want to suspend the machine"+PhysicalMachine2.this);
                    Suspend();
                    if(PhysicalMachine2.this.isSubscribed()){
                    
                        throw new SimulatedMachineException("suspension due to migration failed");
                    }
                }
                
                if(state.equals(State.Shutdowning)){
                     System.out.println("i want to stop the machines"+this);
                    Shutdown();
                }
            
            }
        }
       };         
            );
           
            for(VirtualMachine vm:vms){
            vMs.remove(vm);
            removeVMResourcesSharing(vm);
            MemoryRequested-=vm.getVMemory();
            vCPUsRequested-=vm.getVCPUs();
            logger.debug("Vm"+vm.getIndex()+"has been migrated on host"+targetHost.index);
            
            }
            
            targetHost.allocateVMs(vms);
       
            
          
        }catch(Exception e){
           throw new SimulatedMachineException(e);}
     
     
    }

    @Override
    public double getMemoryCapacity() {
       return (double)description.get("memorycapacity");
    }

    @Override
    public double getvCPUs() {
        return (double)description.get("vCPUs");
    }
    
   @Override
    public double getCPUsUtilization(VirtualMachine vm) {
         double percentage=(double)vm.getVCPUs()/(double)getvCPUs();
        return (percentage < 1 ) ? percentage*vm.getCPUdemand() : (double)vm.getCPUdemand() ; /* the user can ask for more vCPUs than the ones  physically present on the host,
        *    but I assume this will not affect the load on the PM
        */
    } 
    
    
    private ArrayList<ResourcesSharingMap> getResourcesSharingMap() {
        return (ArrayList<ResourcesSharingMap>)description.get("sharingMap"); //To change body of generated methods, choose Tools | Templates.
    }
    
    private void removeVMResourcesSharing(VirtualMachine vm){
    
        for (ResourcesSharingMap sm:getResourcesSharingMap()){
            
             if(sm.vm==vm){
                 getResourcesSharingMap().remove(sm);
             
                 break;
             }        
        }
    }
    
    private ResourcesSharingMap getVMResourcesSharing(VirtualMachine vm){
    
        for (ResourcesSharingMap sm:getResourcesSharingMap()){
            
             if(sm.vm==vm)
                 return sm;        
        }
      return null;
    }
    
    
    
    
    @Override
    public ArrayList<VirtualMachine> getvMsTobeMigrated(){
    
        return (ArrayList<VirtualMachine>)description.get("vMsTobeMigrated");
    
    }
    
    private void setvMsTobeMigrated(ArrayList<VirtualMachine> n){
    
        description.put("vMsTobeMigrated",n);
    
    }
    
   
    
    @Override
     public  HashMap <VirtualMachine,ComputationalNode> getMigrationMap(){
    
        return (HashMap <VirtualMachine,ComputationalNode>)description.get("MigrationMap");
    
    }
     
     
 
     private void setMigrationMap(HashMap <VirtualMachine,ComputationalNode> m){
    
         description.put("MigrationMap",m);
    
    }

    @Override
    protected boolean stop() {
       
  
       try{
         for(VirtualMachine vm:getAllocatedVMs()){
            if(!vm.state.equals(SimulatedMachine.State.Migration))
             vm.Shutdown();
         }
         
            description.put("MigrationMap", null);
            description.put("vMsTobeMigrated", this);
       
            ((ArrayList)description.get("VMs")).clear();
            ((ArrayList)description.get("sharingMap")).clear();
            
             setHostCPUsUtilization(0);
             setHostMemoryUtilization(0);
             
             setvCPUsRequested(0);
             setMemoryRequested(0);
            
       }
       catch(Exception e){
       
           throw e;
       
       }
       
      return true;
    }

    @Override
    public ComputationalNode getHost() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


   

    @Override
    public double getHostMemoryUtilization() {
        return (double)description.get("memoryUtilization");

    }

    @Override
    public double getHostCPUsUtilization() {
        return (double)description.get("CPUsUtilization"); 
    }
    
    private void setHostMemoryUtilization(double newUt) {
        description.put("memoryUtilization",newUt);

    }

 
    private void setHostCPUsUtilization(double newUt) {
        description.put("CPUsUtilization",newUt); 
    }

    
    @Override
    protected boolean start() {
        
        VirtualMachine domO=new Dom_O("Dom_0", getHost().getIndex(), 2, 0);
        domO.setMyHost(getHost());
        
        if(!getAllocatedVMs().isEmpty()){
        
            throw new Exception("VMs already allocated before the Hypervisor start process completed");
        }
        
        allocateVMs(Collections.singletonList(domO));
        getHost().registerEvents(updateResourcesConsumption,updateWorkingSet);
        
        return true;
    }

    @Override
    protected boolean suspend() {
        
       for(VirtualMachine vm:getAllocatedVMs()){
            if(!vm.state.equals(SimulatedMachine.State.Migration))
             vm.Suspend();
         }
       
            description.put("MigrationMap", null);
            description.put("vMsTobeMigrated", this);

            ((ArrayList)description.get("sharingMap")).clear();
            
             setHostCPUsUtilization(0);
             setHostMemoryUtilization(0);
             
             setvCPUsRequested(0);
             setMemoryRequested(0);
       
       return true;
    }

    @Override
    protected boolean resume() {
         for(VirtualMachine vm:getAllocatedVMs()){
                 
            if(vm.state.equals(SimulatedMachine.State.Suspended))
                 vm.run();
            
            else
                throw new SimulatedMachine.SimulatedMachineException(new Exception(vm+"was not correctly suspended and cannot be correctly runedd again"));
            }
             
             allocateVMs(getAllocatedVMs());
             
             return true;
    }

    

    private interface I_O_Driver {
        
            public void onReceivingaction(Datastore.fileDescriptor fd, SimulatedMachine source, SimulatedMachine target);
            public void onSendingaction(Datastore.fileDescriptor fd, SimulatedMachine source, SimulatedMachine target);
        } 
  
    
    
    private class Dom_O extends VirtualMachine{
        
        final List<I_O_Driver> i_o_queue= new ArrayList();
        final HashMap<Datastore.fileDescriptor,I_O_Driver> fileTX=new HashMap();
        
        
        final Workload overhead=new Workload(){

            GaussianGenerator sChedulerWorkload=new GaussianGenerator
            (HypervisorMemoryutilizationModel(),0.1
              , HypervisorCPUutilizationModel(),0.1);
            
          @Override
            public double getMemoryDemand() {
                return sChedulerWorkload.getMemoryDemand(); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public double getCPUDemand() {
               return sChedulerWorkload.getCPUDemand(); //To change body of generated methods, choose Tools | Templates.
            }
        
            private double HypervisorCPUutilizationModel(){
    
                return (1/(1+Math.exp(-((2*getvCPUsRequested()/getVCPUs())-2))));
    
            }
              private double HypervisorMemoryutilizationModel(){
    
                return 0.1*getMemoryCapacity();
             }
        }; 
        
        public Dom_O(String index,String PM,int VCPUs,double Vmem) {
            super(index,PM,VCPUs,Vmem);
            setWorkload(overhead);
            
            
        }

        @Override
        protected void FileReceived(Datastore.fileDescriptor fd, SimulatedMachine source, SimulatedMachine target) {
           
        }

        @Override
        protected void FileSent(Datastore.fileDescriptor fd, SimulatedMachine source, SimulatedMachine target) {
            
        }

        
        public double sentFile(Datastore.fileDescriptor fd,I_O_Driver driver, SimulatedMachine source, SimulatedMachine target) {
                 
                if(!i_o_queue.contains(driver)){
                   throw new SimulatedMachine.SimulatedMachineException("the driver used for network IO does not exist or it is not registred");
                }
                
                if(!fileTX.containsKey(fd)){
                
                    fileTX.put(fd, driver);
                
                }   
                getHost().get_attached_Network().get_end_to_end_Channel(this, target); //To change body of generated methods, choose Tools | Templates.
                getHost().get_attached_Network().sendFile(fd, this, target);
                
                double timestamp=Timed.getFireCount();
                
                System.out.println("file "+fd.getFileID()+"transfer started at"+timestamp+"from"+source+"to"+target);
                return 0;
                
                
        }

        public void registerI_O_driver(I_O_Driver io){
        
            i_o_queue.add(io);
        
        }
        
        public void deregisterI_O_driver(I_O_Driver io){
        
            i_o_queue.remove(io);
            
        }
        
        
        
        
        
        public void setMigrationOverhead(double ovh){
        
            
        }
        
        
    }    
    
    /*Resources sharing Map gives informations about the VMs resource consumption.
    * This informations are relevant to determine the dynamic multiplexing of the physical resources. 
    */
   public class ResourcesSharingInfo{
    
        private final double CPUweight;
        private final double Memorytarget;
        private final double WorkingSet;
        private final double MemoryShare;
        private final double cPUscredit;
        private final VirtualMachine vm;
        
       private ResourcesSharingInfo(ResourcesSharingMap m){
        
            this.CPUweight=m.CPUweight;
            this.MemoryShare=m.MemoryShare;
            this.Memorytarget=m.Memorytarget;
            this.WorkingSet=m.WorkingSet;
            this.cPUscredit=m.cPUscredit;
            this.vm=m.vm;
        }
        
        @Override
        public String toString(){
        
           return "\n Virtual machine "+vm.getIndex()+
                   "CPU share"+CPUweight+"\nMemory share"+
                   MemoryShare+"\nWorking set"+
                   WorkingSet+"\nMemory target"+Memorytarget; 
        
        }

        public double getCPUshare() {
            return CPUweight;
        }

        public double getMemorytarget() {
            return Memorytarget;
        }

        public double getWorkingSet() {
            return WorkingSet;
        }

        public double getMemoryShare() {
            return MemoryShare;
        }
        public double getCPUsCredits() {
            return cPUscredit;
        }
        
    };
    
    private class ResourcesSharingMap{
    
        private double CPUweight=1;
        private double Memorytarget=-1;//see Xen balloning for more details.
        private double WorkingSet;
        private double MemoryShare=1;
        private double cPUscredit;//see Xen scheduler for more details.

       
        private VirtualMachine vm=null;

        public ResourcesSharingMap(VirtualMachine vm) {
            this.vm=vm;
            setInitialMemoryTarget();
        }
        
        
        public double getcPUscredit() {
            return cPUscredit;
        }

        public void setcPUscredit(double cPUscredit) {
            this.cPUscredit = cPUscredit;
        }
        public double getCPUshare() {
            return (vm!=null)?(this.CPUweight):null;
        }

        public void setCPUshare(double CPUshare) {
            this.CPUweight = CPUshare;
        }

        public double getMemorytarget() {
            return Memorytarget;
        }

        public void setMemorytarget(double Memorytarget) {
            this.Memorytarget = Memorytarget;
        }
    
         public void setWorkingSet(double WorkingSet) {
            double alpha=0.6D;
            this.WorkingSet =alpha* this.WorkingSet+(1-alpha)*WorkingSet;
        }
    
         
        private void setInitialMemoryTarget(){
        
            this.Memorytarget=(getMemoryRequested()>getMemoryCapacity())?
                    ((MemoryShare*(double)vm.getVMemory()/(double)getMemoryRequested())
                    *getMemoryCapacity()):(double)vm.getVMemory();
            
        }
        
       
       
    }
    
    
    /*this method dynamically updates each VM memory 
    target accordingly to the current memory request*/
    
    private void updateMemoryTarget(){
    
        if (getMemoryRequested()<=getMemoryCapacity()){
           // change the memory target only if there is overcommitment
            return;
        }
        
      // System.out.println("\n\nEEEEEEEEEEEEEEEE ="+getMemoryRequested()+"\n\n");
        
        double totalMemoryUsage = 0,allocMem
                ,avgMemDem,updateMem
                ,myNewTarget,share;
        int vIndex=0;
        boolean update=false;
        
        double[] newComputedTarget=new double[getAllocatedVMs().size()];
        
        for (VirtualMachine vmTemp:getAllocatedVMs()){
        
            share=getVMResourcesSharing(vmTemp).MemoryShare;
            allocMem=getVMResourcesSharing(vmTemp).Memorytarget/getMemoryCapacity();
            avgMemDem=getVMResourcesSharing(vmTemp).WorkingSet;
            
            updateMem=allocMem-avgMemDem;
          //  System.out.println("allocmem "+allocMem+"avgmemdem "+avgMemDem);
            
            if(updateMem<0 && !update ){
            
                update=true;
                logger.debug("update "+vmTemp.getIndex()+"memory required "+updateMem);
            }
                
            newComputedTarget[vIndex]=avgMemDem-allocMem;
            totalMemoryUsage+=(share*(avgMemDem-allocMem)*(getMemoryCapacity())+getVMResourcesSharing(vmTemp).Memorytarget);
            vIndex+=1; 
           
        }
        
        if(!update)//there is no need to update the VM memorytarget 
            return;
        
        if(logger.isDebugEnabled()){
        String array=getHost().getIndex()+"Memory demand[ ";
            for(double e:newComputedTarget)
                array+="|"+e;
            logger.debug(array+" ]");
       } 
        
        vIndex=0;
        for  (VirtualMachine vmTemp:getAllocatedVMs()){
            
            share=getVMResourcesSharing(vmTemp).MemoryShare;
            
            myNewTarget=((newComputedTarget[vIndex]*share*(getMemoryCapacity())+
                    (getVMResourcesSharing(vmTemp).Memorytarget))
                    /totalMemoryUsage)*getMemoryCapacity();
            
            getVMResourcesSharing(vmTemp).setMemorytarget(myNewTarget);
            vIndex+=1;
        }    
    }

   
     /*this method dynamically updates each VM CPUs credit 
    accordingly to the current CPUs demand*/
   private void updateCPUsCredits() {
    
           class CheckCorrectness{
        
            private double weightSum;

            public double getWeightSum() {
                return weightSum;
            }

            public void setWeightSum(double weightSum) {
                this.weightSum = weightSum;
            }
            
           
           private double unused;  

            public double getUnused() {
                return unused;
            }

            public void setUnused(double unused) {
                this.unused = unused;
            }
           
           public double additional;

            public double getAdditional() {
                return additional;
            }

            public void setAdditional(double additional) {
                this.additional = additional;
            }
           
           public double CreditsCheck(){
           
               double sum=0;
               for (VirtualMachine vmTemp:getAllocatedVMs()){
                    sum+=getVMResourcesSharing(vmTemp).cPUscredit;
               }
           
               return sum;
           }
            
        }
        
      // the method starts here !!!!
         if(SimulatedMachine.State.ShutDown.equals(getHost().state)||SimulatedMachine.State.Suspended.equals(getHost().state))
            return ;
        
        double CreditsDemand=0,myCredits=0;
        double notUsed=0,addictional=0;
        double demand,weight=0;
        
        double TotalAddictionalDemand=0;

        CheckCorrectness evaluate=new CheckCorrectness();
        
        
        //nothing to do!!
         if(getAllocatedVMs().isEmpty()){
              return ;
         }
        
         //no overcommitment, no dinamic Credits computation is required
         if(getvCPUs()<=getvCPUsRequested()){
         
              for (VirtualMachine vmTemp:getAllocatedVMs()){
              
                  getVMResourcesSharing(vmTemp).setcPUscredit(getCPUsUtilization(vmTemp));
                  
              }
        
              return;
         }
         
         //computes the current credits demand 
        for (VirtualMachine vmTemp:getAllocatedVMs()){
        
            CreditsDemand+=getVMResourcesSharing(vmTemp).CPUweight*getCPUsUtilization(vmTemp);
        }
          
        
        for (VirtualMachine vmTemp:getAllocatedVMs()){
            //credit settings, implements the proportional fair share
            //determining the share of the CPU according to the wheight parameter (see XEN credit scheduler)  
            myCredits=getVMResourcesSharing(vmTemp).CPUweight*
                    getCPUsUtilization(vmTemp)/CreditsDemand;
            
            getVMResourcesSharing(vmTemp).setcPUscredit(myCredits);

            demand=myCredits-(getCPUsUtilization(vmTemp));

            if(demand<0){

                TotalAddictionalDemand+=(-1)*demand;
            }
             
            else{
                notUsed+=(demand);
            }
            
        }

         for (VirtualMachine vmTemp:getAllocatedVMs()){
         
             demand=getVMResourcesSharing(vmTemp).getcPUscredit()-(getCPUsUtilization(vmTemp));
         
              if(demand<0){
                  weight=(0-demand)/TotalAddictionalDemand;
 
                  addictional=weight*notUsed;
      
                  myCredits=demand+addictional;
        
                  if(myCredits>=0){
        
                      getVMResourcesSharing(vmTemp).setcPUscredit(getCPUsUtilization(vmTemp));    
                  }            
                  else{
                      getVMResourcesSharing(vmTemp).setcPUscredit(getVMResourcesSharing(vmTemp).getcPUscredit()+addictional);                
                  }
              }
              else{
                  getVMResourcesSharing(vmTemp).setcPUscredit(getCPUsUtilization(vmTemp));
              }
         }
        
        if (logger.isDebugEnabled()){
        logger.debug("\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n"
                + "Credit Calculation Debug info for "+this.getHost()
                + "\n ******************************************************\n"+
                "CPUs credits->  "+evaluate.CreditsCheck()+"\n"+
                "not used Host CPU (%)->  "+evaluate.getUnused()+"\n"+
                "total weight info->  "+evaluate.getWeightSum()+"\n"+
                "Vm addictional->  "+evaluate.getAdditional()+
                "\n******************************************************************\n"+
                "\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n"
        );
        
        }
       
       
        if(evaluate.CreditsCheck() <= 0.9990 || evaluate.CreditsCheck() > 1.00001){
            logger.error("the real utilization method does not provide correct results"
                    + " (the check value is "+evaluate.CreditsCheck()+" should be 0.99< check >1.00001)");
        }
    }
    
  private void UpdateCPUUtilization(){
    
         double totalCpu=0;
         for (VirtualMachine vTemp : getAllocatedVMs()) {
                    
                        totalCpu+=getCPUsUtilization(vTemp);
                        
                    }
          if(totalCpu > 1.0000002){
                    logger.error("the cpu utilization is not correct"
                    + " ( value is "+totalCpu+" should be < 1.0000002)");
                    
             }
             
            setHostCPUsUtilization(totalCpu);
  }
  
  private void UpdateMemoryUtilization(){
    
         double totalMem=0;
         for (VirtualMachine vTemp : getAllocatedVMs()) {
                    
                        totalMem+=getRealMEMUtilization(vTemp);
                    }
                    
                    //totalCpu+=getSchedulingOverhead()+migrationOverhead;
        setHostMemoryUtilization(totalMem);
    
    }
  
    
     public ResourcesSharingInfo getVMResourceSharing(VirtualMachine vm) throws Exception{
    
        
        for (ResourcesSharingMap m:getResourcesSharingMap()){

            if(m.vm==vm){
                     
                return new ResourcesSharingInfo(m) ;
            }
        
        }
    
        throw new Exception("It is not possible to get the resource sharing,"
                    + " vm-"+vm.getIndex()+
                    "does not exist or it is not allocated on this PM");
    }
    
     public boolean isThisAllocated(VirtualMachine vm ){
     
           return getAllocatedVMs().contains(vm);
     
     }
     
     
     private Dom_O getDom_0(){
     
         for (VirtualMachine vm:getAllocatedVMs()){
         
            if(vm instanceof Dom_O){
             
                return (Dom_O)vm;
            }
         }
     
         return null;
     }

     
     
     
     
    //Hypervisor CODE FINISH HERE!!!!!
}
