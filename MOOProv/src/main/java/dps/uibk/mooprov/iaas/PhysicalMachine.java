/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.iaas;

import java.util.HashMap;

/**
 *
 * @author ennio
 */
public abstract class PhysicalMachine extends SimulatedMachine{

    // this hashMap contains aal the information regarding the PM instance
    protected final HashMap<String,Object> description;
    
    
    
    
     public PhysicalMachine(String index,double MemoryCapacity,
            int CPUs,int cores,boolean hp,
            double IdlePWR,double MaxPWR) throws SimulatedMachineException{
     
            description=new HashMap<>();
            
            //
            
            description.put("index", index);
            
            description.put("MemoryCapacity", index);
            
            description.put("CPUs", index);
            
            description.put("cores", index);
            
            description.put("IdlePWR", index);
            
            description.put("MaxPWR", index);
            
            description.put("hyperthreading", hp);
            
            description.put("VCPUs", (hp) ? (CPUs*cores*2) : (CPUs*cores));
     
     }
     
     
     public PhysicalMachine(double MemoryCapacity, int CPUs,int cores,boolean hp) throws SimulatedMachineException{
        this("",MemoryCapacity,CPUs,cores,hp,50,200);
     
        description.put("index", getindex());
    }
     
     
    
    public PhysicalMachine() {
        this("",256*Math.pow(2,30),16,2,true,50,200);//256 GB,16CPUs,2cores,Hyperthreading
        
        description.put("index", getindex());
    }

    
    
    public PhysicalMachine(String index, double MemoryCapacity, int CPUs,int cores,boolean hp) {
        this(index,MemoryCapacity,CPUs,cores,hp,50,200);
        
    }
    
    
    
    
    //get an ID for the current machine
    private String getindex(){
        int i=7;
        String index="";
        char s[]={'a','b','2','r','5','7','g','h'
                ,'i','o','a','f','4','v','2','s',
                't','r','v','a','e','u','o','r',
                'y','#','l','$','m','k','u','8'};
        while((i--)>0)
            index+=s[(int)((Math.random()*100)%32)];
        return index;
            
    }
    
    
    @Override
    public String toString(){
        
        
        String out= 
                     " PM info -  "+description.get("index")+"\n"
                    +" Physical CPUs - "+ description.get("CPUs")+"\n" 
                    +" Physical Memory - "+ description.get("MemoryCapacity")+"\n";
        
        
        return out;
                
                
    }
    
    
     public State getMachineState(){
    
        return state;
    }
    
    public Double getMemoryCapacity(){


        return (double)description.get("MemoryCapacity");
   }
    
    public Double getCpus(){


        return (double)description.get("CPUs");
   }
    
     public Double getCores(){


        return (double)description.get("cores");
   }     
    
    public String getIndex(){
    
        return (String)description.get("index");
    
    }
    
    
    public double getIdlePWR(){
    
        return (double)description.get("IdlePWR");
    
    }
    
     public double getMaxPWR(){
    
        return (double)description.get("MaxPWR");
    
    }
     
    public double getVCPUs(){
    
        return (double)description.get("VCPUs");
    
    }
    
    public boolean isHyperthreading (){
    
        return (boolean)description.get("hyperthreading");
    }
    
    public void attach_Network(Network e){
    
        description.put("Network", e);
        e.getRoute(this);
    }
    
    public Network get_attached_Network(){
    
        return (Network)description.get("Network");
    }
    
    public void detach_Network(Network e){
    
        e.closeRoute(this);
        description.remove("Network");
        
    }
    
}
