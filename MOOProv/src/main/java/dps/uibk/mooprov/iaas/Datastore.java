/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.iaas;

/**
 *
 * @author ennio
 */
public  abstract class Datastore extends PhysicalMachine {
    
    
    public static class fileDescriptor{

        
        final private long fileSize;
        final private long filetimestamp;
        final private String fileID;
        
        public fileDescriptor(long size,long tstamp,String iD) {
            
            this.fileID=iD;
            this.fileSize=size;
            this.filetimestamp=tstamp;
        }

        public long getFileSize() {
            return fileSize;
        }

        public long getFiletimestamp() {
            return filetimestamp;
        }

        public String getFileID() {
            return fileID;
        }
    
        
    
    }
    

   
    
}
