/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.iaas;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

/**
 *
 * @author ennio
 */
public abstract class Hypervisor{

    
    protected final HashMap<String,Object> description;
    
    
    /*description: 
    
       memorycapacity
       memoryRequested ,         total memory requested by the VMs
       memoryUtilization,        PM memory utilization
       cPUutilization ,          PM cpu utilization
       schedulingOvh;            CPUs hypervisors demand
       migrationOverhead ,       CPUs migration overhead
       vCPUs
       vCPUsRequested,           total VCPUs required by the VMs 
       sharingMap,               description of shares for every VMs
       sChedulerWorkload,        should be changed ??
       VMs,                      list of allocated VMs
       vMsTobeMigrated,          list of VMs has to be migrated (?)
       MigrationMap              description of source and target host for each VM which has to be migrated
    */
    
    
    
    
    
    public Hypervisor (){
    
        description=new HashMap<>();
        
        //list of VMs managed by this Hypervisor       
        
    }
    
    
    
    public void registerPM(ComputationalNode pm){
    
        description.put("PhysicalMachine",pm);
        
        double vcpus=(pm.isHyperthreading()) ? (pm.getCpus()*pm.getCores()*2) : (pm.getCpus()*pm.getCores());
        
        description.put("vCPUs",vcpus);
        description.put("memorycapacity", pm.getMemoryCapacity());
        
    }
    
    
    /**
     * 
     * @param vMs list of VMs to allocate
     * @return returns the computing host
     */
    public abstract ComputationalNode getHost();
    
    /**
     * 
     * @param vMs list of VMs to allocate
     * @return returns the computing host
     */
    public abstract double getHostMemoryUtilization();
    
    /**
     * 
     * @param vMs list of VMs to allocate
     * @return returns the computing host
     */
    public abstract double getHostCPUsUtilization();
    
    
    /**
     * used to allocate the VMs on the computational node on which this Hypervisor is running
     * @param vMs list of VMs to allocate
     * 
     */
    public abstract void allocateVMs(Collection<VirtualMachine> vMs);
    
    /**
     * used to deallocate the VMs on the computational node on which this Hypervisor is running
     * @param vMs list of VMs to deallocate
     * 
     */
     public abstract void deAllocateVms(Collection<VirtualMachine> vMs);
    
    
     /**
     * gives the list of allocated VMs 
     * @return the current list of VMs allocated on the computational node. 
     */
     public abstract Collection<VirtualMachine> getAllocatedVMs();
     
     
     /**
     * gives the number of allocated VMs 
     * @return the current number of VMs allocated to the computational node. 
     */
     public abstract int getNumberofAllocatedVms();
     
      
    /**
     * gives the actual percentage of memory used by the VM 
     * @param vm VM of interest
     * @return the current memory allocated to the VM accordingly to current PM memory load. 
     */
      public abstract double getRealMEMUtilization(VirtualMachine vm);
    
     /**
     * returns the memory demand in percentage , requested by the VM 
     * @param vm VM of interest
     * @return the current memory requested by the VM regardless the current PM memory load. 
     */  
    public abstract double getMEMUtilization(VirtualMachine vm);
    
    /**
     * This method accounts for the current memory requested by the VMs 
     * @param vm VM of interest
     * @return the current memory requested by the VM regardless the current PM memory load. 
     */     
     public abstract double getMemoryRequested();
    
     
     /**
     * This method accounts for the current memory capacity of the Host 
     * @param vm VM of interest
     * @return the current memory requested by the VM regardless the current PM memory load. 
     */     
     public abstract double getMemoryCapacity();
     
      /**
     * This method accounts for the requested CPUs percentage by the VM 
     * @param vm VM of interest
     * @return the CPUs requested by the VM regardless the current PM memory load. 
     */
     public abstract double getCPUsUtilization(VirtualMachine vm);

     /**
     * 
     * @param vm VM of interest
     * @return the current CPU utilization accordingly to current PM load. 
     */
    public abstract double getRealCPUUtilization(VirtualMachine vm); 
     
    
    
    /**
     * 
     *
     * @return the total VCPUS requested by the managed VMs.
     */
    public abstract double getvCPUsRequested(); 
    
    
    /**
     * 
     * @param vm VM of interest
     * @return the total VCPUS on the host.
     */
    public abstract double getvCPUs(); 

     /**
     * 
     * @param vm VM of interest
     * @return set the weights for each VM (see Xen scheduler for an example).
     */
    public abstract void setVMWeights(VirtualMachine vm,double ... shares ) throws Exception;
    
     /**
     * This method allows to live migrate one or a set of VMs from one host to another
     * @param hashMap <VirtualMachine,PhysicalMachine2> contains each VM to be migrated end the target host.
     * 
     */
    public abstract void LivemigrateVms(HashMap<VirtualMachine,ComputationalNode> migrationmap) throws MigrationException;

    
     /**
     *
     * @return a list <VirtualMachine> of VM to be migrated.
     * 
     */
    public abstract ArrayList<VirtualMachine> getvMsTobeMigrated(); 
    
    /**
     * This method allows to check which VMs are migrated and the target host
     * @param hashMap <VirtualMachine,PhysicalMachine2> contains each VM to be migrated end the target host.
     * 
     */
    public abstract HashMap <VirtualMachine,ComputationalNode> getMigrationMap();
    
     /**
     *This method allows to stop the Hypervisor before a shutdown of computing node
     * @param new list <VirtualMachine> of VM to be migrated.
     * 
     */
    protected abstract boolean stop();
    
    /**
     *This method allows to start the Hypervisor of a computing node
     * @param new list <VirtualMachine> of VM to be migrated.
     * 
     */
    protected abstract boolean start();
    
    
    /**
     *This method allows to safety suspend before a suspension of computing node
     * @param new list <VirtualMachine> of VM to be migrated.
     * 
     */
    protected abstract boolean suspend();
    
    /**
     *This method allows to safety resume after a suspension of a computing node
     * @param new list <VirtualMachine> of VM to be migrated.
     * 
     */
    protected abstract boolean resume();
    
    
    
    public static class MigrationException extends Exception{

        public MigrationException(Throwable cause) {
            super(cause);
        }
        
         public MigrationException(String s) {
            super(s);
        }
    
    }

   


}
