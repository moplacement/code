/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.iaas;

import hu.mta.sztaki.lpds.cloud.simulator.DeferredEvent;
import hu.mta.sztaki.lpds.cloud.simulator.Timed;
import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;

/**
 *
 * @author ennio
 */
public abstract class Network extends Timed{

    
    protected final Collection <NetworkEvents> EventQueue=
            Collections.synchronizedList(new ArrayList<NetworkEvents>());
    
    protected final Collection <NetworkTrasmissionEvents> trasmissionQueue=
            Collections.synchronizedList(new ArrayList<NetworkTrasmissionEvents>());

    protected final HashMap<SimulatedMachine,ArrayList> lookupTable=
            new HashMap<>();
    
    protected long timestamp=0l;
    
    public Collection<NetworkEvents> getEventQueue() {
        return EventQueue;
    }
    
    
    public Network()throws SimulatedNetworkException{
        try{
            
            this.setBackPreference(true);//this set the priority of this event,setting this preference guarantees an order among the events 
        //are going to be executed at the same time (i.e. same time fire.)
            this.subscribe(1);
        }
        catch(Exception e){
            throw new SimulatedNetworkException(e);
                }
    }
    
    protected State state;
    
    public static enum State {
    
        ;
    
    } 
    
    
    public static abstract class NetworkEvents extends DeferredEvent{
        
                
        public NetworkEvents(long delay) {
            super(delay);
        }

        @Override
        protected void eventAction() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        public boolean reSubscribe(long freq){
            return isCancelled()? false:subscribe(freq);
        }
        
        public String getEventID() {
           return "";
        }
        
        public abstract long getPacketsToSend();

}
    
    
    
    
     public static abstract class NetworkTrasmissionEvents extends Timed{
        
        
        
        public NetworkTrasmissionEvents(long delay) {
            
        }
        
        

        
        protected void eventAction() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void tick(long fires){
        
            eventAction();
        }

        
        public boolean reSubscribe(long freq){
            return this.isSubscribed() ? false:subscribe(freq);
        }
        
        public boolean deSubscribe(long freq){
            return this.isSubscribed() ? unsubscribe():true;
        }
        
        public String getEventID() {
           return "";
        }
        
        public abstract void sentPackets(long packets);
        
        public abstract void  endTrasmission();
        
        public abstract void  startTrasmission();

}
    
    
    
    
   // final int frequency=0; 
   
    @Override
        public void tick(long fires) {
          for (NetworkEvents df:EventQueue)
                df.reSubscribe(df.getFrequency()); //This implements the execution of the queued events.
          
          for (NetworkTrasmissionEvents te:trasmissionQueue)
                te.reSubscribe(te.getFrequency());
           // System.out.println("i'm working fire time ="); //To change body of generated methods, choose Tools | Templates.
        }
       
    public boolean getRoute(SimulatedMachine source) throws SimulatedNetworkException{
    
        registerNodes(source);
        return true;
    }
    
     public boolean closeRoute(SimulatedMachine source) throws SimulatedNetworkException{
    
        DeregisterNodes(source);
        return true;
    }
     
    
    private void registerNodes(SimulatedMachine source)throws SimulatedNetworkException{
    
        
        if(!lookupTable.containsKey(source)){
            
            lookupTable.put(source, new ArrayList());
            
            for(SimulatedMachine sm:lookupTable.keySet()){
             
                lookupTable.get(source).add(sm);
                lookupTable.get(sm).add(source);
            
            }
            
            lookupTable.get(source).remove(source);
                    
        }
    
    }
    
    private void DeregisterNodes(SimulatedMachine source)throws SimulatedNetworkException{
    
        if(!lookupTable.containsKey(source)){
        
            throw new SimulatedNetworkException("there is not network between the provided source and destination hosts");
        }
        if(lookupTable.containsKey(source)){
       
            for(SimulatedMachine sm:lookupTable.keySet()){
             
                lookupTable.get(sm).remove(source);
            
            }
        }   
    
    }
    
    public abstract boolean sendFile(Datastore.fileDescriptor file,SimulatedMachine source,SimulatedMachine destin)throws SimulatedNetworkException;
    
    
    
    public static class SimulatedNetworkException extends java.lang.UnsupportedOperationException{

        public SimulatedNetworkException(Throwable cause) {
            super(cause);
        }
        
         public SimulatedNetworkException(String s) {
            super(s);
        }
         
        @Override
        public String toString() {
            
            return "<"+Arrays.toString(Thread.getAllStackTraces().get(this))+">"+super.toString();
        }

        @Override
        public synchronized Throwable initCause(Throwable cause) {
            return super.initCause(cause); //To change body of generated methods, choose Tools | Templates.
        }
    
    }
    
    
    
     public void registerEvents (NetworkEvents ... myevent) throws SimulatedNetworkException{
    
        try{
        EventQueue.addAll(Arrays.asList(myevent));
        }
        catch(Exception e){
            throw new SimulatedNetworkException(e);
                }
    }
    
     
     
    public void deRegisterEvents ()throws SimulatedNetworkException{
        
        try{
        for(NetworkEvents de:EventQueue){
            de.cancel();
            
        }
        EventQueue.clear();
        }
        catch(Exception e){
            throw new SimulatedNetworkException(e);
                }
    }
    
    public void deRegisterEvent (NetworkEvents de)throws SimulatedNetworkException{
        
        try{
            System.out.println("i delete"+de);
            de.cancel();
            EventQueue.remove(de);
        
        }
        catch(Exception e){
            throw new SimulatedNetworkException(e);
                }
    }
    
    
    
    public void registerEvents (NetworkTrasmissionEvents ... myevent) throws SimulatedNetworkException{
    
        try{
         trasmissionQueue.addAll(Arrays.asList(myevent));
        }
        catch(Exception e){
            throw new SimulatedNetworkException(e);
                }
    }
    
     
     
    public void deTrQRegisterEvents ()throws SimulatedNetworkException{
        
        try{
        for(NetworkTrasmissionEvents de:trasmissionQueue){
            de.deSubscribe(0);
        }
            trasmissionQueue.clear();
        }
        catch(Exception e){
            throw new SimulatedNetworkException(e);
                }
    }
    
    public void deRegisterEvent (NetworkTrasmissionEvents de)throws SimulatedNetworkException{
        
        try{
            de.deSubscribe(0);
            trasmissionQueue.remove(de);
        
        }
        catch(Exception e){
            throw new SimulatedNetworkException(e);
                }
    }
    
    public abstract boolean close_end_to_end_Channel(SimulatedMachine source, SimulatedMachine target) throws SimulatedNetworkException;
    
    public abstract boolean get_end_to_end_Channel(SimulatedMachine source, SimulatedMachine target) throws SimulatedNetworkException;

    public Collection getNodesConnected(SimulatedMachine sm) throws SimulatedNetworkException{
    
        if(!lookupTable.containsKey(sm)){
        
            throw new SimulatedNetworkException("there is not network between the provided source and destination hosts");
        }
    
        return lookupTable.get(sm);
    }
    
    public boolean areNodesConnected(SimulatedMachine source,SimulatedMachine target) throws SimulatedNetworkException{
    
        return lookupTable.get(source).contains(target);
    }


}
