/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.iaas;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;



/**
 *
 * @author ennio
 */
public class Tcp_Ip extends Network{

    
    
    
    public double networkBWT=(3.5E8)/1000;//network bandwidth (bites)
   

    private HashMap<SimulatedMachine,Tcpip> tcpConnections= new HashMap<>();
    
    
    public Tcp_Ip() throws SimulatedNetworkException {
        
         try{
            
             //this.setBackPreference(true);//this set the priority of this event,setting this preference guarantees an order among the events 
        //are going to be executed at the same time (i.e. same time fire.)
            this.subscribe(100);
        }
        catch(Exception e){
            throw new SimulatedNetworkException(e);
                }
        
    }    
    
    
    private class packetBulk{
    
        long packets=0;
        NetworkTrasmissionEvents dataLinkLayer;

        public NetworkTrasmissionEvents getDataLinkLayer() {
            return dataLinkLayer;
        }

        public void setDataLinkLayer(NetworkTrasmissionEvents dataLinkLayer) {
            this.dataLinkLayer = dataLinkLayer;
        }

        public long getPackets() {
            return packets;
        }

        public void setPackets(long packets) {
            this.packets = packets;
        }
        
    
    }

   
    private class Tcpip extends NetworkEvents{
        
        private final List <packetBulk> packetBulkQueue=
            Collections.synchronizedList(new ArrayList<packetBulk>());
        
         public double rTT= 0.0281;//round trip Time
         public double T0= 1.359;//Time out
         public double MTU=1500;//byte
         public double maxWindowsSize=64;//((networkBWT*rTT)/8)/MTU; //TCP window size >= BWT * RTT 
         public double lossRate=0.000013;//loss packet rate, Loss rate in %. typ. < 10-6% (< 10-8)
         public double b=2;//number of packets cumulatively ACKnowledged by a receiver .

        public Tcpip(long delay) {
            super(delay);
        }
        
        
        @Override
        protected void eventAction() {
           
        
        if(packetBulkQueue.size()>=1){
            
        long minBulksCanSend=1;
        long maxBulksCanSend=Math.round(padhye_Model(maxWindowsSize,rTT,T0,lossRate,b)/1000l)/minBulksCanSend;
        long avgPackets= maxBulksCanSend<packetBulkQueue.size()? minBulksCanSend : Math.round(( padhye_Model(maxWindowsSize,rTT,T0,lossRate,b)/1000l )
                /(packetBulkQueue.size()));
        packetBulk pb=null;
        
      //  System.out.println("model "+padhye_Model());
        
        //System.out.println("frequency "+this.getFrequency());
        for (int i=0;i<maxBulksCanSend&&i<packetBulkQueue.size();i++){
                
               pb=packetBulkQueue.get(i);
               
              // System.out.println("avgPackets "+avgPackets);
               
               if(pb.getPackets()<=avgPackets){
               
                   
                   pb.getDataLinkLayer().sentPackets(pb.getPackets());
                   pb.getDataLinkLayer().endTrasmission();
                   pb.setPackets(0);
                   packetBulkQueue.remove(pb);
               }else{
               
                   pb.getDataLinkLayer().sentPackets(avgPackets);
                   pb.setPackets(pb.getPackets()-avgPackets);
               }
        
        }
       }
        }

        @Override
        public long getPacketsToSend() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
        
        
        public List getBulKPaketsQueue() {
           return packetBulkQueue;//To change body of generated methods, choose Tools | Templates.
        }
        
        public boolean isActive(){
            return !packetBulkQueue.isEmpty();
        
        }

        public double getrTT() {
            return rTT;
        }

        public void setrTT(double rTT) {
            this.rTT = rTT;
        }

        public double getT0() {
            return T0;
        }

        public void setT0(double T0) {
            this.T0 = T0;
        }

        public double getMTU() {
            return MTU;
        }

        public void setMTU(double MTU) {
            this.MTU = MTU;
        }

        public double getMaxWindowsSize() {
            return maxWindowsSize;
        }

        public void setMaxWindowsSize(double maxWindowsSize) {
            this.maxWindowsSize = maxWindowsSize;
        }

        public double getLossRate() {
            return lossRate;
        }

        public void setLossRate(double lossRate) {
            this.lossRate = lossRate;
        }

        public double getB() {
            return b;
        }

        public void setB(double b) {
            this.b = b;
        }
        
        
    };
    
    
    
    
    @Override
    public boolean sendFile(final Datastore.fileDescriptor file, final SimulatedMachine source, final SimulatedMachine destin) {
        try{
        
            if(!lookupTable.get(source).contains(destin)||!tcpConnections.containsKey(source))
               throw  new SimulatedNetworkException("there is no channel between specified source and destination");
        }catch (NullPointerException ex){
        
            throw new SimulatedNetworkException("there is no channel between specified source and destination");
        
        }
        
        final int packetNumber=Math.round(file.getFileSize()/1500l);//+padding();
        packetBulk bulk=new packetBulk();
        bulk.setPackets(packetNumber);
        
       // System.out.println("packet to send for real"+packetNumber);
       
        final NetworkTrasmissionEvents sendBulk= new NetworkTrasmissionEvents(1) {
            
            private long packetsToSent;
            private long packetsStream;
            private boolean start=false,stop=false;
            
            @Override
            protected void eventAction() {
               // System.out.println("packets to sent ??"+Timed.getFireCount()+"  "+packetsToSent+"  "+stop+start);
               if((packetsToSent==0)&&stop){
           //         System.out.println("new time rrrrr "+timestamp);
           //         System.out.println("i terminate the connection");
                    source.FileSent(file,source, destin);
                    destin.FileReceived(file,source, destin);
                    deRegisterEvent(this);
                   
               }else if((packetsToSent>0)&&start) {
                   
                   packetsStream=(long)(Math.random()*packetsToSent);
                   System.out.println("packet stream "+packetsStream);
                   packetsStream=packetsToSent-packetsStream;
                   
                   timestamp+=Math.max(1,Math.round((packetsStream*tcpConnections.get(source).MTU*8)/networkBWT));
                   this.updateFrequency(timestamp);
             //      System.out.println("new time rrrrr "+timestamp+Timed.getFireCount()+"  "+this.isSubscribed());
                   packetsToSent-=packetsStream;
               }
            }

            @Override
            public void sentPackets(long packets) {
               packetsToSent+=packets;
            }

            @Override
            public void endTrasmission() {
                stop=true;
            }

            @Override
            public void startTrasmission() {
                start=true;
                this.updateFrequency(1);
                registerEvents(this);
            }        
        
        };
        
        bulk.setDataLinkLayer(sendBulk);
        
        tcpConnections.get(source).getBulKPaketsQueue().add(bulk);
        
        bulk.getDataLinkLayer().startTrasmission();
   
        return true;
    }
        
    
    
    
    public long padhye_Model(double maxWindowsSize,double rTT, double T0, double lossRate, double b){
    
        double valueA=(maxWindowsSize/rTT);
        double valueB=(1/
                (   (rTT*   (Math.sqrt( (2*b*lossRate)/3)   )   )
                   + T0* (Math.min( 1,  3*Math.sqrt(  (3*b*lossRate)/8) ) *
                         (lossRate* (1+32*Math.pow(lossRate, 2) ) ) )        
                )
        );
        return ((long)(Math.min(valueA, valueB)));
    }

    @Override
    public boolean close_end_to_end_Channel(SimulatedMachine source,SimulatedMachine target) throws SimulatedNetworkException {
         //super.closeChannel(source, target);
        
        if(!areNodesConnected(source, target))
             throw new SimulatedNetworkException("there is not network between the provided source and destination hosts");
        
        if(tcpConnections.containsKey(source)){
         
            if(!tcpConnections.get(source).isActive()){
            deRegisterEvent(tcpConnections.get(source));
            tcpConnections.remove(source);
            }
            
        }else{
         
             return false;
         }
        
         return true;
    }

    @Override
    public boolean get_end_to_end_Channel(SimulatedMachine source, SimulatedMachine target) throws SimulatedNetworkException {
        // super.getChannel(source, target);
         
        if(!areNodesConnected(source, target))
             throw new SimulatedNetworkException("there is not network between the provided source and destination hosts"); 
        
        if(!tcpConnections.containsKey(source)){
            tcpConnections.put(source, new Tcpip(1));
        
         registerEvents(tcpConnections.get(source));
        }
        
         return true; 
    }
    
    public Tcpip getChannel( SimulatedMachine source){
    
       if(!tcpConnections.containsKey(source)){
            return null;}
       
       return tcpConnections.get(source);
            
    }
    
    private int padding (){
    
       return 0;
    }
    
    
}
