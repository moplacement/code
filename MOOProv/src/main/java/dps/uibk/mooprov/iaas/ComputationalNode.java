/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.iaas;

/**
 *
 * @author ennio
 */
public class ComputationalNode extends PhysicalMachine {

     public ComputationalNode(){
    
    
    }
    
    
    @Override
    public String toString() {
        return super.toString(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
     public void run() throws SimulatedMachineException{
        
         if(!this.isSubscribed()&&!this.getEventQueue().isEmpty()){ //suspend
   
             if(!this.subscribe(1)){
                
                   throw new SimulatedMachineException("host" + this+"activation failed"); 
                
                }
           getHypervisor().resume();
         }
         
         else if(!this.isSubscribed()&&this.getEventQueue().isEmpty()){ //shutdown
         
             
             if(!this.subscribe(1)){
                
                   throw new SimulatedMachineException("host" + this+"activation failed"); 
                
                }
             getHypervisor().start();
         }
      
         else throw new SimulatedMachineException(new Exception("the machine cannot be runned because is already"
                 + " running or it has not correctly stopped"));
     
          this.state= State.Running;
     
     }
     
     
     
     @Override
     public void Shutdown()throws SimulatedMachineException{
        
           MachineEvents shutdown=new MachineEvents(5) {

               @Override
               protected void eventAction() {
                   
                   
                  if(IsMigrationProcessAlive()){
         
                    state=State.Suspension;
                    this.updateFrequency(5);
                  }
                  else if(getHypervisor().stop()){
         
                      try{
                          deRegisterEvents();
                          if(!this.unsubscribe()){

                              throw new SimulatedMachineException("The shutdown of the host" + this+"failed"); 
                          }
                          state= State.ShutDown;
                      }
                      catch(Exception e){
                          throw new SimulatedMachineException(e);}
                  }
              
               }
          };
         
           
           registerEvents(shutdown);
      
     }
     
     
     
     
      public boolean isRunning()throws SimulatedMachineException{
         if(State.Running.equals(this.state)!=this.isSubscribed()){
              
               if(!(State.Shutdowning.equals(this.state)||State.Suspension.equals(this.state))){
              
                   throw new SimulatedMachineException(new Exception("the machine:"+this+" is running (isSuscribed="+isSubscribed()+") but its state:"+state+" is wrong"));
               }
         }
         return this.isSubscribed(); 
     }
      
      

    @Override
    public void Suspend() throws SimulatedMachineException {
        
        MachineEvents suspend=new MachineEvents(5) {

            @Override
            protected void eventAction() {
                 if(IsMigrationProcessAlive()){
         
                    state=State.Suspension;
                    this.updateFrequency(5);
                  }
                 
                 else  if(getHypervisor().suspend()){
                     if(!this.unsubscribe()){
                
                   throw new SimulatedMachineException("The suspension of the host" + this+"failed");      
                     }
         
                     state=State.Suspended;
                 }
            }     
        };   
        
        registerEvents(suspend);
    }

  
    
   public void installHypervisor(Hypervisor hp){
   
       description.put("hypervisor", hp);
       hp.registerPM(this);
       hp.start();
   
   
   }
   
   public Hypervisor getHypervisor(){
   
       return (Hypervisor)description.get("hypervisor");
   
   }
   
   
    public boolean IsMigrationProcessAlive(){
        
        for (MachineEvents e:this.getEventQueue()){
         
            if(e.getEventID().equals("MigrationDaemon")){
             
                 
                 return true;
             }
            }
    
        return false;
    }

    @Override
    public double sentFile(Datastore.fileDescriptor fd, SimulatedMachine source, SimulatedMachine target) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void FileSent(Datastore.fileDescriptor fd, SimulatedMachine source, SimulatedMachine target) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void FileReceived(Datastore.fileDescriptor fd, SimulatedMachine source, SimulatedMachine target) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
