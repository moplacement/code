/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.iaas;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import hu.mta.sztaki.lpds.cloud.simulator.DeferredEvent;
import hu.mta.sztaki.lpds.cloud.simulator.Timed;
import java.util.Arrays;
/**
 *
 * @author ennio
 */
public  abstract class SimulatedMachine extends Timed{
    
    private final Collection <MachineEvents> EventQueue=
            Collections.synchronizedList(new ArrayList<MachineEvents>());

    public Collection<MachineEvents> getEventQueue() {
        return EventQueue;
    }
    
    
    protected State state;

    public static enum State {
    
        Suspended,Running,ShutDown,Suspension,Shutdowning,Migration;
    
    } 
    
    
    public static abstract class MachineEvents extends DeferredEvent{
        
        
        
        public MachineEvents(long delay) {
            super(delay);
        }

        @Override
        protected void eventAction() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        public boolean reSubscribe(long freq){
            return isCancelled()? false:subscribe(freq);
        }
        
        public String getEventID() {
           return "";
        }

}
    
   // final int frequency=0; 
   
    @Override
        public void tick(long fires) {
          for (MachineEvents df:EventQueue)
                df.reSubscribe(df.getFrequency()); //This implements the execution of the queued events.
           
          validateMachineStatus();
           
           // System.out.println("i'm working fire time ="); //To change body of generated methods, choose Tools | Templates.
        }
       
        
        
        
    public SimulatedMachine()throws SimulatedMachineException{
        try{
            
             this.setBackPreference(true);//this set the priority of this event,setting this preference guarantees an order among the events 
        //are going to be executed at the same time (i.e. same time fire.)
            this.subscribe(1);
            this.state=State.Running;
        }
        catch(Exception e){
            throw new SimulatedMachineException(e);
                }
    }
    
    
    public void registerEvents (MachineEvents ... myevent) throws SimulatedMachineException{
    
        try{
        EventQueue.addAll(Arrays.asList(myevent));
        }
        catch(Exception e){
            throw new SimulatedMachineException(e);
                }
    }
    
    public void deRegisterEvents ()throws SimulatedMachineException{
        
        try{
        for(MachineEvents de:EventQueue){
            de.cancel();
        }
        EventQueue.clear();
        
        }
        catch(Exception e){
            throw new SimulatedMachineException(e);
                }
    }
    
    public void deRegisterEvent (MachineEvents de)throws SimulatedMachineException{
        
        try{
            de.cancel();
            EventQueue.remove(de);
        
        }
        catch(Exception e){
            throw new SimulatedMachineException(e);
                }
    }
    
    
   
    public abstract void Shutdown()throws SimulatedMachineException;
    
    public abstract void Suspend()throws SimulatedMachineException;
     
    public abstract void run()throws SimulatedMachineException;
    
   
    public static class SimulatedMachineException extends java.lang.UnsupportedOperationException{

        public SimulatedMachineException(Throwable cause) {
            super(cause);
        }
        
         public SimulatedMachineException(String s) {
            super(s);
        }
         
        @Override
        public String toString() {
            
            return "<"+Arrays.toString(Thread.getAllStackTraces().get(this))+">"+super.toString();
        }

        @Override
        public synchronized Throwable initCause(Throwable cause) {
            return super.initCause(cause); //To change body of generated methods, choose Tools | Templates.
        }
    
        
    
    }
    
        private void validateMachineStatus() throws SimulatedMachineException{
    
        if(State.Running.equals(this.state)!=this.isSubscribed()){
              
               if(!(State.Shutdowning.equals(this.state)||State.Migration.equals(this.state)||State.Suspension.equals(this.state))){
              
                   throw new SimulatedMachineException("the machine:"+this
                           +" is running (isSuscribed="+isSubscribed()
                           +") but its state:"+state+" is wrong at simulation time:"
                           +Timed.getFireCount());
               }
         }
    
    }
        
        
        
        
        public abstract double sentFile (Datastore.fileDescriptor fd,SimulatedMachine source, SimulatedMachine target);
        
        protected abstract void FileSent(Datastore.fileDescriptor fd,SimulatedMachine source, SimulatedMachine target);
        
        protected abstract void FileReceived(Datastore.fileDescriptor fd,SimulatedMachine source, SimulatedMachine target);
        
}
