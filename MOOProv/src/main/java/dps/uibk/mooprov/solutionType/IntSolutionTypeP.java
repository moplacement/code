/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.solutionType;

import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.Variable;
import jmetal.encodings.solutionType.IntSolutionType;
import jmetal.encodings.variable.Int;

/**
 *
 * @author ennio
 */
public class IntSolutionTypeP extends IntSolutionType{

    public IntSolutionTypeP(Problem problem) {
        super(problem);
    }
    
    
     private Solution parent;
     private final double probabylity=0.1;
    
    public Solution getParent() {
        return parent;
    }

    public void setParent(Solution parent) {
        this.parent = parent;
    }

    

    @Override
    public Variable[] createVariables() {
       
        try {
             
            
            
           
            double randNum=(Math.random());
           
            
           // System.out.println("----rand number "+randNum);
            
            if(randNum<probabylity)
                return getParent().getDecisionVariables();
            
            else
            {
            
               return super.createVariables();
            
            }
            
            // return super.createVariables(); //it creates a random VMs to PM assignment
        } catch (Exception ex) {
           System.out.println("error in placement solution");
            return null;
        }
    }

}
