/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.solutionType;

import java.util.ArrayList;
import java.util.Arrays;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.Variable;
import jmetal.encodings.solutionType.IntSolutionType;
import jmetal.encodings.variable.Int;
import jmetal.util.JMException;
import jmetal.util.PseudoRandom;


/**
 *
 * @author ennio
 */
public class Placement extends IntSolutionType {

    private Solution parent;
    private final int ProbArraySize=1000;
    private int diffe,rNum;
    private final int [] pMsSelectionProbability;
    private int boundary;

    public Solution getParent() {
        return parent;
    }

    public void setParent(Solution parent) {
        this.parent = parent;
    }

    
    
    
    public Placement(Problem problem) {
        super(problem);
        pMsSelectionProbability=new int [ProbArraySize];
    }
    

    @Override
    public Variable[] createVariables() {
       
              Variable[] variables; 
        
        try {
            
            if(parent.getDecisionVariables().length<1){//new placement should be created
            
                 Variable[] Tempvariables = new Variable[problem_.getNumberOfVariables()];
            
            
                 for (int var = 0; var < problem_.getNumberOfVariables(); var++){
                
                     Tempvariables[var] = new Int((int)problem_.getLowerLimit(var),
                        (int)(problem_.getUpperLimit(var)));}
            
                setParent(new Solution(problem_,Tempvariables));
            
            }//end of random placement creation;
            
            
            
            
            try{
            computeProbabilities();
             }
             catch (Exception e){
                    
                        throw new Exception(e+"the problem is in here"); 
                    }
            int randNum=(int)(Math.random()*ProbArraySize);
            int pMindex=pMsSelectionProbability[randNum];
           // int pMindex=1;
           
           // System.out.println("----rand number "+randNum);
            
           
            
            /*
            if(pMindex==-1){
            
                   randNum=(int)((Math.random()*ProbArraySize));//to avoid to get the old placement
                
                   randNum=randNum<(boundary)?randNum:boundary-1;
                   
                   pMindex=pMsSelectionProbability[randNum];
                   
                  
                if(parent.getDecisionVariables().length<problem_.getNumberOfVariables()){
                
                    
                    
                    
                    int diff=problem_.getNumberOfVariables()-parent.getDecisionVariables().length;
                    
                    Variable[] tail= new Variable[diff];
            
                    diffe=diff;
                    
                    for (int var = 0; var < diff; var++){
                    tail[var] = new Int(pMindex,(int)problem_.getLowerLimit(var),
                        (int)problem_.getUpperLimit(var));
                    }
                    
                    ArrayList<Variable> SolutionAsList;
                    SolutionAsList=new ArrayList(Arrays.asList(parent.getDecisionVariables()));
                    SolutionAsList.addAll(Arrays.asList(tail));
                    variables=SolutionAsList.toArray(tail);
                    
                  //  return variables;
            
                    
                }    
               
               else
                {
                   variables=getParent().getDecisionVariables();
                }
                
               /* 
               int rndStart=(int)(Math.random()*problem_.getNumberOfVariables());
               
               for (int var = problem_.getNumberOfVariables()-rndStart; var < problem_.getNumberOfVariables(); var+=1){
                     
                      variables[var] = new Int(pMindex,(int)problem_.getLowerLimit(var),
                        (int)problem_.getUpperLimit(var));
                    }
               */
               //return variables; 
           // }
            
            
            variables = new Variable[problem_.getNumberOfVariables()];
            
            
           // int rndStart=PseudoRandom.randInt(0,problem_.getNumberOfVariables()-1);
            
           // variables=getParent().getDecisionVariables();
            
            for (int var = 0; var < problem_.getNumberOfVariables(); var++)
                variables[var] = new Int(pMindex,(int)problem_.getLowerLimit(var),
                        (int)problem_.getUpperLimit(var));
            
            return variables ;
            
            // return super.createVariables(); //it creates a random VMs to PM assignment
        } catch (Exception ex) {
           System.out.println("error in placement solution"+ex);
           ex.printStackTrace();
           System.out.println("rNum"+rNum);
           System.out.println("diff"+diffe);
            return null;
        }
    }

    private void computeProbabilities() throws JMException{
    
        double [] pMsAllocCounter=new double [1+(int)problem_.getUpperLimit(0)];
        
        Variable [] oldPlacement=parent.getDecisionVariables();
        
        int index=0,posCounter=0;
        
        for (int var = 0; var < oldPlacement.length; var++){
        
            pMsAllocCounter[(int)oldPlacement[var].getValue()]++;
        
        }
        
        for(int i=0;i< pMsAllocCounter.length;i++){
        
          // posCounter+=(int)((((pMsAllocCounter[i]+1)/(oldPlacement.length+pMsAllocCounter.length))*ProbArraySize));
          // System.out.println("---- counter "+posCounter);
          posCounter+=(int)((((pMsAllocCounter[i]+1)/(problem_.getNumberOfVariables()+pMsAllocCounter.length))*ProbArraySize)); 
            //posCounter+=(int)((((pMsAllocCounter[i])/(problem_.getNumberOfVariables()))*ProbArraySize));
            posCounter=posCounter>ProbArraySize?ProbArraySize:posCounter; //approx due to array index out of bound
            
            for(;index<posCounter;index++){ 
            pMsSelectionProbability[index]=i;
        }
        
             
    }
      /*
        if(index<ProbArraySize){
        
             boundary=index;
             
             for(;index<ProbArraySize;index++){ 
            pMsSelectionProbability[index]=-1;
             }
        }*/
        //System.out.println(Arrays.toString(pMsSelectionProbability));
    
  }
}
