/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.schedulers;


import dps.uibk.ac.at.algorithms.FirstFit;
import dps.uibk.ac.at.energyModels.MigrationModel;
import dps.uibk.ac.at.mOeA.MetaNsgaII;
import dps.uibk.ac.at.mOeA.MoCell;
import dps.uibk.ac.at.mOeA.MoEAlgorithm;
import dps.uibk.ac.at.mOeA.NsgaII;
import dps.uibk.ac.at.mOeA.SoFirstFit;
import dps.uibk.ac.at.mOeA.SoMBDF;
import dps.uibk.mooprov.MOEAGUI.Gui;
import dps.uibk.mooprov.comparator.MigrationComparator;
import dps.uibk.mooprov.comparator.MigrationEnergyComparator;

import dps.uibk.mooprov.iaas.IaaS;
import dps.uibk.mooprov.iaas.PhysicalMachine2;
import dps.uibk.mooprov.iaas.SimulatedMachine;
import dps.uibk.mooprov.iaas.SimulatedMachine.SimulatedMachineException;
import dps.uibk.mooprov.problems.MoeaProvisioning;
import dps.uibk.mooprov.problems.MoeaProvisioningII;
import dps.uibk.mooprov.utility.Agent;
import dps.uibk.mooprov.utility.ChartCreation;
import hu.mta.sztaki.lpds.cloud.simulator.Timed;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;



import jmetal.core.Problem;

import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.util.JMException;
import jmetal.util.Ranking;
import org.apache.log4j.Logger;

/**
 *
 * @author ennio
 */
public class MoeaScheduler extends IaaS.Scheduler {

    // private final ArrayList<onParetoFrontComputation> myEventQueue;
     private ChartCreation chart;
      
     static Logger logger_=Logger.getLogger(MoeaScheduler.class);
     
     private Solution Placement;
             
     private Solution newPlacement;

     private Solution oldPlacement;
     
     private final Object placementLock=new Object();
    
     private MoEAlgorithm MoAlgorithm;
     
     //private static String AlgName; //TODO: unesufull, just for the experiments.

    
     
     private ArrayList<onParetoFrontComputation> schedListeners;
     
    public MoeaScheduler(IaaS s,long delay) throws SchedulingException{
        super(s,delay);
       // myEventQueue=new ArrayList<>();
        Placement=newPlacement=oldPlacement=s.getPlacement();//new Solution();
        schedListeners=new ArrayList<>();
       // setNewPlacement(Placement);
        
       // s.ApplyPlacement(Placement);
        
        //s.setPlacement(Placement);
        
        
        //TODO: decomment this afeter experiments
        runInterface(); //TODO: remove it after experiments
       
       /* DECOMMENT THIS FOR THE EXPERIMENT AGENT !!!!
        Agent.addListener(new Agent.onUserChoise() {

            @Override
            public void actionToDo(Solution s) {
                setNewPlacement(s);
                logger_.debug("new placement= "+s);
                wakeMeUp();
            }
        });
        
       */ 
    }
     
  
    
    
     public static abstract class onParetoFrontComputation{
        
            public abstract void actionToDo(Object s);
            
            public void Execute(){
               
            }
        
        }
    
    @Override
        protected void eventAction() {
         try {
            
                oldPlacement=myIaas.getPlacement();
                Placement=ComputeSchedule();
            
            if(!myIaas.isMigrationProcessAlive()){
            
                myIaas.setPlacement(Placement);
             
                myIaas.ApplyPlacement(Placement);
            
            }
            
            else{
        
            
               //Placement=oldPlacement;
            
                myIaas.setPlacement(oldPlacement);
            
                logger_.error("new placement not applied due to : migration process still in progress ");
            }
            
        }
         catch (Exception ex) {
             // logger_.error(ex+" "+ex.getStackTrace()[0]);
             logger_.error(ex);
             throw new SimulatedMachineException(ex);
         }
        }

    @Override
        public boolean reSubscribe(long freq){
            return isCancelled()? false:subscribe(freq);
        }

    @Override
    public Solution ComputeSchedule() throws SchedulingException {
        
        
        System.out.println("The new placement is going to be computed");
        try{
       /* 
        Problem   problem   ; // The problem to solve
        Algorithm algorithm ; // The algorithm to use
        Operator  crossover ; // Crossover operator
        Operator  mutation  ; // Mutation operator
        Operator  selection ; // Selection operator
    
        HashMap  parameters ; // Operator parameters
    
        QualityIndicator indicators ; // Object to get quality indicators
    
        indicators=null;
        
       
            
         problem=new MoeaProvisioning("Int",myIaas.getgUest(),myIaas.gethOst());
         
         logger_.info("Mo Problem instantiated");
         
        // Solution solution1=new Solution(problem);
        ((MoeaProvisioning)problem).setInitialSched(myIaas.getPlacement());
        //((MoeaProvisioning)problem).setComparator(new MigrationComparator());
        
        ((MoeaProvisioning)problem).setComparator(new MigrationEnergyComparator(myIaas,new MigrationModel()));
    // else
          logger_.info("MoE Problem currectly set up");
          
          
          algorithm = new NSGAII(problem);
          //algorithm = new MOCell(problem);

    // Algorithm parameters
          algorithm.setInputParameter("populationSize",100);
          algorithm.setInputParameter("maxEvaluations",100000);
          
         // algorithm.setInputParameter("archiveSize", 100);
 
    // Mutation and Crossover for Real codification 
          parameters = new HashMap() ;
          parameters.put("probability", 0.9) ;
         
             //parameters.put("distributionIndex", 20.0) ;
             crossover = CrossoverFactory.getCrossoverOperator("SinglePointCrossover", parameters);
        

         parameters = new HashMap() ;
         parameters.put("probability", 1.0/problem.getNumberOfVariables()) ;
        
             //parameters.put("distributionIndex", 20.0) ;
             mutation = MutationFactory.getMutationOperator("BitFlipMutation", parameters);
         

    // Selection Operator 
        parameters = null ;
                                  
             selection = SelectionFactory.getSelectionOperator("BinaryTournament2", parameters) ;
        

    // Add the operators to the algorithm
        algorithm.addOperator("crossover",crossover);
        algorithm.addOperator("mutation",mutation);
        algorithm.addOperator("selection",selection);

    // Add the indicator object to the algorithm
        algorithm.setInputParameter("indicators", indicators) ;
    
    // Execute the Algorithm
        long initTime = System.currentTimeMillis();
        SolutionSet population;
        
        
             population = algorithm.execute();
        
         
        long estimatedTime = System.currentTimeMillis() - initTime;
    
    // Result messages 
        logger_.info("Total execution time: "+estimatedTime + "ms");
        logger_.info("Variables values have been writen to file VAR");
        population.printVariablesToFile("VAR");    
        logger_.info("Objectives values have been writen to file FUN");
        population.printObjectivesToFile("FUN");
    */  
            
       // Problem problem=new MoeaProvisioningII("PlacementII",myIaas.getgUest(),myIaas.gethOst());
        Problem problem=new MoeaProvisioningII("Placement",myIaas.getgUest(),myIaas.gethOst());
        // Problem problem=new MoeaProvisioningII("Int",myIaas.getgUest(),myIaas.gethOst());
         
         logger_.info("Mo Problem instantiated"+myIaas.getPlacement());
         
        // Solution solution1=new Solution(problem);
        ((MoeaProvisioning)problem).setInitialSched(myIaas.getPlacement());
        //((MoeaProvisioning)problem).setComparator(new MigrationComparator());
        
        ((MoeaProvisioning)problem).setComparator(new MigrationEnergyComparator(myIaas,new MigrationModel()));    
        
         logger_.info("Mo algorithm instantiated");
        
          MoAlgorithm=new MetaNsgaII(problem);//DECOMMENT THIS after experiments.
         
         
         // TODO: this is just for the experiments, should be removed
          /*
         Class c = Class.forName(AlgName);
         Constructor<?> cons = c.getConstructor(Problem.class);
         
         
         MoAlgorithm = (MoEAlgorithm)cons.newInstance(problem);
        
        
         //MoAlgorithm=new NsgaII(problem);
        
         //MoAlgorithm=new MoCell(problem);
        // MoAlgorithm=new SoMBDF(problem);
         //MoAlgorithm=new SoFirstFit(problem);
        */
          
        SolutionSet population=MoAlgorithm.getComputedPlacements();
        
       Ranking ranking = new Ranking(population);
       
          
       if(MoAlgorithm instanceof MetaNsgaII || MoAlgorithm instanceof NsgaII)
       ranking.getSubfront(0).printFeasibleFUN("Pareto/ParetoFront"+Timed.getFireCount()) ;
       else
       ranking.getSubfront(0).printObjectivesToFile("Pareto/ParetoSet"+Timed.getFireCount()) ;
        
        logger_.debug("The new placement has been computed");
        
        
        
        //TODO remove after experiments
         chart.updateGraph(population);//update the graph with the current computed solutions
         
        //Agent.updateGraph(population);//TODO: agent only for experiments
        
         
             waitFor();//wait the user choise
        
         notifySchedListener();//notify all the listener 
         
         logger_.debug("The scheduler is awake");
         
         return getNewPlacement();
         
          }catch (InterruptedException | JMException | ClassNotFoundException ex){// |
                 // ClassNotFoundException | InstantiationException |
                 // IllegalAccessException |InvocationTargetException |
                 // IllegalArgumentException |NoSuchMethodException ex) {
              
            throw new SchedulingException(ex);
            
         }
     
    }
    
    
    /*
     public void addListener(onParetoFrontComputation event) {
         myEventQueue.add(event);
    }
    
    public void removeListener(onParetoFrontComputation event) {
         myEventQueue.remove(event);
    }
    
    private void notifyListener(){
    
        for (onParetoFrontComputation choise:myEventQueue){
        
            choise.Execute();        
        }
        
      
    
    } */
   
    private void runInterface(){
    
        chart=new ChartCreation();
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                final Gui applet= new Gui();
                //applet.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                //applet.add(c.prepareAndShowChart());
                applet.addJChartpanel(chart.prepareAndShowCPUChart(),chart.prepareAndShowMeMChart());
                applet.pack();
                applet.setVisible(true);
               
                //c.update(population2);
            }
        });
        
        chart.addListener(new ChartCreation.onUserChoise() {

            @Override
            public void actionToDo(Solution s) {
                setNewPlacement(s);
                logger_.debug("new placement= "+s);
                wakeMeUp();
            }
        });
    
    }
    
    public void waitFor() throws InterruptedException{
    
        synchronized(this){
            logger_.debug("The scheduler waits for the user response");
            this.wait();
        }
    }
    
    
    public void wakeMeUp(){
    
        synchronized(this){
           
            this.notify();
        }
    
    }
    
    private Solution getNewPlacement() {
        
        synchronized(placementLock){
        return newPlacement;
        }
    }

    private void setNewPlacement(Solution s) {
        synchronized(placementLock){
           this.newPlacement = s;
        }
        
    }
    
    public void addSchedListener(onParetoFrontComputation e){

        schedListeners.add(e);
    
    }
    
    public void removeSchedListener(onParetoFrontComputation e){

        schedListeners.remove(e);
    
    }
    
    public void notifySchedListener(){

        for (onParetoFrontComputation e:schedListeners){
        
            e.actionToDo(null);
        }
         
    
    }
     
    //TODO: remove this!!!!
    
    public static void setAlgName(String algName) {
       // AlgName = algName;
    }
}
