/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.meters;

import dps.uibk.ac.at.energyModels.PMpowerModel;
import dps.uibk.mooprov.iaas.IaaS;
import dps.uibk.mooprov.iaas.PhysicalMachine2;
import dps.uibk.mooprov.iaas.SimulatedMachine;
import dps.uibk.mooprov.iaas.VirtualMachine;
import dps.uibk.mooprov.utility.PrintCsv;
import hu.mta.sztaki.lpds.cloud.simulator.Timed;

/**
 *
 * @author ennio
 */
public class IaasStatistics {
    
    private IaaS iaas;
    private final  SimulatedMachine machine;
    private final PrintCsv Print;
    private final SimulatedMachine.MachineEvents meter;
    private final PMpowerModel pwModel;
    
     public IaasStatistics(IaaS Iaas,long samplingRate) {
          
            if(Iaas!=null && !Iaas.gethOst().isEmpty()&&!Iaas.getgUest().isEmpty()){
                this.iaas=Iaas;
                machine=Iaas;
                pwModel=new PMpowerModel();
            } else{
                throw new UnsupportedOperationException("Specify a valid Iaas");
            }
            
           //System.out.println(host);
            Print=new PrintCsv();
            
            meter = new SimulatedMachine.MachineEvents(samplingRate){

               
                @Override
                 protected void eventAction() {
                   int running=0;
                   
                    for (PhysicalMachine2 pm : iaas.gethOst()) {
                        
                        
                        
                        if(pm.isRunning()){
                        running+=1;
                        }
                    }
                  //  host=VirtualMachine.getMyHost();//the machine can change host during its lifecycle
                   // System.out.println("my host is-"+host.getIndex());
                       double [] ovH=getIaasUtilizationsInfo();
                       double []Slas=getOverallSLAViolation();
                       
                       Print.AddToFile("meters/"+"IaasStatistics.csv").Line(Timed.getFireCount()+" "+running+" "+
                                 iaas.getPlacement().getObjective(2)+" "+
                                ovH[0]/running+" "+ovH[1]/running+" "+
                                ovH[2]/running+" "+ovH[3]/running+" "+
                                Slas[0]+" "+Slas[1]+" "+
                                ovH[4]); //To change body of generated methods, choose Tools | Templates.
                    //System.out.println("update perf fire time ="+ Timed.getFireCount());
                    
                 }

                @Override
                public void cancel() {
                    super.cancel(); //To change body of generated methods, choose Tools | Templates.
            
                    Print.close();
                }
                 
                         
                  };
            
            
        }
     
     public void runSlaMeter(long samplingRate){
    
       
         
            try{
                
                machine.registerEvents(meter);
               
        
         }
            catch(NullPointerException | SimulatedMachine.SimulatedMachineException e){
            
                System.err.println(e);
            }
         }
        
    
    public void StopSLAMeter(){
    
        machine.deRegisterEvent(meter);
    
    }
    
    public double[] getIaasUtilizationsInfo(){
    
        double[] ovH=new double[5];
        
        for (PhysicalMachine2 pm:iaas.gethOst()){
        
            ovH[0]+=pm.getMemoryRequested()/pm.getMemoryCapacity();//memory overhead
            ovH[1]+=((double) pm.getvCPUsRequested())/(pm.getVCPUs());//cpu overhead
            ovH[2]+=pm.getOverallMachineCPUUtilization();//cpu utilization
            ovH[3]+=pm.getOverallMachineMemoryUtilization();//memory utilization
            ovH[4]+=pwModel.getPowerConsumption(pm);
                    
        }
      
       
        return ovH;
    }
    
    public double [] getOverallSLAViolation(){

        double[] ovH=new double[2];

        for (PhysicalMachine2 pm:iaas.gethOst()){
        
            for (VirtualMachine vm:pm.getAllocatedVMs()){
        
                ovH[0]+=ComputeCPUSLAViolation(pm, vm);
                ovH[1]+=ComputeMemorySLAViolation(pm,vm);
            }
        }
        return ovH;
}
    
     private double ComputeCPUSLAViolation(PhysicalMachine2 pm,VirtualMachine vm){
    
        return (pm.getCPUUtilization(vm)-pm.getRealCPUUtilization(vm)>0.0)?1:0;
        
    
    }
    
    private double ComputeMemorySLAViolation(PhysicalMachine2 pm,VirtualMachine vm){
    
        return (pm.getMEMUtilization(vm)-pm.getRealMEMUtilization(vm)>0.001)?1:0;
    }
}
