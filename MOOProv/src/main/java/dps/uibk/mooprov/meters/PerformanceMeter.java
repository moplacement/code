/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.meters;

import dps.uibk.mooprov.iaas.PhysicalMachine2;
import dps.uibk.mooprov.iaas.SimulatedMachine;
import dps.uibk.mooprov.iaas.VirtualMachine;
import dps.uibk.mooprov.utility.PrintCsv;
import hu.mta.sztaki.lpds.cloud.simulator.Timed;
import java.util.Iterator;



/**
 *
 * @author ennio
 */
public class PerformanceMeter {

    private PhysicalMachine2 host;
    private final VirtualMachine VirtualMachine;
    private final PrintCsv Print;
    private final SimulatedMachine machine;
    private final SimulatedMachine.MachineEvents meter;
    private final double initTime;
   
    
    public PerformanceMeter(VirtualMachine Guest,long samplingRate) {
          
            if(Guest!=null){
                VirtualMachine=Guest;
                machine=Guest;
                
            } else{
                throw new UnsupportedOperationException("Specify a Virtual machine");
            }
            
            host=Guest.getMyHost();
            initTime=Timed.getFireCount();
            //System.out.println(host);
            Print=new PrintCsv();
            
          
            
            meter = new SimulatedMachine.MachineEvents(samplingRate){

                @Override
                 protected void eventAction() {
                    
                    host=VirtualMachine.getMyHost();//the hosting machine can change host during vm lifecycle
                   // System.out.println("my host is-"+host.getIndex());
                   double memoryDivider,cpuDivider;
            
                     memoryDivider=host.getMemoryCapacity()/VirtualMachine.getVMemory();
                     cpuDivider=host.getVCPUs()/VirtualMachine.getVCPUs();
                    
                    Print.AddToFile("meters/"+VirtualMachine.getIndex()+
                            "SLAViolation.csv").Line(Timed.getFireCount()+
                            " "+(host.getCPUUtilization(VirtualMachine)*cpuDivider)+
                            " "+(host.getRealCPUUtilization(VirtualMachine)*cpuDivider)+
                            " "+(host.getMEMUtilization(VirtualMachine)*memoryDivider)+
                            " "+(host.getRealMEMUtilization(VirtualMachine)*memoryDivider)+
                            " "+ComputeCPUSLAViolation(host,VirtualMachine)+
                            " "+ComputeMemorySLAViolation(host,VirtualMachine)+
                            " "+host.getIndex()); //To change body of generated methods, choose Tools | Templates.
                    //System.out.println("update perf fire time ="+ Timed.getFireCount());
                }

                @Override
                public void cancel() {
                    super.cancel(); //To change body of generated methods, choose Tools | Templates.
                    //System.out.println("i was closed");
                    Print.close();
                }
                 
                         
                  };
            
            
        }
        
    
         public PerformanceMeter(PhysicalMachine2 host,long samplingRate) {
          
            if(host!=null){
                this.host=host;
            } else{
                throw new UnsupportedOperationException("Specify a Virtual machine");
            }
            
            machine=host;
            //System.out.println(host);
            Print=new PrintCsv();
            
            meter = new SimulatedMachine.MachineEvents(samplingRate){

                @Override
                 protected void eventAction() {
                    
                  //the machine can change host during its lifecycle
                    double totalCpu=0,totalMem=0;
                    
                    
                    
                        totalCpu=PerformanceMeter.this.host.getOverallMachineCPUUtilization();
                        totalMem=PerformanceMeter.this.host.getOverallMachineMemoryUtilization();
                    
                    
                   
                    
                     Print.AddToFile("meters/"+PerformanceMeter.this.host.getIndex()+initTime+"SLA.csv").Line(Timed.getFireCount()+" "+totalCpu+" "+totalMem);
                    
                }

                @Override
                public void cancel() {
                    super.cancel(); //To change body of generated methods, choose Tools | Templates.
            
                    Print.close();
                }
                 
                         
                  };
            VirtualMachine=null;
            initTime=Timed.getFireCount();
        }
        
    
    public void runSlaMeter(long samplingRate){
    
       
         
            try{
                
                machine.registerEvents(meter);
        
        
         }
            catch(NullPointerException | SimulatedMachine.SimulatedMachineException e){
            
                System.err.println(e);
            }
         }
        
    
    public void StopSLAMeter(){
    
        machine.deRegisterEvent(meter);
    
    }

    private double ComputeCPUSLAViolation(PhysicalMachine2 pm,VirtualMachine vm){
    
        return pm.getCPUUtilization(vm)-pm.getRealCPUUtilization(vm);
        
    
    }
    
    private double ComputeMemorySLAViolation(PhysicalMachine2 pm,VirtualMachine vm){
    
        return pm.getMEMUtilization(vm)-pm.getRealMEMUtilization(vm);
        
    
    }
    
}
