/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.meters;

import dps.uibk.mooprov.iaas.PhysicalMachine2;
import dps.uibk.mooprov.iaas.SimulatedMachine;
import dps.uibk.mooprov.iaas.VirtualMachine;
import dps.uibk.mooprov.utility.PrintCsv;
import hu.mta.sztaki.lpds.cloud.simulator.Timed;

/**
 *
 * @author ennio
 */
public class PowerMeters {
    
    
    private PhysicalMachine2 host;
    private final VirtualMachine VirtualMachine;
    private final PrintCsv Print;
    private final SimulatedMachine machine;
    private final SimulatedMachine.MachineEvents meter;
    private final double initTime;
   
    
    public PowerMeters(VirtualMachine Guest,long samplingRate) {
          
            if(Guest!=null){
                VirtualMachine=Guest;
                machine=Guest;
                
            } else{
                throw new UnsupportedOperationException("Specify a Virtual machine");
            }
            
            
            initTime=Timed.getFireCount();
            //System.out.println(host);
            Print=new PrintCsv();
            
            meter = new SimulatedMachine.MachineEvents(samplingRate){

                @Override
                 protected void eventAction() {
                    
                    host=VirtualMachine.getMyHost();//the hosting machine can change host during vm lifecycle
                   // System.out.println("my host is-"+host.getIndex());
                    Print.AddToFile("meters/"+VirtualMachine.getIndex()+"P.csv").Line(Timed.getFireCount()+" "+host.getCPUUtilization(VirtualMachine)+" "+host.getRealCPUUtilization(VirtualMachine)+" "+host.getMEMUtilization(VirtualMachine)+" "+host.getRealMEMUtilization(VirtualMachine)); //To change body of generated methods, choose Tools | Templates.
                    //System.out.println("update perf fire time ="+ Timed.getFireCount());
                }

                @Override
                public void cancel() {
                    super.cancel(); //To change body of generated methods, choose Tools | Templates.
                    //System.out.println("i was closed");
                    Print.close();
                }
                 
                         
                  };
            
            
        }
        
    

    public void StopSLAMeter(){
    
        machine.deRegisterEvent(meter);
    
    }

    


}
