/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.problems;

/**
 *
 * @author ennio
 */

import java.util.Arrays;
import java.util.List;
import jmetal.core.Solution;
import jmetal.core.Variable;
import jmetal.util.JMException;
import org.apache.log4j.Logger;
//import dps.uibk.ac.at.energyModels.PMpowerModel;
/**
 *
 * @author ennio
 */
    public class MoeaProvisioningII extends MoeaProvisioning {
    
    static Logger logger = Logger.getLogger(MoeaProvisioningII.class);

       
    
    
    public MoeaProvisioningII(String solutionType,List vMList,List pMList) {
        
     super(solutionType, vMList, pMList);
    
     obj_Names=Arrays.asList("OvercommitmentRatio","ResourcesWastage","Migrations");
    }
    
    
    @Override
    public void evaluateConstraints(Solution solution) throws JMException {
       
       Variable[] variable = solution.getDecisionVariables();
        
       double totalViolation=0;
       int numOfViolation=0;
       
       
      
        
       int pMindex; 
        
       double [] cPUconstraint = new double[this.numberOfConstraints_];
       double [] meMconstraint = new double[this.numberOfConstraints_];
       
      // double [] vCPUconstraint = new double[this.numberOfConstraints_];
      // double [] vMeMconstraint = new double[this.numberOfConstraints_];
       
       for(int vMindex=0;vMindex<variable.length;vMindex++){
           //System.out.println(vMList.get(vMindex).getIndex());
          // System.out.println("ok"+variable.length);
          // System.out.println(variable[vMindex].getValue());
           pMindex=(int) variable[vMindex].getValue();
          
           //System.out.println(pMindex);
          // cPUconstraint[pMindex]+=pMList.get(pMindex).getCPUUtilization(vMList.get(vMindex));
           
           cPUconstraint[pMindex]+=pMList.get(pMindex).forecastCPUUtilization(vMList.get(vMindex));
      //     vCPUconstraint[pMindex]+=(vMList.get(vMindex).getVCPUs());
           
          // meMconstraint[pMindex]+=pMList.get(pMindex).getMEMUtilization(vMList.get(vMindex));
          
           
           meMconstraint[pMindex]+=pMList.get(pMindex).forecastMEMUtilization(vMList.get(vMindex));
       //    vMeMconstraint[pMindex]+=(vMList.get(vMindex).getVMemory());
        }
       
       for(int pM=0;pM<pMList.size();pM++){
       
         
           
           if(cPUconstraint[pM]>1){
               

                //Uncomment this line to considering the scheduling ovh
               //totalViolation+=1-(cPUconstraint[pM]+pMList.get(pM).getSchedulingOverhead((int)vCPUconstraint[pM]));
               totalViolation+=(1-(cPUconstraint[pM]));
               //System.out.println("CPU usage Violation PM-"+pM+" = "+(cPUconstraint[pM]-1));
               numOfViolation++;
           }
         
           if(meMconstraint[pM]>1){
               totalViolation+=(1-(meMconstraint[pM]));//-1;
               numOfViolation++;
           }
         
       }
       
       solution.setOverallConstraintViolation(0);
       solution.setNumberOfViolatedConstraint(0);
    }
    
    
    @Override
    public void evaluate(Solution sltn) throws JMException {
        Variable[] variable = sltn.getDecisionVariables();
        double [] cPUusage = new double[this.numberOfPhysicalMachines];
        double [] meMusage = new double[this.numberOfPhysicalMachines];
        
        double [] CPUOvercommittment = new double[this.numberOfPhysicalMachines];
        double [] MemoryOvercommittment = new double[this.numberOfPhysicalMachines];
        
       // int totalPMused;
        double totalMemUsage=0,totalCpusUsage=0,
                totalOvercommittment=0,
                totalCpusviolation=0,totalMemoryviolation=0,totalEnergy=0;
        double numOfMigrations=0;
        
  //      PMpowerModel pwr= new PMpowerModel();
        int pMindex; 
        for(int vMindex=0;vMindex<variable.length;vMindex++){
        
            
           pMindex= (int) variable[vMindex].getValue();
           /*
            cPUusage[pMindex]+=pMList.get(pMindex).getCPUUtilization(vMList.get(vMindex));
            meMusage[pMindex]+=pMList.get(pMindex).getMEMUtilization(vMList.get(vMindex));
           
           */
         
           
           cPUusage[pMindex]+=pMList.get(pMindex).forecastCPUUtilization(vMList.get(vMindex));
           meMusage[pMindex]+=pMList.get(pMindex).forecastMEMUtilization(vMList.get(vMindex));
           
           MemoryOvercommittment[pMindex]+=(vMList.get(vMindex).getVMemory());
           CPUOvercommittment[pMindex]+=((vMList.get(vMindex).getVCPUs()));
           
           //TotalCpusUsage+=pMList.get(pMindex).getCPUUtilization(vMList.get(vMindex));
           //TotalMemUsage+=pMList.get(pMindex).getMEMUtilization(vMList.get(vMindex));
        }
        
        double PMcounter=0;
        for (int i=0;i<numberOfPhysicalMachines;i++){
        
            if(cPUusage[i]>0 && meMusage[i]>0){
            
                
            //TotalMemUsage+=Math.exp();
                //decomment this line to evaluate the scheduling overhead   
           // totalCpusUsage+=(1-(cPUusage[i]+pMList.get(i).getSchedulingOverhead((int)vCPUallocated[i])));//*(pMList.get(i).getVCPUs()/VCPUshare[i]);
                //decomment these lines to get pareto front without overcommitment 
               //totalCpusUsage+=Math.pow(getGreaterThanZero(1D-(cPUusage[i])),2);
               //totalMemUsage+=Math.pow(getGreaterThanZero(1D-meMusage[i]),2);
               
               totalCpusUsage+=Math.pow(getGreaterThanZero(1D-(cPUusage[i])),2);
               totalMemUsage+=Math.pow(getGreaterThanZero(1D-meMusage[i]),2);
               
                totalCpusviolation+=getLowerrThanZero(1D-cPUusage[i]);
                totalMemoryviolation+=getLowerrThanZero(1D-meMusage[i]);


              // totalEnergy+=pwr.getPowerConsumption(pMList.get(i).getIdlePWR(),
              //  pMList.get(i).getMaxPWR(),cPUusage[i]);
             
                MemoryOvercommittment[i]=MemoryOvercommittment[i]/(pMList.get(i).getMemoryCapacity());
                CPUOvercommittment[i]=CPUOvercommittment[i]/(pMList.get(i).getVCPUs());
               //  totalOvercommittment+=((CPUOvercommittment[i]));
                totalOvercommittment+=((MemoryOvercommittment[i]+CPUOvercommittment[i])/2D);
                PMcounter++;
            }
        
        }
        
      //  System.out.println("total PMCounter -"+PMcounter);
       // System.out.println("migration number -"+numOfMigrations);
    //    System.out.println("totalEnergy -"+totalEnergy);
        numOfMigrations=solComparator.compare(initialSched,sltn);
        //numOfMigrations+=totalEnergy;
        
       // sltn.setObjective(0,totalCpusUsage);
        //sltn.setObjective(0,Math.sqrt(totalCpusUsage));
       sltn.setObjective(0,Math.sqrt(totalCpusUsage+totalMemUsage-3*(totalCpusviolation+totalMemoryviolation)));
       sltn.setObjective(1,(totalOvercommittment/PMcounter));
       
        //double minmig=(1.0/numOfMigrations)*100;
        sltn.setObjective(2,numOfMigrations);
    }
    
    double getLowerrThanZero(double a){
    
        return a<0?a:0;
    }
}

