/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.problems;



import dps.uibk.mooprov.iaas.PhysicalMachine2;
import dps.uibk.mooprov.iaas.VirtualMachine;
import dps.uibk.mooprov.comparator.MigrationComparator;
import dps.uibk.mooprov.solutionType.IntSolutionTypeP;
import dps.uibk.mooprov.solutionType.Placement;
import dps.uibk.mooprov.solutionType.PlacementII;
import java.util.Arrays;

import java.util.Comparator;
import java.util.List;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.Variable;
import jmetal.encodings.solutionType.IntSolutionType;
import jmetal.util.JMException;
import org.apache.log4j.Logger;

/**
 *
 * @author ennio
 */
    public class MoeaProvisioning extends Problem {
    
    static Logger logger = Logger.getLogger(MoeaProvisioning.class);
    protected int numberOfPhysicalMachines;
    protected int numberOfVirtulaMachines;
    protected List<VirtualMachine> vMList; 
    protected List<PhysicalMachine2> pMList;
    protected double threshold;
    protected Solution initialSched;

    protected List<String> obj_Names;
  
   
    protected Comparator solComparator;

    public MoeaProvisioning(String solutionType,List vMList,List pMList,Solution sl) {
        this(solutionType, vMList, pMList);
        this.initialSched=sl;
        solComparator=new MigrationComparator();
        
    }
    
    
    
    
    public MoeaProvisioning(String solutionType,List vMList,List pMList) {
        
    if((vMList==null||pMList==null)&&(!vMList.isEmpty()||!pMList.isEmpty())){
    
        System.out.println("Error: Lists Empty " ) ;
    	System.exit(-1) ;
    
    }     
    
    this.vMList=vMList;
    this.pMList=pMList;
    
    threshold=30;
    
    numberOfPhysicalMachines=pMList.size();
    numberOfVirtulaMachines=vMList.size();
    
    numberOfVariables_  = numberOfVirtulaMachines;
    numberOfObjectives_ = 3;
    
    obj_Names=Arrays.asList("MemoryWastage","CpuWastage","Migrations");
    
    numberOfConstraints_= numberOfPhysicalMachines;
    problemName_        = "MoeaProvisioning";
        
    upperLimit_ = new double[numberOfVariables_];
    lowerLimit_ = new double[numberOfVariables_];
    for (int var = 0; var < numberOfVariables_; var++){
      lowerLimit_[var] = 0;
      upperLimit_[var] = numberOfPhysicalMachines-1;
    } // for
        
    if (solutionType.compareTo("Int") == 0){
    	solutionType_ = new IntSolutionType(this) ;
        logger.info(solutionType);
    }
    else if (solutionType.compareTo("Placement") == 0){
    	solutionType_ = new Placement(this) ;
      //  System.out.println("Error: solution type " + initialSched + " invalid") ;
        //((Placement)solutionType_).setParent(initialSched);
    }
    
    else if (solutionType.compareTo("PlacementII") == 0){
    	solutionType_ = new PlacementII(this) ;
     //   System.out.println("Error: solution type " + initialSched + " invalid") ;
        //((Placement)solutionType_).setParent(initialSched);       
     }
    
    else if (solutionType.compareTo("IntP") == 0){
    	solutionType_ = new IntSolutionTypeP(this) ;
      //  System.out.println("Error: solution type " + initialSched + " invalid") ;
        //((Placement)solutionType_).setParent(initialSched);       
     } 
    else {
    	System.out.println("Error: solution type " + solutionType + " invalid") ;
    	System.exit(-1) ;
    }      
    
    }
    @Override
    public void evaluateConstraints(Solution solution) throws JMException {
       
       Variable[] variable = solution.getDecisionVariables();
        
       double totalViolation=0;
       int numOfViolation=0;
       
       
       double cPUThreshold,memThreshold;
        
       int pMindex; 
        
       double [] cPUconstraint = new double[this.numberOfConstraints_];
       double [] meMconstraint = new double[this.numberOfConstraints_];
       
       double [] vCPUconstraint = new double[this.numberOfConstraints_];
       double [] vMeMconstraint = new double[this.numberOfConstraints_];
       
       for(int vMindex=0;vMindex<variable.length;vMindex++){
           //System.out.println(vMList.get(vMindex).getIndex());
          // System.out.println("ok"+variable.length);
          // System.out.println(variable[vMindex].getValue());
           pMindex=(int) variable[vMindex].getValue();
          
           //System.out.println(pMindex);
          // cPUconstraint[pMindex]+=pMList.get(pMindex).getCPUUtilization(vMList.get(vMindex));
           
           cPUconstraint[pMindex]+=pMList.get(pMindex).forecastCPUUtilization(vMList.get(vMindex));
           vCPUconstraint[pMindex]+=(vMList.get(vMindex).getVCPUs());
           
          // meMconstraint[pMindex]+=pMList.get(pMindex).getMEMUtilization(vMList.get(vMindex));
          
           
           meMconstraint[pMindex]+=pMList.get(pMindex).forecastMEMUtilization(vMList.get(vMindex));
           vMeMconstraint[pMindex]+=(vMList.get(vMindex).getVMemory());
        }
       
       for(int pM=0;pM<pMList.size();pM++){
       
           cPUThreshold=threshold*pMList.get(pM).getVCPUs();
           memThreshold=threshold*pMList.get(pM).getMemoryCapacity();
           
           if(cPUconstraint[pM]>1.2){
               

                //Uncomment this line to considering the scheduling ovh
               //totalViolation+=1-(cPUconstraint[pM]+pMList.get(pM).getSchedulingOverhead((int)vCPUconstraint[pM]));
               totalViolation+=1-(cPUconstraint[pM]);
               //System.out.println("CPU usage Violation PM-"+pM+" = "+(cPUconstraint[pM]-1));
               numOfViolation++;
           }
           if(vCPUconstraint[pM]>cPUThreshold){
               totalViolation+=(cPUThreshold-vCPUconstraint[pM])/pMList.get(pM).getVCPUs();
              // System.out.println("VCPU allocation Violation PM-"+pM+" = "+vCPUconstraint[pM]);
               numOfViolation++;
           }
           if(meMconstraint[pM]>1.3){
               totalViolation+=1-meMconstraint[pM];//-1;
               numOfViolation++;
           }
           if(vMeMconstraint[pM]>memThreshold){
               totalViolation+=(memThreshold-vMeMconstraint[pM])/pMList.get(pM).getMemoryCapacity();
               numOfViolation++;
           }
       }
       
       solution.setOverallConstraintViolation(totalViolation);
       solution.setNumberOfViolatedConstraint(numOfViolation);
    }
    
    
    @Override
    public void evaluate(Solution sltn) throws JMException {
        Variable[] variable = sltn.getDecisionVariables();
        double [] cPUusage = new double[this.numberOfPhysicalMachines];
        double [] meMusage = new double[this.numberOfPhysicalMachines];
        
        double [] vCPUallocated = new double[this.numberOfPhysicalMachines];
        double [] vMeMallocated = new double[this.numberOfPhysicalMachines];
        
       // int totalPMused;
        double totalMemUsage=0,totalCpusUsage=0;
        int numOfMigrations=0;
        
        int pMindex; 
        for(int vMindex=0;vMindex<variable.length;vMindex++){
        
            
           pMindex= (int) variable[vMindex].getValue();
           /*
            cPUusage[pMindex]+=pMList.get(pMindex).getCPUUtilization(vMList.get(vMindex));
            meMusage[pMindex]+=pMList.get(pMindex).getMEMUtilization(vMList.get(vMindex));
           
           */
         
           
           cPUusage[pMindex]+=pMList.get(pMindex).forecastCPUUtilization(vMList.get(vMindex));
           meMusage[pMindex]+=pMList.get(pMindex).forecastMEMUtilization(vMList.get(vMindex));
           
           vMeMallocated[pMindex]+=(vMList.get(vMindex).getVMemory());
           vCPUallocated[pMindex]+=((vMList.get(vMindex).getVCPUs()));
           
           //TotalCpusUsage+=pMList.get(pMindex).getCPUUtilization(vMList.get(vMindex));
           //TotalMemUsage+=pMList.get(pMindex).getMEMUtilization(vMList.get(vMindex));
        }
        
        for (int i=0;i<numberOfPhysicalMachines;i++){
        
            if(cPUusage[i]>0 && meMusage[i]>0){
            //TotalCpusUsage+=Math.exp(cPUusage[i])+Math.exp(meMusage[i]);
            //TotalMemUsage+=Math.exp(meMusage[i]);
                //decomment this line to evaluate the scheduling overhead   
           // totalCpusUsage+=(1-(cPUusage[i]+pMList.get(i).getSchedulingOverhead((int)vCPUallocated[i])));//*(pMList.get(i).getVCPUs()/VCPUshare[i]);
              //decomment these lines to get pareto front without overcommitment 
               // totalCpusUsage+=Math.pow(getGreaterThanZero(1D-(cPUusage[i])),2);
               // totalMemUsage+=Math.pow(getGreaterThanZero(1D-meMusage[i]),2);
                 totalCpusUsage+=(Math.pow(getGreaterThanZero(1D-(cPUusage[i])),2)*100/(vCPUallocated[i]/pMList.get(i).getVCPUs()));
                 totalMemUsage+=(Math.pow(getGreaterThanZero(1D-meMusage[i]),2)*100/(vMeMallocated[i]/pMList.get(i).getMemoryCapacity()));
            }
            
           
            
             
        }
        
       // System.out.println("total cpu usage -"+totalCpusUsage);
       // System.out.println("migration number -"+numOfMigrations);
       // System.out.println("migration number -"+totalMemUsage);
        numOfMigrations=solComparator.compare(initialSched,sltn);
        
        sltn.setObjective(0,Math.sqrt(totalCpusUsage));
        sltn.setObjective(1,Math.sqrt(totalMemUsage));
        //double minmig=(1.0/numOfMigrations)*100;
        sltn.setObjective(2,numOfMigrations);
    }
    
    
     public void setInitialSched(Solution initialSched) {
        this.initialSched = initialSched;
        
        if(solutionType_ instanceof Placement){
        ((Placement)solutionType_).setParent(initialSched);}
        
        if(solutionType_ instanceof PlacementII){
        ((PlacementII)solutionType_).setParent(initialSched);}
        
        if(solutionType_ instanceof IntSolutionTypeP){
        ((IntSolutionTypeP)solutionType_).setParent(initialSched);}
        
    }
     
     public void setComparator(Comparator cmp) {
        this.solComparator = cmp;
    }
     
     public Comparator getComparator() {
        return this.solComparator ;
    }
     
     
    public List<VirtualMachine> getvMList() {
        return vMList;
    }

    public List<PhysicalMachine2> getpMList() {
        return pMList;
    }

    public Solution getInitialSched() {
        return initialSched;
    }
    
    protected double getGreaterThanZero(double a){
    
        return a>0?a:0;
    }

}
