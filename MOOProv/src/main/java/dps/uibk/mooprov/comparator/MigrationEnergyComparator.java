/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.comparator;

import dps.uibk.ac.at.energyModels.MigrationModel;
import dps.uibk.mooprov.iaas.IaaS;
import dps.uibk.mooprov.iaas.PhysicalMachine2;
import dps.uibk.mooprov.iaas.VirtualMachine;
import java.util.Comparator;
import java.util.logging.Level;
import jmetal.core.Solution;
import jmetal.core.Variable;
import jmetal.util.JMException;
import org.apache.log4j.Logger;

/**
 *
 * @author ennio
 */
public class MigrationEnergyComparator implements Comparator{

    public MigrationEnergyComparator(IaaS myiAAS,MigrationModel mM) {
        this.iaas=myiAAS;
        this.mM=mM;
    }
    
    static Logger logger = Logger.getLogger(MigrationComparator.class);
    private final IaaS iaas;
    private final MigrationModel mM;
    
    /**
     *this method returns the energy consumption due to migration of VMs when moving from placement t to Placement t1
     * 
     * @param t old placement
     * @param t1 new placement
     * @return migrations energy consumption
     */
    
    
    
    @Override
    public int compare(Object t, Object t1) {
        Solution s1 =(Solution)t;
        Solution s2 =(Solution)t1;
        
        Variable variables1[]=s1.getDecisionVariables();
        Variable variables2[]=s2.getDecisionVariables();
        
        PhysicalMachine2 source;
        PhysicalMachine2 target;
        VirtualMachine vm;
        
       int migrations=0;
       
       if(variables1==null||variables2==null)
           return migrations;
        
        if(variables1.length>variables2.length){
         
           
        for(int i=0;i<variables2.length;i++){
        
            try {
                
                
                if(!(variables2[i].getValue()==variables1[i].getValue())){
                    
                    source=iaas.gethOst().get((int)variables1[i].getValue());
                    target=iaas.gethOst().get((int)variables2[i].getValue());
                    vm=iaas.getgUest().get(i);
                    migrations +=(int)mM.getEnergyConsumption(vm, source, target);
                }
            } catch (JMException ex) {
                logger.warn("Comparator has filed to compare the two solutions");
               return -1;
            } catch (Exception ex) {
                logger.warn("Comparator has filed to compare the two solutions");
            
                return -1;
            }
              
        }
        
        }
        
        
        else{
        
            
            for(int i=0;i<variables1.length;i++){
        
            try {
                
                
                if(!(variables1[i].getValue()==variables2[i].getValue())){
                    
                    source=iaas.gethOst().get((int)variables1[i].getValue());
                    target=iaas.gethOst().get((int)variables2[i].getValue());
                    vm=iaas.getgUest().get(i);
                    migrations +=(int)mM.getEnergyConsumption(vm, source, target);
                }
            } catch (Exception  ex) {
                logger.warn("Comparator has filed to compare the two solutions");
               return -1;
            }
              
        }
        
        
        }
        
        
        return migrations;
    }
    
}
