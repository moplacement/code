/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.comparator;

import java.util.Comparator;
import jmetal.core.Solution;
import jmetal.core.Variable;
import jmetal.util.JMException;
import org.apache.log4j.Logger;

/**
 *
 * @author ennio
 */
public class MigrationComparator implements Comparator{
    
    static Logger logger = Logger.getLogger(MigrationComparator.class);

    @Override
    public int compare(Object t, Object t1) {
        Solution s1 =(Solution)t;
        Solution s2 =(Solution)t1;
        
        Variable variables1[]=s1.getDecisionVariables();
        Variable variables2[]=s2.getDecisionVariables();
        
       int migrations=0;
       
       if(variables1==null||variables2==null)
           return migrations;
        
        if(variables1.length>variables2.length){
          Variable temp[]=variables2;
            variables2=variables1;
            variables1=temp;
        }
           
        for(int i=0;i<variables1.length;i++){
        
            try {
                if(!(variables1[i].getValue()==variables2[i].getValue())){
                    
                    migrations++;
                }
            } catch (JMException ex) {
                logger.warn("Comparator has filed to compare the two solutions");
               return -1;
            }
              
        }
        
        
        return migrations;
    }
    
}
