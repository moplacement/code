/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.exec;

import dps.uibk.ac.at.Factories.PmFactory;
import dps.uibk.ac.at.energyModels.MigrationModel;
import dps.uibk.ac.at.mOeA.SoFirstFit;
import dps.uibk.mooprov.comparator.MigrationEnergyComparator;
import dps.uibk.mooprov.iaas.IaaS;
import dps.uibk.mooprov.iaas.VirtualMachine;
import dps.uibk.mooprov.meters.IaasStatistics;
import dps.uibk.mooprov.problems.MoeaProvisioning;
import dps.uibk.mooprov.schedulers.MoeaScheduler;
import dps.uibk.mooprov.workload.Workload;
import hu.mta.sztaki.lpds.cloud.simulator.Timed;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import jmetal.core.Solution;
import org.apache.log4j.Logger;

/**
 *
 * @author ennio
 */
public class evoexamples {

    private static final long MAXSIMULATIONTIME=10000;
    private static final long MAXLOADTIME=7200;
    private static final Logger logger_=Logger.getLogger(ResourceSaturationExp.class);
    
     
     /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, Exception {
        // TODO code application logic here
        
        IaaS Provider=new IaaS(MoeaScheduler.class);
        
        //datacenter Physical infrastructure creation
        
        int DCsize=3;
        
        //creates 10 PMs Xeon-E-2690  
        for (int i=0;i<(DCsize);i++){
         
        Provider.addHost(PmFactory.Pm("Xeon-E-2690"));//add PMs servers with the same characterisics
     } 
        
        double OverallProviderCPUs=(DCsize-1)*Provider.gethOst().get(0).getVCPUs();
        double OverallProviderMemory=(DCsize-1)*Provider.gethOst().get(0).getMemoryCapacity();
        // VMs creation
        
        String Type;
        double seed=0;
        
        
        VirtualMachine vm;
        Workload application;
    
        ArrayList<VirtualMachine> VmtobeAllocated=MoScheduler.GetVMs(true,10);
        int counter=0;
         do{
        
            vm=VmtobeAllocated.get(counter);
            Provider.addGuest(VmtobeAllocated.get(counter));
            OverallProviderCPUs-=vm.getVCPUs();
            OverallProviderMemory-=vm.getVMemory();
            counter++;
        }
        while(OverallProviderCPUs>=0&&OverallProviderMemory>=0);
         
         
        MoeaProvisioning problem=new MoeaProvisioning("Int",Provider.getgUest(), Provider.gethOst());
        
        problem.setComparator(new MigrationEnergyComparator(Provider,new MigrationModel()));    
        
        SoFirstFit instance = new SoFirstFit(problem);
        
        //Solution initialP1=new Solution(new MoeaProvisioning("Int",InitialvmList, pmList));
        
        Solution initialP=instance.getComputedPlacements().get(0);
        
        System.out.println("total nuber of PMs ("+Provider.gethOst().size()+") ");
        
        System.out.println("IaaS ("+Provider.getgUest().size()+") VMs");
        
        logger_.info("Initial solution-"+Arrays.toString(initialP.getDecisionVariables()));
       // System.out.println(Arrays.toString(pMsSelectionProbability));
        
        Provider.ApplyPlacement(initialP);
        Provider.setPlacement(initialP);
        
        
       // IaasStatistics check=new IaasStatistics(Provider, 5);
       // check.runSlaMeter(5);
       
        //Timed.simulateUntil(7300);
        
        Timed.simulateUntil(MAXSIMULATIONTIME);
        
        System.exit(0);
    }
    
}

    

