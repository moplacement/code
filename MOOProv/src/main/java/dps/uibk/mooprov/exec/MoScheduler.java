/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.exec;

import dps.uibk.ac.at.Factories.PmFactory;
import dps.uibk.ac.at.Factories.VmFactory;
import dps.uibk.ac.at.algorithms.FirstFit;
import dps.uibk.ac.at.energyModels.MigrationModel;
import dps.uibk.ac.at.mOeA.SoFirstFit;
import dps.uibk.mooprov.comparator.MigrationEnergyComparator;
import static dps.uibk.mooprov.exec.NSGAII_main.logger_;
import dps.uibk.mooprov.iaas.IaaS;
import dps.uibk.mooprov.iaas.PhysicalMachine2;
import dps.uibk.mooprov.iaas.SimulatedMachine;
import dps.uibk.mooprov.iaas.VirtualMachine;
import dps.uibk.mooprov.meters.IaasStatistics;
import dps.uibk.mooprov.meters.PerformanceMeter;
import dps.uibk.mooprov.problems.MoeaProvisioning;
import dps.uibk.mooprov.problems.MoeaProvisioningII;
import dps.uibk.mooprov.schedulers.MoeaScheduler;
import dps.uibk.mooprov.utility.GoogleTracesUtility;
import dps.uibk.mooprov.workload.GaussianGenerator;
import dps.uibk.mooprov.workload.GoogleWorkload;
import dps.uibk.mooprov.workload.Rect;
import dps.uibk.mooprov.workload.Workload;
import hu.mta.sztaki.lpds.cloud.simulator.Timed;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import jmetal.core.Solution;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author ennio
 */
public class MoScheduler {

    /**
     * @param args the command line arguments
     */
    
    
    private final static ArrayList<PhysicalMachine2> pmList=new ArrayList<>();
    private final static ArrayList<VirtualMachine> vmList=new ArrayList<>();
    private final static ArrayList<VirtualMachine> pilotVMList=new ArrayList<>();
    private static IaaS Iaas;
    private static File [] GoogleDir;
   
    private static final Logger logger_=Logger.getLogger(MoScheduler.class);

    public MoScheduler() throws Exception {
        
         PropertyConfigurator.configure("log/log4j.properties");
        
        Iaas=new IaaS(MoeaScheduler.class);
        
        IaaSInit();
    }
    
    
    
    public static void IaaSInit() throws IOException{
    
    int PMs=200,MaxVMs=10;
   
    for (int i=0;i<PMs;i++){
         
         pmList.add(PmFactory.Pm("Xeon-E-2690"));//add PMs servers with the same characterisics
     } 
    
    /*
     for (int i=0;i<(PMs*5/10);i++){
         
        pmList.add(PmFactory.Pm("Xeon-E-2690"));//add PMs servers with the same characterisics
     }   
     
     for (int i=0;i<(PMs*5/10);i++){
         
        pmList.add(PmFactory.Pm("Opteron-8356"));//add PMs servers with the same characterisics
     }
	*/    
    
       //init the infrastructure;
     
        for(PhysicalMachine2 pm:pmList)
            Iaas.addHost(pm);
        
        //create the vMs will run
    
        //for(int i=0;i<30;i++){ 
         
        vmList.addAll(GetVMs(true,45));
     
        
        
        for(VirtualMachine vm:vmList)
            Iaas.addGuest(vm);
        
        
        pilotVMList.addAll(GetPilotVMs(true, 2));
        
        for(VirtualMachine vm:pilotVMList)
            Iaas.addGuest(vm);
         //vmList=GetVMs(true);
        
        //for(VirtualMachine vm:vmList)
         //   Iaas.addGuest(vm);
        
        
    }
    
    
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        
        new MoScheduler();
        
        System.out.println("total nuber of PMs ("+Iaas.gethOst().size()+") ");
        
        System.out.println("IaaS ("+Iaas.getgUest().size()+") VMs");
        
        ArrayList<VirtualMachine> InitialvmList=new ArrayList<>();
        
        MoeaProvisioningII problem=new MoeaProvisioningII("Int",Iaas.getgUest(), pmList);
        
        problem.setComparator(new MigrationEnergyComparator(Iaas,new MigrationModel()));    
        
        SoFirstFit instance = new SoFirstFit(problem);
        
        //Solution initialP1=new Solution(new MoeaProvisioning("Int",InitialvmList, pmList));
        
        Solution initialP=instance.getComputedPlacements().get(0);
        
        logger_.info("solution-"+pmList.size());
        logger_.info("solution-"+vmList.size());
        logger_.info("Initial solution-"+Arrays.toString(initialP.getDecisionVariables()));
       // System.out.println(Arrays.toString(pMsSelectionProbability));
        
        Iaas.ApplyPlacement(initialP);
        Iaas.setPlacement(initialP);
        
        //Timed.simulateUntil(7300);
        
        VirtualMachine pilot1=pilotVMList.get(0);
        VirtualMachine pilot2=pilotVMList.get(1);
        
        System.out.println(pilot1);
        
        PerformanceMeter meter1=new PerformanceMeter(pilot1,7);
        meter1.runSlaMeter(5);
        
        PerformanceMeter meter6=new PerformanceMeter(pilot2,7);
        meter6.runSlaMeter(5);
        
        PerformanceMeter meter2=new PerformanceMeter(pmList.get(60),5);
        meter2.runSlaMeter(5);
        
   //     PerformanceMeter meter3=new PerformanceMeter(pmList.get(51),5);
   //     meter3.runSlaMeter(5);
        
        //PerformanceMeter meter4=new PerformanceMeter(pmList.get(2),5);
        //meter4.runSlaMeter(5);
        
        IaasStatistics check=new IaasStatistics(Iaas, 5);
        check.runSlaMeter(5);
        
        //Timed.simulateUntil(7300);
        
        Timed.simulateUntil(30100);
        
       //ADD VMS 
        
       // vmList.clear();
        ArrayList<VirtualMachine>vmListnext=GetVMs(false,5);
        vmList.addAll(vmListnext);
        
       
        
        for(VirtualMachine vm:vmListnext)
            Iaas.addGuest(vm);
        
        System.out.println("New machines ("+vmListnext.size()+") added");
        
        System.out.println("IaaS ("+Iaas.getgUest().size()+") VMs"); 
        
        
        
        
        //meter2.StopSLAMeter();
       // meter3.StopSLAMeter();
       // meter4.StopSLAMeter();
              
       // meter2=new PerformanceMeter(pmList.get(1),5);
       // meter2.runSlaMeter(5);
        
       // meter3=new PerformanceMeter(pmList.get(0),5);
      //  meter3.runSlaMeter(5);
        
      //  meter4=new PerformanceMeter(pmList.get(2),5);
      //  meter4.runSlaMeter(5);
        
        Timed.simulateUntil(38100);
        
        
        //REMOVE VMS 
        
        
        //Timed.simulateUntilLastEvent();
        
        List <VirtualMachine> vmListremove=vmList.subList(1, 100);
        
        for(VirtualMachine vm:vmListremove)
            Iaas.removeGuest(vm);
        
        vmList.removeAll(vmListremove);
        
        
        
         System.out.println("IaaS ("+Iaas.getgUest().size()+") VMs");
         
        //meter2.StopSLAMeter();
      //  meter3.StopSLAMeter();
      //  meter4.StopSLAMeter();
        
        
       // meter2=new PerformanceMeter(pmList.get(1),5);
       // meter2.runSlaMeter(5);
        
     //   meter3=new PerformanceMeter(pmList.get(0),5);
     //   meter3.runSlaMeter(5);
        
     //   meter4=new PerformanceMeter(pmList.get(2),5);
    //    meter4.runSlaMeter(5);
        
         Timed.simulateUntil(47100);
         
        //ADD VMS 
         
         
        vmListnext=GetVMs(false,10);
        vmList.addAll(vmListnext);
        
       
        
        for(VirtualMachine vm:vmListnext)
            Iaas.addGuest(vm);
        
        System.out.println("New machines ("+vmListnext.size()+") added");
        
        System.out.println("IaaS ("+Iaas.getgUest().size()+") VMs"); 
         
        Timed.simulateUntil(56000);
        
        
        //REMOVE VMS 
        
        
         
         vmListremove=vmList.subList(40, 100);
        
        for(VirtualMachine vm:vmListremove)
            Iaas.removeGuest(vm);
         
        vmList.removeAll(vmListremove);
        
        
        
         System.out.println("IaaS ("+Iaas.getgUest().size()+") VMs");
         
         Timed.simulateUntil(63000);
         
         
         
        //REMOVE VMS 
        
        vmListremove=vmList.subList(100, 160);
        
        for(VirtualMachine vm:vmListremove)
            Iaas.removeGuest(vm);
        
        vmList.removeAll(vmListremove);
        
        
        
         System.out.println("IaaS ("+Iaas.getgUest().size()+") VMs");
         
         Timed.simulateUntil(69100);
         
         
         
         //REMOVE VMS 
         
         
          vmListremove=vmList.subList(200, 223);
        
          
          for(VirtualMachine vm:vmListremove)
            Iaas.removeGuest(vm);
          
          
        vmList.removeAll(vmListremove);
        
        
        
         System.out.println("IaaS ("+Iaas.getgUest().size()+") VMs");
         
         Timed.simulateUntil(80100);
         
         
         //ADD VMS
         
         vmListnext=GetVMs(true,10);
        vmList.addAll(vmListnext);
        
       
        
        for(VirtualMachine vm:vmListnext)
            Iaas.addGuest(vm);
        
        System.out.println("New machines ("+vmListnext.size()+") added");
        
        System.out.println("IaaS ("+Iaas.getgUest().size()+") VMs"); 
         
         
         Timed.simulateUntil(97100);
         
         System.exit(0);
    }
    
    
    
     public static ArrayList<VirtualMachine> GetPilotVMs(boolean overcommittment,double number) throws FileNotFoundException, IOException{
     
     
        //String dir = "/home/ennio/Documents/clusterdata-2011-2/pilotWorkload";
       // String vmfile="/home/ennio/Documents/clusterdata-2011-2/VMinfo/VmsInfo1.csv";
          String dir = "traces/pilotWorkload";
          String vmfile="traces/VMinfo/VmsInfo1.csv";
         
         GoogleTracesUtility google=new GoogleTracesUtility();
        
        ArrayList<VirtualMachine> VMs=new ArrayList();
        while (number>0){
            VMs.add(google.getGoogleWorkload(dir,vmfile));
            
            number--;
        }
       
        return VMs;
     }

    
     public static ArrayList<VirtualMachine> GetVMs(boolean overcommittment,double round) throws FileNotFoundException, IOException{
     
         int allocatedvCPU=0;
       
        double totalVCPU=0,totalMem=0;
        
        //PerformanceMeter meter1,meter2;
        ArrayList<VirtualMachine> VMs=new ArrayList();
        Workload w;
        PhysicalMachine2 Host=new PhysicalMachine2();
        VirtualMachine vm;
        GoogleTracesUtility google=new GoogleTracesUtility();
        //String dir = "/home/ennio/Documents/clusterdata-2011-2/task_usage_ordered_final";
        //String vmfile="/home/ennio/Documents/clusterdata-2011-2/VMinfo/VmsInfo1.csv";        
         String dir = "traces/task_usage_ordered_final";
         String vmfile="traces/VMinfo/VmsInfo1.csv";        
//VirtualMachine expResult = null;
        
        
        
        int randomness=0;
       
        for(int rounds=0;rounds<round;rounds++){
            
            randomness=Math.round((float)Math.random()*10);
            allocatedvCPU=0;
            totalMem=0;
            
        if(overcommittment){
            totalVCPU=Host.getVCPUs()+10;}
        else {
            totalVCPU=Host.getVCPUs()-randomness;}
            
        do
        {
            
           // w= new GaussianGenerator(0.8, 0.5, 0.7, 0.5); //high memory load
           // w= new Rect(250,0.05,0.07);
            
            //vm=VmFactory.Vm(getVMtype());
           //vm=VmFactory.Vm("medium");
           // w=getGoogleWorkload("/home/ennio/Documents/clusterdata-2011-2/task_usage_ordered_final", vm);
           // vm.setWorkload(w);
            
            vm=google.getGoogleWorkload(dir,vmfile);
            VMs.add(vm);
            allocatedvCPU+=vm.getVCPUs(); 
            totalMem+=vm.getVMemory();
            //System.out.println(totalVCPU);
            // System.out.println(Host.getVCPUs());
           
        }
        while(allocatedvCPU<totalVCPU);
        
        System.out.println("CPUOVH - "+(allocatedvCPU/Host.getVCPUs())+" MEMOVH "+totalMem/Host.getMemoryCapacity());
        
        
     }
       
       
         return VMs;
       }
    
   /* 
      public static Workload getGoogleWorkload(String dir,VirtualMachine v) throws FileNotFoundException, IOException{
       
           
           File Dir=new File(dir);
            GoogleWorkload w;
       
                       
           if(GoogleDir==null||GoogleDir.length==0){
           
           if(Dir.isDirectory()){
           
               GoogleDir=Dir.listFiles();
               
           }
           
       }
               int index=0;
               while(index>GoogleDir.length)
                  index=(int)Math.round(Math.random()*10+Math.random()*15);
               
               
               System.out.println(GoogleDir[index].getAbsolutePath());
               w=new GoogleWorkload(GoogleDir[index].getAbsolutePath(),v);
                
                ArrayList <File> temp=new ArrayList(Arrays.asList(GoogleDir));
                temp.remove(index);
                GoogleDir=temp.toArray(GoogleDir);
          
     
           
           if(w==null)
                   throw new SimulatedMachine.SimulatedMachineException("google workload null");
           
           return w;
        }
     
       public static String getVMtype(){
       
           int index;
           index=(int)(Math.random()*10)%4;
           
           return index>2?"large":index>1?"medium":index>0?"small":"extrasmall";
         // return "large";
       }
    */
}
