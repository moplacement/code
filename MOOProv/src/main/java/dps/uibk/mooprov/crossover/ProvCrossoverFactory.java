/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.crossover;

import java.util.HashMap;
import jmetal.operators.crossover.Crossover;
import jmetal.operators.crossover.CrossoverFactory;
import jmetal.util.JMException;

/**
 *
 * @author ennio
 */
public class ProvCrossoverFactory {
 
    public static Crossover getCrossoverOperator(String name, HashMap parameters) throws JMException {
        if (name.equalsIgnoreCase("ProvCrossover"))
            return new ProvCrossover(parameters); 
        else if (name.equalsIgnoreCase("SinglePointCrossoverP"))
            return new SinglePointCrossoverP(parameters);
        else
            return CrossoverFactory.getCrossoverOperator(name, parameters);
       
    }
}
