/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.workload;

import dps.uibk.ac.at.Factories.VmFactory;
import dps.uibk.mooprov.iaas.VirtualMachine;
import hu.mta.sztaki.lpds.cloud.simulator.Timed;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


/**
 *
 * @author ennio
 */
public class GoogleWorkloadTest {
    
    
    private static VirtualMachine vm;
    private static GoogleWorkload gW;
    
    public GoogleWorkloadTest() {
    }
    
    @BeforeClass
    public static void setUpClass() throws FileNotFoundException, IOException {
         Timed.resetTimed(); 
         vm= VmFactory.Vm("small");
         gW=new GoogleWorkload("traces/task_usage_ordered_final/3418442-0.csv", vm);
         vm.setWorkload(gW);
         
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
      
        
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSomeMethod() {
        // TODO review the generated test code and remove the default call to fail.
       
           System.out.println("memory usage = "+gW.getMemoryDemand());
           System.out.println("cpu usage = "+gW.getCPUDemand());
           
       Timed.simulateUntil(30000);
           
           System.out.println("memory usage = "+vm.getMEMdemand());
           System.out.println("cpu usage = "+vm.getCPUdemand());
       
        Timed.simulateUntil(30000);
        
         System.out.println("memory usage = "+vm.getMEMdemand());
         System.out.println("cpu usage = "+vm.getCPUdemand());
         
    }
    
}
