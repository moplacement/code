/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.problems;

import dps.uibk.mooprov.iaas.PhysicalMachine2;
import dps.uibk.mooprov.iaas.VirtualMachine;
import dps.uibk.mooprov.workload.GaussianGenerator;
import dps.uibk.mooprov.comparator.MigrationComparator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import jmetal.core.Solution;
import jmetal.core.Variable;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ennio
 */
public class MoeaProvisioningTest {
    
    MoeaProvisioning instance;
    
    public MoeaProvisioningTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
       List<VirtualMachine> vMlist=new ArrayList<>(); 
        List<PhysicalMachine2> pMlist=new ArrayList<>();
        GaussianGenerator w = new GaussianGenerator(0.5, 0.1, 0.9, 0.2);
        
        int vCPU=0,power=0;
        double vMEM=0;
        VirtualMachine vm;
        for(int vM=0;vM<12;vM++){
        
            vCPU=(int)(Math.random()*30);
            power=(int)(Math.random()*20);
            vMEM=(Math.pow(2, power)*10E6);
            vm=new VirtualMachine("dummy"+vM,"",vCPU,vMEM,w);
            vm.updateCPUdemand();
            vm.updateMemdemand();
            System.out.println("VM-"+vm.getIndex()+" VCPU- "+vm.getVCPUs()+" Vmem- "+vm.getVMemory());
            vMlist.add(vm);
        
        }
        
        for(int pM=0;pM<2;pM++){
        
            pMlist.add(pM,new PhysicalMachine2());
        }
        
        System.out.println("VM number ="+vMlist.size());
        System.out.println("PM number ="+pMlist.size());
        
       
       instance=new MoeaProvisioning("Int",vMlist,pMlist);
      
       
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of evaluateConstraints method, of class MoeaProvisioning.
     */
    @Test
    public void testEvaluateConstraints() throws Exception {
        System.out.println("evaluateConstraints");
        Solution solution ;
        String schedule;
        
         for(int i=0;i<3;i++){
             
        solution = new Solution(instance);
        schedule="";
        for (Variable s:solution.getDecisionVariables())
            schedule+=" "+(int)s.getValue();
            System.out.println(schedule);
        instance.evaluateConstraints(solution);
        // TODO review the generated test code and remove the default call to fail.
        System.out.println("Violated constraint - "+solution.getNumberOfViolatedConstraint());
        System.out.println("Constraint Violation - "+solution.getOverallConstraintViolation());
        
         }
        //System.out.println(solution.getObjective(2));
    }

    /**
     * Test of evaluate method, of class MoeaProvisioning.
     */
    @Test
    public void testEvaluate() throws Exception {
        System.out.println("evaluate");
        Solution solution,solution1;
        MoeaProvisioning mop;
        String schedule="";
        
        solution1=new Solution(instance);
        instance.setInitialSched(solution1);
        instance.setComparator(new MigrationComparator());
        
        for (Variable s:solution1.getDecisionVariables())
            schedule+=" "+(int)s.getValue();
            System.out.println(schedule);
        
        for(int i=0;i<3;i++){
        solution = new Solution(instance);
        schedule="";
        for (Variable s:solution.getDecisionVariables())
            schedule+=" "+(int)s.getValue();
            System.out.println(schedule);
       
        instance.evaluate(solution);
        
         System.out.println("Objective-1 - "+solution.getObjective(0));
         System.out.println("Objective-2 - "+solution.getObjective(1));
         System.out.println("Objective-3 - "+solution.getObjective(2));
        
        }
        //instance.evaluate(sltn);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
