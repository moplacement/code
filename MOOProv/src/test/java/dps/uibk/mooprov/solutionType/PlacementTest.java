/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.solutionType;

import static dps.uibk.mooprov.exec.MoScheduler.GetVMs;
import dps.uibk.mooprov.iaas.IaaS;
import dps.uibk.mooprov.iaas.PhysicalMachine2;
import dps.uibk.mooprov.iaas.VirtualMachine;
import dps.uibk.mooprov.problems.MoeaProvisioning;
import dps.uibk.mooprov.schedulers.DummyScheduler;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.Variable;
import org.junit.Test;


/**
 *
 * @author ennio
 */
public class PlacementTest {
    
    private final static ArrayList<PhysicalMachine2> pmList=new ArrayList<>();
    private final static ArrayList<VirtualMachine> vmList=new ArrayList<>();
    private static IaaS Iaas;
    
    public PlacementTest() throws IOException {
       
    }

     private void init() throws IOException{
    
     System.out.println("pm in list"+pmList.size());
    
     for (int i=0;i<50;i++){
        pmList.add(new PhysicalMachine2());//add 3 servers with the same characterisics
     }   
       
         System.out.println("pm in list"+pmList.size());
        for(PhysicalMachine2 pm:pmList)
            Iaas.addHost(pm);
     
         
       vmList.addAll(GetVMs(true,1));
       
        for(VirtualMachine vm:vmList)
            Iaas.addGuest(vm);
        System.out.println("vm in list"+vmList.size());
        
         
         //vmList=GetVMs(true);
        
        //for(VirtualMachine vm:vmList)
         //   Iaas.addGuest(vm);
        
        
    }
    
    @Test
    public void testSomeMethod() {
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }

    @Test
    public void testGetParent() {
        System.out.println("getParent");
        Placement instance = null;
        Solution expResult = null;
       // Solution result = instance.getParent();
      //  assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }

    @Test
    public void testSetParent() {
        System.out.println("setParent");
        Solution parent = null;
        Placement instance = null;
      //  instance.setParent(parent);
        // TODO review the generated test code and remove the default call to fail.
      //  fail("The test case is a prototype.");
    }

    @Test
    public void testCreateVariables() throws ClassNotFoundException, IOException {
        System.out.println("createVariables");
         
        Iaas=new IaaS(DummyScheduler.class);
        init();
        
        System.out.println("total nuber of PMs ("+Iaas.gethOst().size()+") ");
        
        System.out.println("IaaS ("+Iaas.getgUest().size()+") VMs");
        
      
        
        Problem prob=new MoeaProvisioning("Int",vmList, pmList);
       
        Solution initialP=new Solution(prob);
        
        Placement instance=new Placement(prob);
        
        instance.setParent(initialP);
        
        Variable[] result = instance.createVariables();
        
        System.out.println(Arrays.toString(result));
       // assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      //  fail("The test case is a prototype.");
    }
    
}
