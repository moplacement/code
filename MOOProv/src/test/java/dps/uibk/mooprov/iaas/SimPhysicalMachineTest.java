/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.iaas;

import dps.uibk.mooprov.meters.PerformanceMeter;
import dps.uibk.mooprov.workload.GaussianGenerator;
import dps.uibk.mooprov.workload.Rect;
import dps.uibk.mooprov.workload.Workload;
import hu.mta.sztaki.lpds.cloud.simulator.Timed;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ennio
 */
public class SimPhysicalMachineTest {
     
    PhysicalMachine2 Host;
    ArrayList <VirtualMachine> VMs= new ArrayList<>();
    Workload w;
    
    
    public SimPhysicalMachineTest() {
         PropertyConfigurator.configure("log/log4j.properties");
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        Timed.resetTimed();
        Host=new PhysicalMachine2();
        System.out.println(Host);
        
        
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
     @Test
     public void hello() {
     
         CPUOvercommitment();
         //MemoryOvercommitment();
         for (VirtualMachine v:VMs)
             System.out.println(v.toString());
         assertTrue(true);
         
     }
    
     public void CPUOvercommitment(){
     
         int vCPU=0;
         double vMEM;
        double totalVCPU=0,totalVMEM=0;
        double memBase=30*Math.pow(2,30);
        PerformanceMeter meter1,meter2;
        do
        {
            vCPU=(int)Math.pow(2,((int)(Math.random()*10))%5);
            vMEM=memBase;
            w= new GaussianGenerator(0.6, 0.5, 0.7, 0.5); //high memory load
            //w= new Rect(250,0.05,0.07);
            VMs.add(new VirtualMachine("dummy"+vCPU, "", vCPU,vMEM , w));
            totalVCPU+=vCPU;            
            //System.out.println(totalVCPU);
            // System.out.println(Host.getVCPUs());
        }
        while(totalVCPU<Host.getVCPUs()+500);
        
        //System.out.println(totalVCPU);
        Host.allocateVMs(VMs);
         System.out.println("CPUOVH - "+(totalVCPU/Host.getVCPUs())+" MEMOVH "+Host.getMemoryRequested()/Host.getMemoryCapacity());
        Iterator <VirtualMachine> iv=VMs.iterator();
        meter1=new PerformanceMeter(iv.next(),5);
        meter1.runSlaMeter(5);
        
        meter2=new PerformanceMeter( iv.next(),5);
        meter2.runSlaMeter(5);
       // Host.getEventQueue().iterator().next().tick(10);
         System.out.println("event has been scheduled"+ Timed.getNextFire());
        Timed.simulateUntil(3600);
        
        System.out.println("the avg exec is:"+ Host.forecastCPUUtilization(VMs.get(0)));
        
        meter1.StopSLAMeter();
          System.out.println("event has been scheduled"+ Host.getEventQueue().iterator().next().isCancelled());
     }
     
     public void MemoryOvercommitment(){
     
        int vCPU=0;
        double vMEM=0;
        double totalVCPU=0,totalVMEM=0;
        double memBase=30*Math.pow(2,30);
        do
        {
            vCPU=(int)Math.pow(2,((int)(Math.random()*10))%3);
            vMEM=Math.pow(2,(Math.random()*10)%4)*memBase;
            w= new GaussianGenerator(0.5, 0.1, 0.9, 0.2); //high memory load
            VMs.add(new VirtualMachine("dummy"+vCPU, "", vCPU,vMEM , w));
            totalVCPU+=vCPU;
            totalVMEM+=vMEM;
            
            //System.out.println(totalVCPU);
            // System.out.println(Host.getVCPUs());
        }
        while(totalVMEM<Host.getMemoryCapacity());
        
        System.out.println("CPUOVH - "+(totalVCPU/Host.getVCPUs())+" MEMOVH "+totalVMEM/Host.getMemoryCapacity());
        Host.allocateVMs(VMs);
        //System.out.println(totalVCPU);
           
        System.out.println("CPUOVH - "+(totalVCPU/Host.getVCPUs())+" MEMOVH "+totalVMEM/Host.getMemoryCapacity());
       
     
     
     
     }
    
}
