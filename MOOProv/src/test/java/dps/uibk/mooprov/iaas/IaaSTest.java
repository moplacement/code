/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.iaas;

import dps.uibk.ac.at.Factories.VmFactory;
import dps.uibk.mooprov.iaas.SimulatedMachine.SimulatedMachineException;
import dps.uibk.mooprov.meters.PerformanceMeter;
import dps.uibk.mooprov.problems.MoeaProvisioning;
import dps.uibk.mooprov.schedulers.DummyScheduler;
import dps.uibk.mooprov.workload.GaussianGenerator;
import dps.uibk.mooprov.workload.GoogleWorkload;

import dps.uibk.mooprov.workload.Workload;
import hu.mta.sztaki.lpds.cloud.simulator.Timed;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import java.util.List;
import jmetal.core.Solution;
import org.junit.After;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


/**
 *
 * @author ennio
 */
public class IaaSTest {
    
    static IaaS testIaas;
    private static File [] GoogleDir;
     
    public IaaSTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
   // @AfterClass
    public static void tearDownClass() throws Exception {
       System.out.println("Shutdown");
        
        IaaS instance = testIaas;
        Timed.simulateUntil(100);
         instance.Shutdown();
        //Timed.simulateUntil(100);
        // TODO review the generated test code and remove the default call to fail.
        System.out.println(instance.isSubscribed());
        assertFalse("Iaas stop failed",instance.isSubscribed());  
    }
    
    @Before
    public void setUp() throws Exception {
        testIaas=new IaaS(DummyScheduler.class);
        Timed.resetTimed();
    }
    
    @After
    public void tearDown() {
       
    }

    @Test
    public void testShutdown() {
       
    }

    @Test
    public void testApplyPlacement() throws Exception {
        System.out.println("ApplyPlacement");
        Solution placement = null;
        IaaS instance = testIaas;
        List<VirtualMachine>vms=GetVMs(false);
        
        ArrayList<PhysicalMachine2>pm=new ArrayList<>();
        PhysicalMachine2  h1,h2;
        h1=new PhysicalMachine2();
        h2=new PhysicalMachine2();
        
        pm.add(h1);
        
        for(VirtualMachine vma:vms)
            testIaas.addGuest(vma);
        
        testIaas.addHost(h1);
        testIaas.addHost(h2);
        
        
        placement=new Solution(new MoeaProvisioning("Int", vms, pm));
        
        
        instance.ApplyPlacement(placement);
        
        Timed.simulateUntil(100);
        
        System.out.println(testIaas.PrintIaaSStatus());
        // TODO review the generated test code and remove the default call to fail.
        assertFalse("Iaas stop failed",h2.isSubscribed());
    }

     @Test
    public void testMigration() throws Exception {
        System.out.println("TestMigration");
        Solution placement = null;
        IaaS instance = testIaas;
        
        /*Workload w= new GaussianGenerator(0.6, 0.5, 0.7, 0.5); //high memory load
            //w= new Rect(250,0.05,0.07);
        VirtualMachine vm=VmFactory.Vm(getVMtype());
        vm.setWorkload(w);*/
        
        List<VirtualMachine>vms=GetVMs(false);//Collections.singletonList(vm) ;//GetVMs(false);
        
        ArrayList<PhysicalMachine2>pm=new ArrayList<>();
        PhysicalMachine2  h1,h2;
        h1=new PhysicalMachine2();
        h2=new PhysicalMachine2();
        
        pm.add(h1);
        
         System.out.println(h1);
        
        for(VirtualMachine vma:vms)
            testIaas.addGuest(vma);
        
       
        testIaas.addHost(h1);
        testIaas.addHost(h2);
        
        placement=new Solution(new MoeaProvisioning("Int", vms, pm));
        
        PerformanceMeter meter1=new PerformanceMeter(vms.get(0),5);
         meter1.runSlaMeter(5);
         
           PerformanceMeter meter2=new PerformanceMeter(h1,5);
         meter2.runSlaMeter(5);
         
            PerformanceMeter meter3=new PerformanceMeter(h2,5);
         meter3.runSlaMeter(5);
        
        instance.ApplyPlacement(placement);
        
        Timed.simulateUntil(100);
        
        System.out.println(testIaas.PrintIaaSStatus());
        
        
        //pm.clear();
        //pm.remove(h1);
        //pm.add(0, h2);//.add(h1);
        //pm.add(1, h2);
        pm.add(h2);
         System.out.println("size of solution list "+pm.size());
        //pm.add(h1);
        placement=new Solution(new MoeaProvisioning("Int", vms, pm));
        // TODO review the generated test code and remove the default call to fail.
        
        instance.ApplyPlacement(placement);
        
        
        
        Timed.simulateUntil(10000);
        
        System.out.println(testIaas.PrintIaaSStatus());
        meter1.StopSLAMeter();
        meter2.StopSLAMeter();
        meter3.StopSLAMeter();
        //assertFalse("Iaas stop failed",h1.isSubscribed());
    }
    
    
    /*
    @Test
    public void testPrintIaaSStatus() {
        System.out.println("PrintIaaSStatus");
        IaaS instance = testIaas;
        List<VirtualMachine>vms=GetVMs(false);
        
        ArrayList<PhysicalMachine>pm=new ArrayList<>();
        PhysicalMachine  h1,h2;
        h1=new PhysicalMachine();
        h2=new PhysicalMachine();
        
        pm.add(h1);
        
        for(VirtualMachine vma:vms)
            testIaas.addGuest(vma);
        
        testIaas.addHost(h2);
        testIaas.addHost(h1);
        
        System.out.println(testIaas);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    
    @Test
    public void testGethOst() {
        System.out.println("gethOst");
        IaaS instance = testIaas;
        List<PhysicalMachine> expResult = null;
        List<PhysicalMachine> result;
        
        expResult=new ArrayList<>();
        PhysicalMachine  h1,h2;
        h1=new PhysicalMachine();
        h2=new PhysicalMachine();
        
        expResult.add(h1);
                
        
        testIaas.addHost(h1);
        testIaas.addHost(h2);
        
        Timed.simulateUntil(100);
       
        
        assertEquals(expResult.get(0), testIaas.gethOst().get(0));
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }

  //  @Test
    public void testGetgUest() {
        System.out.println("getgUest");
        IaaS instance = null;
        List<VirtualMachine> expResult = null;
        List<VirtualMachine> result = instance.getgUest();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

  //  @Test
    public void testGetPlacement() {
        System.out.println("getPlacement");
        IaaS instance = null;
        Solution expResult = null;
        Solution result = instance.getPlacement();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

   // @Test
    public void testSetPlacement() {
        System.out.println("setPlacement");
        Solution Placement = null;
        IaaS instance = null;
        instance.setPlacement(Placement);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

   // @Test
    public void testAddGuest() {
        System.out.println("addGuest");
        VirtualMachine vm = null;
        IaaS instance = null;
        instance.addGuest(vm);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    @Test
    public void testRemoveGuest() throws Exception {
        System.out.println("removeGuest");
         Solution placementA,placementB;
        IaaS instance = testIaas;
        List<VirtualMachine>vms=GetVMs(false);
        
        VirtualMachine IwantYou=vms.get(3);
        
        ArrayList<PhysicalMachine>pm=new ArrayList<>();
        PhysicalMachine  h1,h2;
        h1=new PhysicalMachine();
        h2=new PhysicalMachine();
        
        pm.add(h1);
        
        for(VirtualMachine vma:vms)
            testIaas.addGuest(vma);
        
        testIaas.addHost(h1);
        testIaas.addHost(h2);
        
        
        placementA=new Solution(new MoeaProvisioning("Int", vms, pm));
        
        instance.ApplyPlacement(placementA);
        
        instance.setPlacement(placementA);
        
        vms.remove(IwantYou);
        
        placementB=new Solution(new MoeaProvisioning("Int", vms, pm));
        
        testIaas.removeGuest(IwantYou);
        
        Timed.simulateUntil(100);
        
        System.out.println(testIaas.PrintIaaSStatus());
        // TODO review the generated test code and remove the default call to fail.
        
        assertTrue("Iaas stop failed",placementB.getDecisionVariables().length==testIaas.getPlacement().getDecisionVariables().length);
   
          assertNotSame(IwantYou, testIaas.getgUest().get(3));
    
    }
    
    
   
    */
       public List<VirtualMachine> GetVMs(boolean overcommittment) throws FileNotFoundException, IOException{
     
         int allocatedvCPU=0;
       
        double totalVCPU=0,totalMem=0;
        
        //PerformanceMeter meter1,meter2;
        ArrayList<VirtualMachine> VMs=new ArrayList();
        Workload w;
        PhysicalMachine2 Host=new PhysicalMachine2();
        VirtualMachine vm;
        
        if(overcommittment)
            totalVCPU=Host.getVCPUs()+10;
        else 
            totalVCPU=Host.getVCPUs();
        
        do
        {
            
            //w= new GaussianGenerator(0.6, 0.5, 0.7, 0.5); //high memory load
            //w= new Rect(250,0.05,0.07);
           
            vm=VmFactory.Vm(getVMtype());
            w=getGoogleWorkload("/home/ennio/Documents/clusterdata-2011-2/task_usage_ordered_final", vm);
            vm.setWorkload(w);
            
            VMs.add(vm);
            allocatedvCPU+=vm.getVCPUs(); 
            totalMem+=vm.getVMemory();
            //System.out.println(totalVCPU);
            // System.out.println(Host.getVCPUs());
        }
        while(allocatedvCPU<totalVCPU);
        
        //System.out.println(totalVCPU);
        //Host.allocateVMs(VMs);
         System.out.println("CPUOVH - "+(allocatedvCPU/Host.getVCPUs())+" MEMOVH "+totalMem/Host.getMemoryCapacity());
       
         return VMs;
       }
       
       public String getVMtype(){
       
           int index;
           index=(int)(Math.random()*10)%4;
           
           return index>2?"large":index>1?"medium":index>0?"small":"extrasmall";
       
       }

        public static Workload getGoogleWorkload(String dir,VirtualMachine v) throws FileNotFoundException, IOException{
       
           
           File Dir=new File(dir);
            GoogleWorkload w;
       
           
           if(GoogleDir==null||GoogleDir.length==0){
           
           if(Dir.isDirectory()){
           
               GoogleDir=Dir.listFiles();
           
           }
           
       }
           
               System.out.println(GoogleDir[0].getAbsolutePath());
               w=new GoogleWorkload(GoogleDir[0].getAbsolutePath(),v);
                
                ArrayList <File> temp=new ArrayList(Arrays.asList(GoogleDir));
                temp.remove(0);
                GoogleDir=temp.toArray(GoogleDir);
          
     
           
           if(w==null)
                   throw new SimulatedMachineException("google workload null");
           
           return w;
        }
}
