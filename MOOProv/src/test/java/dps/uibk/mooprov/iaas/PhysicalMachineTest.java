/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.iaas;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import dps.uibk.mooprov.workload.GaussianGenerator;
import dps.uibk.mooprov.workload.Workload;
import hu.mta.sztaki.lpds.cloud.simulator.Timed;
import java.util.ArrayList;
import java.util.Collection;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ennio
 */
public class PhysicalMachineTest {
    
    PhysicalMachine2 instance;
    Workload w;
    Logger logger = Logger.getLogger(PhysicalMachineTest.class);
    
    public PhysicalMachineTest() {
         PropertyConfigurator.configure("log/log4j.properties");
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        Timed.resetTimed();
        instance = new PhysicalMachine2();
        w= new GaussianGenerator(0.5, 0.1, 0.9, 0.2);
        
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getCPUUtilization method, of class PhysicalMachine.
     */
    @Test
    public void testGetCPUUtilization() {
        System.out.println("getCPUUtilization");
        VirtualMachine vm = new VirtualMachine("dummy", "", 128, 1024, w);
        vm.updateCPUdemand();
        System.out.println("VM cpu load - "+vm.getCPUdemand());
        double result = instance.getCPUUtilization(vm);
        System.out.println("VM cpu of PM demand - "+result);
        assertTrue("ERROR - the CPU utilization is lesser than 0", result>0);
        
    }

    /**
     * Test of getMEMUtilization method, of class PhysicalMachine.
     */
    @Test
    public void testGetMEMUtilization() {
        System.out.println("getMEMUtilization");
        VirtualMachine vm = new VirtualMachine("dummy", "", 4, 1024*10E6, w);
        vm.updateMemdemand();
        double result = instance.getMEMUtilization(vm);
        System.out.println(result);
        assertTrue("ERROR - the Mem utilization is lesser than 0", result>0);
    }

    /**
     * Test of toString method, of class PhysicalMachine.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        PhysicalMachine2 dummyinstance = new PhysicalMachine2();
        String expResult = "";
        System.out.println(dummyinstance.toString());
        
    }
    
    @Test
    public void testCPUOvercomittment() {
        System.out.println("getOvercommitment");
        double cPU,cPUnum=instance.getVCPUs();
        cPU=cPUnum;
        int vCPU,totalVCPU=0;
        VirtualMachine vm;
        double result=0;
        do
        {
            vCPU=(int)(Math.random()*10);
            vm=new VirtualMachine("dummy"+vCPU, "", vCPU, 1024*vCPU, w);
            vm.updateCPUdemand();
            System.out.println("VM"+vm.getIndex()+" cpu load - "+vm.getCPUdemand());
            result+=instance.getCPUUtilization(vm);
            System.out.println("VM cpu of PM demand - "+result);
            cPUnum-=vCPU;
            totalVCPU+=vCPU;
        }
        while(result<1);
        
        System.out.println("VCPUs VS PHY CPU - "+totalVCPU+"/"+cPU);
        System.out.println("VM cpu of PM demand - "+result);
        assertTrue("ERROR - CPU Overcommitment does not work", result>0 && totalVCPU>=cPU );
        
    }
    
    @Test
    public void testMEMOvercomittment() {
        System.out.println("getMemoryOvercommitment");
        double mEM,mEMsize=instance.getMemoryCapacity();
        mEM=mEMsize;
        double vMEM,totalVMEM=0;
        VirtualMachine vm;
        double result=0,power=0;
        do
        {
            power=(int)(Math.random()*20);
            vMEM=(Math.pow(2, power)*10E6);
            vm=new VirtualMachine("dummy"+vMEM, "", 8, vMEM, w);
            vm.updateMemdemand();
            System.out.println("VM"+vm.getIndex()+" mem load - "+vm.getMEMdemand());
            result+=instance.getMEMUtilization(vm);
            System.out.println("VM mem of PM demand - "+result);
            mEMsize-=vMEM;
            totalVMEM+=vMEM;
        }
        while(result<1);
        
        System.out.println("VMEM VS PHY MEM - "+totalVMEM+"/"+vMEM);
        System.out.println("VM cpu of PM demand - "+result);
        assertTrue("ERROR - MEM Overcommitment does not work", result>0 && totalVMEM>=mEM );
        
    }
    
    @Test
    public void testCpuSchedulingOverhead() {
       System.out.println("CpuSchedulingOverhead");
       logger.info("CpuSchedulingOverhead");
        double cPU,cPUnum=instance.getVCPUs();
        cPU=cPUnum;
        int vCPU,totalVCPU=0;
        VirtualMachine vm;
        double result=0;
        do
        {
            vCPU=(int)(Math.random()*10);
            vm=new VirtualMachine("dummy"+vCPU, "", vCPU, 1024*vCPU, w);
            vm.updateCPUdemand();
            System.out.println("VM"+vm.getIndex()+" cpu load - "+vm.getCPUdemand());
            result+=instance.getCPUUtilization(vm);
            System.out.println("VM cpu of PM demand - "+result);
            cPUnum-=vCPU;
            totalVCPU+=vCPU;
        }
        while(result<0.5);
        
        System.out.println("VCPUs VS PHY CPU - "+totalVCPU+"/"+cPU);
        
        double schovh=instance.getSchedulingOverhead(totalVCPU);
        
        System.out.println("scheduling overhead - "+schovh);
         
        result+=schovh;
        
        System.out.println("VM cpu of PM demand - "+result);
        
        
        assertFalse("ERROR - CPU Overcommitment does not work", result>0 && totalVCPU>=cPU && schovh>0 );
       
    
    
    
    }
    
    
    @Test
      public void testCpuAndMemorySharing() {
       System.out.println("CpuAndMemorySharing");
         
       
        double cPU,cPUnum=instance.getVCPUs();
        cPU=cPUnum;
        int vCPU,totalVCPU=0;
        VirtualMachine vm;
        double CpusDemand=0,MemDemand=0;
        
        Collection<VirtualMachine>vms=new <VirtualMachine>ArrayList();
        do
        {
            vCPU=(int)(Math.random()*10);
            vm=new VirtualMachine("dummy"+vCPU, "", vCPU,Math.pow(2,30), w);
            vm.updateCPUdemand();
            vm.updateMemdemand();
            vms.add(vm);
            CpusDemand+=instance.getCPUUtilization(vm);
            MemDemand+=instance.getMEMUtilization(vm);
            
           logger.debug("VM"+vm.getIndex()+" mem - "+vm.getVMemory());
        }
        while(CpusDemand<0.5&&MemDemand<0.6);
        
        instance.allocateVMs(vms);
        
        logger.debug("Number of allocated VM -" + instance.getNumberofAllocatedVms());
        
        logger.debug("VCPUs VS PHY CPU - "+instance.getvCPUsRequested()+"/"+instance.getVCPUs());
        
        logger.debug("VMemory VS PHYMemory - "+instance.getMemoryRequested()+"/"+instance.getMemoryCapacity());
        
       for(int i=0;i<5;i++){
            logger.debug("---- Iteration "+i);
        for(VirtualMachine temp:instance.getAllocatedVMs()){
       
            
                 temp.updateCPUdemand();
                 temp.updateMemdemand();
                 
           try {
               //logger.info("VM - "+temp.getIndex());
              
               logger.debug("CPU utilization"+instance.getCPUUtilization(temp));
               logger.debug("Real CPU utilization"+instance.getRealCPUUtilization(temp));
               logger.debug("Memory utilization"+instance.getMEMUtilization(temp));
               logger.debug("Real Memory utilization"+instance.getRealMEMUtilization(temp));
               logger.debug(instance.getVMResourceSharing(temp));
           } catch (Exception ex) {
                logger.debug(ex);
           }
           
           //instance.updateMemoryTarget();
        }
       }
        
        
        logger.debug("overall VMs CPU demand - "+CpusDemand);
        logger.debug("overall VMs Memory demand - "+MemDemand);
        
        assertFalse("ERROR - Resource sharing does not work properly",
                CpusDemand>0 && CpusDemand<0
                && MemDemand>0 && MemDemand<0
                && instance.getRealCPUUtilization(vm)==instance.getCPUUtilization(vm)
                && instance.getMEMUtilization(vm)==instance.getRealMEMUtilization(vm)
        );
       
    
    
    
    }
    
/*
    @Test
    public void testGetMemoryCapacity() {
        System.out.println("getMemoryCapacity");
        PhysicalMachine instance = new PhysicalMachine();
        double expResult = 0.0;
        double result = instance.getMemoryCapacity();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    @Test
    public void testGetVCPUs() {
        System.out.println("getVCPUs");
        PhysicalMachine instance = new PhysicalMachine();
        int expResult = 0;
        int result = instance.getVCPUs();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    @Test
    public void testGetSchedulingOverhead() {
        System.out.println("getSchedulingOverhead");
        int vCpus = 0;
        PhysicalMachine instance = new PhysicalMachine();
        double expResult = 0.0;
        double result = instance.getSchedulingOverhead(vCpus);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
   */ 
}
