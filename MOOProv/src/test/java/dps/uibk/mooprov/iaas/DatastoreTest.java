/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.iaas;

import hu.mta.sztaki.lpds.cloud.simulator.Timed;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ennio
 */
public class DatastoreTest {
    
    public DatastoreTest() {
    }

    @Test
    public void testSomeMethod() {
        
        Datastore A=new Datastore(){

            double timestamp;
            
            @Override
            public void Shutdown() throws SimulatedMachine.SimulatedMachineException {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void Suspend() throws SimulatedMachine.SimulatedMachineException {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void run() throws SimulatedMachine.SimulatedMachineException {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public double sentFile(Datastore.fileDescriptor fd, SimulatedMachine source, SimulatedMachine target) {
                get_attached_Network().get_end_to_end_Channel(source, target); //To change body of generated methods, choose Tools | Templates.
                get_attached_Network().sendFile(fd, source, target);
                timestamp=Timed.getFireCount();
                
                System.out.println("file "+fd.getFileID()+"transfer started at"+timestamp+"from"+source+"to"+target);
                return 0;
            }

            @Override
            protected void FileSent(Datastore.fileDescriptor fd, SimulatedMachine source, SimulatedMachine target) {
                System.out.println("file "+fd.getFileID()+"sended from"+source);
                System.out.println("file transfer lasted "+(Timed.getFireCount()-timestamp));
                get_attached_Network().close_end_to_end_Channel(source,target); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            protected void FileReceived(Datastore.fileDescriptor fd, SimulatedMachine source, SimulatedMachine target) {
                System.out.println("file "+fd.getFileID()+"received at"+Timed.getFireCount()+"from"+source+"to"+target);
                
               // get_attached_Network().closeChannel(source, target); //To change body of generated methods, choose Tools | Templates.
            }
    
        
    
    };
        Datastore B=new Datastore(){

            double timestamp;
            
            @Override
            public void Shutdown() throws SimulatedMachine.SimulatedMachineException {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void Suspend() throws SimulatedMachine.SimulatedMachineException {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void run() throws SimulatedMachine.SimulatedMachineException {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public double sentFile(Datastore.fileDescriptor fd, SimulatedMachine source, SimulatedMachine target) {
                get_attached_Network().get_end_to_end_Channel(source, target); //To change body of generated methods, choose Tools | Templates.
                get_attached_Network().sendFile(fd, source, target);
                timestamp=Timed.getFireCount();
                
                System.out.println("file "+fd.getFileID()+"transfer started at"+timestamp+"from"+source+"to"+target);
                return 0;
            }

            @Override
            protected void FileSent(Datastore.fileDescriptor fd, SimulatedMachine source, SimulatedMachine target) {
                System.out.println("file "+fd.getFileID()+"sended from"+source);
                System.out.println("file transfer lasted "+(Timed.getFireCount()-timestamp));
                get_attached_Network().close_end_to_end_Channel(source, target); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            protected void FileReceived(Datastore.fileDescriptor fd, SimulatedMachine source, SimulatedMachine target) {
                System.out.println("file "+fd.getFileID()+"received at"+Timed.getFireCount()+"from"+source+"to"+target);
                
                //get_attached_Network().closeChannel(source, target); //To change body of generated methods, choose Tools | Templates.
            }
    
        
    
    };
        
        Datastore.fileDescriptor fd,fd2;
        fd = new Datastore.fileDescriptor((long)256E6,1,"23");
        fd2 = new Datastore.fileDescriptor((long)256E6,1,"24");
        Network route= new Tcp_Ip();
        A.attach_Network(route);
        B.attach_Network(route);
        System.out.println(A.get_attached_Network().getNodesConnected(B));
        A.sentFile(fd,A, B);
       // A.sentFile(fd2,A, B);
        //A.sentFile(fd,A, B);
        B.sentFile(fd,B, A);
        //B.sentFile(fd2,B, A);
       // Timed.simulateUntil(90000);
       //  B.sentFile(fd2,B, A);
        
        Timed.simulateUntil(1000000);
        //assertTrue("The test case is a prototype.");
    }
    
}
