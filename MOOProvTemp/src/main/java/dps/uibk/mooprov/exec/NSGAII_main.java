/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.exec;

import dps.uibk.mooprov.MOEAGUI.Gui;
import dps.uibk.mooprov.comparator.MigrationComparator;
import dps.uibk.mooprov.iaas.PhysicalMachine;
import dps.uibk.mooprov.iaas.VirtualMachine;
import dps.uibk.mooprov.workload.GaussianGenerator;
import dps.uibk.mooprov.problems.MoeaProvisioning;
import dps.uibk.mooprov.utility.ChartCreation;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import java.util.logging.FileHandler;

import jmetal.core.Algorithm;
import jmetal.core.Operator;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.metaheuristics.nsgaII.NSGAII;
import jmetal.operators.crossover.CrossoverFactory;
import jmetal.operators.mutation.MutationFactory;
import jmetal.operators.selection.SelectionFactory;
import jmetal.qualityIndicator.QualityIndicator;
import jmetal.util.JMException;
import jmetal.util.Ranking;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;


/**
 *
 * @author ennio
 */
public class NSGAII_main {
    
  public static Logger      logger_ ;      // Logger object
  public static FileHandler fileHandler_ ; // FileHandler object

  /**
   * @param args Command line arguments.
   * @throws JMException 
   * @throws IOException 
   * @throws SecurityException 
   * Usage: three options
   *      - jmetal.metaheuristics.nsgaII.NSGAII_main
   *      - jmetal.metaheuristics.nsgaII.NSGAII_main problemName
   *      - jmetal.metaheuristics.nsgaII.NSGAII_main problemName paretoFrontFile
   */
  public static void main(String [] args) throws 
                                  JMException, 
                                  SecurityException, 
                                  IOException, 
                                  ClassNotFoundException,
                                  InterruptedException {
      
    Problem   problem   ; // The problem to solve
    Algorithm algorithm ; // The algorithm to use
    Operator  crossover ; // Crossover operator
    Operator  mutation  ; // Mutation operator
    Operator  selection ; // Selection operator
    
    HashMap  parameters ; // Operator parameters
    
    QualityIndicator indicators ; // Object to get quality indicators

    // Logger object and file to store log messages
    /*logger_      = Configuration.logger_ ;
    fileHandler_ = new FileHandler("NSGAII_main.log"); 
    logger_.addHandler(fileHandler_) ;*/
    
     PropertyConfigurator.configure("log/log4j.properties");
     logger_=Logger.getLogger(NSGAII_main.class);
     
     //GUI stuff
     
     final ChartCreation c=new ChartCreation();
     
     
     
     
     
    indicators = null ;
    if (args.length == 1){//TODO
        problem=null;
    }
   /* if (args.length == 1) {
      Object [] params = {"Real"};
      problem = (new ProblemFactory()).getProblem(args[0],params);
    } // if
    else if (args.length == 2) {
      Object [] params = {"Real"};
      problem = (new ProblemFactory()).getProblem(args[0],params);
      indicators = new QualityIndicator(problem, args[1]) ;
    } // if TODO */
    else { // Default problem
        ArrayList<PhysicalMachine> pMlist=new ArrayList();
        ArrayList <VirtualMachine> vMlist=new ArrayList();
        GaussianGenerator w = new GaussianGenerator(0.5, 0.1, 0.9, 0.2);
        
        int vCPU=0,power=0;
        double vMEM=0;
        VirtualMachine vm;
        for(int VM=0;VM<30;VM++){
        
            vCPU=(int)(Math.random()*10)+1;
            power=(int)(Math.random()*20);
            vMEM=(10E9);
            vm=new VirtualMachine("dummy"+VM,"",vCPU,vMEM,w);
            vm.updateCPUdemand();
            vm.updateMemdemand();
           // System.out.println("VM-"+vm.getIndex()+" vCPU- "+vm.getVCPUs()+" Vmem- "+vm.getVMemory());
            vMlist.add(vm);
            logger_.info(vm.toString());
        
        }
        
        for(int pM=0;pM<10;pM++){
        
            pMlist.add(new PhysicalMachine());
        }
        
       // System.out.println("VM number ="+vMlist.size());
       // System.out.println("pM number ="+pMlist.size());
        
       problem=new MoeaProvisioning("Int",vMlist,pMlist);
        Solution solution1=new Solution(problem);
        ((MoeaProvisioning)problem).setInitialSched(solution1);
        ((MoeaProvisioning)problem).setComparator(new MigrationComparator());
    } // else
    
    algorithm = new NSGAII(problem);
    //algorithm = new ssNSGAII(problem);

    // Algorithm parameters
    algorithm.setInputParameter("populationSize",100);
    algorithm.setInputParameter("maxEvaluations",1000000);
 
    // Mutation and Crossover for Real codification 
    parameters = new HashMap() ;
    parameters.put("probability", 0.9) ;
    //parameters.put("distributionIndex", 20.0) ;
    crossover = CrossoverFactory.getCrossoverOperator("SinglePointCrossover", parameters);                   

    parameters = new HashMap() ;
    parameters.put("probability", 1.0/problem.getNumberOfVariables()) ;
    //parameters.put("distributionIndex", 20.0) ;
    mutation = MutationFactory.getMutationOperator("BitFlipMutation", parameters);                    

    // Selection Operator 
    parameters = null ;
    selection = SelectionFactory.getSelectionOperator("BinaryTournament2", parameters) ;                           

    // Add the operators to the algorithm
    algorithm.addOperator("crossover",crossover);
    algorithm.addOperator("mutation",mutation);
    algorithm.addOperator("selection",selection);

    // Add the indicator object to the algorithm
    algorithm.setInputParameter("indicators", indicators) ;
    
    // Execute the Algorithm
    long initTime = System.currentTimeMillis();
    SolutionSet population = algorithm.execute();
    long estimatedTime = System.currentTimeMillis() - initTime;
    
    // Result messages 
    logger_.info("Total execution time: "+estimatedTime + "ms");
    logger_.info("Variables values have been writen to file VAR");
    population.printVariablesToFile("VAR");    
    logger_.info("Objectives values have been writen to file FUN");
    population.printObjectivesToFile("FUN");
    
     Ranking ranking = new Ranking(population);
    ranking.getSubfront(0).printFeasibleFUN("FUN_NSGAII_check") ;
    
    c.updateGraph(population);
  
  
    if (indicators != null) {
      logger_.info("Quality indicators") ;
      logger_.info("Hypervolume: " + indicators.getHypervolume(population)) ;
      logger_.info("GD         : " + indicators.getGD(population)) ;
      logger_.info("IGD        : " + indicators.getIGD(population)) ;
      logger_.info("Spread     : " + indicators.getSpread(population)) ;
      logger_.info("Epsilon    : " + indicators.getEpsilon(population)) ;  
     
      int evaluations = ((Integer)algorithm.getOutputParameter("evaluations")).intValue();
      logger_.info("Speed      : " + evaluations + " evaluations") ;      
    } // if
    
    
    
     java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                final Gui applet= new Gui();
                //applet.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                //applet.add(c.prepareAndShowChart());
                applet.addJChartpanel(c.prepareAndShowCPUChart(),c.prepareAndShowMeMChart());
                applet.pack();
                applet.setVisible(true);
               
                //c.update(population2);
            }
        });
     
  } //main
  
  
 
  
} // NSGAII_main


