/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.exec;

import dps.uibk.ac.at.mOeA.MetaNsgaII;
import dps.uibk.ac.at.mOeA.MoEAlgorithm;
import dps.uibk.mooprov.schedulers.MoeaScheduler;
import dps.uibk.mooprov.utility.Agent;

/**
 *
 * @author ennio
 */
public class Automata {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        int a=Integer.parseInt(args[0]);
        double c,f,v;
        
        //int a=0;
        boolean is;
        
        switch(a){
        
            case 0:{
                c=Double.parseDouble(args[1]);
                
                MultiObj(c);    
                 break;
            }
        
            case 1:{
                c=Double.parseDouble(args[1]);//upper
                f=Double.parseDouble(args[2]);//lower
                is=Boolean.parseBoolean(args[3]);//check or not memory utilization
                MBDF(c,f,is);    
                 break; }
                
            case 2:{
                FirtsFit();    
                 break; 
            }

            case 3:{
                c=Double.parseDouble(args[1]);
                 NSGA(c);    
                 break; 
            }
        }
        
        
       
       MoScheduler.main(args);
    }
    
    
    public static void MultiObj(double obj){
    
        Agent.ActualChartData.setXY(obj, -1, -1);
        MoeaScheduler.setAlgName("dps.uibk.ac.at.mOeA.MetaNsgaII");
    } 
    
    public static void MBDF (double up, double down, boolean is){
    
        Agent.ActualChartData.setXY(3, -1, -1);
        Agent.algInfo.setTHDOWN(down);
        Agent.algInfo.setTHUP(up);
        Agent.algInfo.setIsOnlyCpu(is);
        MoeaScheduler.setAlgName("dps.uibk.ac.at.mOeA.SoMBDF");
    
    }
    
    public static void FirtsFit (){
    
        Agent.ActualChartData.setXY(3, -1, -1);
        MoeaScheduler.setAlgName("dps.uibk.ac.at.mOeA.SoFirstFit");
    
    }

    public static void NSGA (double a){

        Agent.ActualChartData.setXY(a, -1, -1);
        MoeaScheduler.setAlgName("dps.uibk.ac.at.mOeA.NsgaII");

    }  
    
}
