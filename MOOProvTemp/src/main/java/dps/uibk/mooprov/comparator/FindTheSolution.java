/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.comparator;

import java.math.BigDecimal;
import java.util.Comparator;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.core.Variable;


/**
 *
 * @author ennio
 */
public class FindTheSolution implements Comparator {

    private Solution toCompare;
    @Override
    public int compare(Object t, Object t1) {
       
        Solution s1 =(Solution)t;
        Solution s2 =(Solution)t1;
        double distanceS1,distanceS2;
        // System.out.println("compare");
        if(toCompare==null)
            return 0;
        
        distanceS1=Euclideandistance(toCompare,s1);
        distanceS2=Euclideandistance(toCompare,s2);
       //  System.out.println("compare finished");
        return (distanceS1<distanceS2)?-1:distanceS1-distanceS2==0?0:1;
        
        //Variable variables1[]=s1.getDecisionVariables();
       // Variable variables2[]=s2.getDecisionVariables();
    }
    
     private double Manhattandistance (Solution o,Solution ot) {
   
    // System.out.println("distance computing");
    double distance=0;
    Solution one,other;
    
    SolutionSet scaledSet=dataExpansion(o, ot);

    one=scaledSet.get(0);
    other=scaledSet.get(1);
    
   if(one.getNumberOfObjectives()>other.getNumberOfObjectives()){
          return 1;
        }
           
        for(int i=0;i<other.getNumberOfObjectives();i++){
        
            try {
               if(one.getObjective(i)<0)
                   continue;
               
               distance +=Math.abs(one.getObjective(i)-other.getObjective(i));
            }
           catch(Exception e){
           
               return 1;
           }
    }
    
        distance = Math.sqrt(distance);
   // System.out.println(distance);
    return distance; 
    }
    
    
    private double Euclideandistance (Solution o,Solution ot) {
   
    // System.out.println("distance computing");
    double distance=0;
    Solution one,other;
    
    SolutionSet scaledSet=dataExpansion(o, ot);

    one=scaledSet.get(0);
    other=scaledSet.get(1);
    
   if(one.getNumberOfObjectives()>other.getNumberOfObjectives()){
          return 1;
        }
           
        for(int i=0;i<other.getNumberOfObjectives();i++){
        
            try {
               if(one.getObjective(i)<0)
                   continue;
               
               distance +=Math.pow(one.getObjective(i)-other.getObjective(i),2);
            }
           catch(Exception e){
           
               return 1;
           }
    }
    
        distance = Math.sqrt(distance);
   // System.out.println(distance);
    return distance; 
    }
 
    public FindTheSolution whithObjectives(double ... objectives){
         toCompare=new Solution(objectives.length);
         
        
        // System.out.println("objectives created");
         
        
        for (int i=0;i<objectives.length;i++){
        
                toCompare.setObjective(i,objectives[i]);
        //    System.out.println("objectives created");
        }
        return this;
    }
   
    private SolutionSet dataExpansion(Solution one,Solution other){
    
        double scaleDifferenceA=0,scaleDifferenceB=0;
        double tempObjValueA,tempObjValueB,max=0d;
        SolutionSet scaledSolutions=new SolutionSet(2);
        
        Solution a=new Solution(one.getNumberOfObjectives());
        Solution b=new Solution(other.getNumberOfObjectives());
        
        for (int i=0;i<one.getNumberOfObjectives();i++){
        
                a.setObjective(i,(int)one.getObjective(i));
                b.setObjective(i,(int)other.getObjective(i));
        //    System.out.println("objectives created");
        }
       
        
        for (int i=0;i<one.getNumberOfObjectives();i++){
        
                
                tempObjValueA=one.getObjective(i)>1?Math.ceil(Math.log10(one.getObjective(i))):0;
               // tempObjValueB=other.getObjective(i)>1?Math.ceil(Math.log10(other.getObjective(i))):0;
                // System.out.println(tempObjValueB);
                 
                if(tempObjValueA>max)
                    max=tempObjValueA;
                
               /* if(tempObjValueB>max)
                    max=tempObjValueB;*/
        }
        
        for (int i=0;i<one.getNumberOfObjectives();i++){
        
                tempObjValueA=one.getObjective(i)>1?Math.ceil(Math.log10(one.getObjective(i))):0;
              //  tempObjValueB=other.getObjective(i)>1?Math.ceil(Math.log10(other.getObjective(i))):0;
               
                scaleDifferenceA=max-tempObjValueA;
               // scaleDifferenceB=max-tempObjValueB;
                //System.out.println(scaleDifference);
                
                if(scaleDifferenceA>0&&a.getObjective(i)!=-1){
                    
                    a.setObjective(i,a.getObjective(i)*Math.pow(10,max-1));
                     b.setObjective(i,b.getObjective(i)*Math.pow(10,max-1));
                }
                
              
        }
        
       
        scaledSolutions.add(a);
        scaledSolutions.add(b);
        
        return scaledSolutions;
    }
}
