/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.crossover;

import dps.uibk.mooprov.solutionType.IntSolutionTypeP;
import dps.uibk.mooprov.solutionType.Placement;
import dps.uibk.mooprov.solutionType.PlacementII;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import jmetal.core.Solution;
import jmetal.encodings.solutionType.BinaryRealSolutionType;
import jmetal.encodings.solutionType.BinarySolutionType;
import jmetal.encodings.solutionType.IntSolutionType;
import jmetal.operators.crossover.SinglePointCrossover;
import jmetal.util.JMException;

/**
 *
 * @author ennio
 */
public class SinglePointCrossoverP extends SinglePointCrossover{

    
     
    public SinglePointCrossoverP(HashMap<String, Object> parameters) {
        super(parameters);
       
        VALID_TYPES=Arrays.asList(BinarySolutionType.class,BinaryRealSolutionType.class,
  		                                            IntSolutionType.class,
                                                            Placement.class,
                                                            PlacementII.class,
                                                            IntSolutionTypeP.class);
   }

    @Override
    public Object execute(Object object) throws JMException {
        return super.execute(object); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Solution[] doCrossover(double probability, Solution parent1, Solution parent2) throws JMException {
        return super.doCrossover(probability, parent1, parent2); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
