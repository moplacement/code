/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.utility;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

/**
 *
 * @author ennio
 */
public class PrintCsv {
         
          private  PrintStream srcWriter=null;
          private  FileOutputStream srcOutfile=null;
          private  PrintCsv myself=null;
          private  String fName="";

    public PrintCsv() {
    
        myself=this;
    
    }
         
        
          
        public PrintCsv AddToFile(String filename) {
            
          if(srcWriter==null){
             
            try {
                fName=filename;
                 //myself=new PrintCsv();
                 srcOutfile = new FileOutputStream(filename);
                 srcWriter = new PrintStream(srcOutfile);
                 return myself;
                 
            } catch (FileNotFoundException ex) {
               System.out.println(ex);
               return null;
            }
            
                
          
          }
          
          else if(fName.compareTo(filename)!=0)
          {
              System.out.println("in qualche modo il nome e errato");
            try {
                 fName=filename;
                 srcWriter.close();
                 srcOutfile = new FileOutputStream(filename);
                 srcWriter = new PrintStream(srcOutfile);
                 return myself;
                 
            } catch (FileNotFoundException ex) {
               System.out.println(ex);
               return null;
            }
            catch (IOException ex) {
               System.out.println(ex);
               return null;
            }
          
          }
         
            return myself;
        }
         
        public PrintCsv Line(String line){
        
            srcWriter.println(line);
            
            return this;
        } 
     
       public void close(){
       
           srcWriter.close();
       
       }
     
     }  

