/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.utility;

import dps.uibk.ac.at.Factories.VmFactory;
import dps.uibk.mooprov.iaas.SimulatedMachine;
import dps.uibk.mooprov.iaas.VirtualMachine;
import dps.uibk.mooprov.workload.GoogleWorkload;
import dps.uibk.mooprov.workload.Workload;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author ennio
 */
public class GoogleTracesUtility {
    
      private  String GoogleDir;
      private  ArrayList<String> GoogleFiles;
      private  String VmsInfo;

    
      private  File VmsSpecifics;
      private  FileReader inputFile;
      private  BufferedReader specifics;
      
      public  VirtualMachine getGoogleWorkload(String dir,String vmInfo) throws FileNotFoundException, IOException{
      
          VmsInfo=vmInfo;
          return getGoogleWorkload(dir);
      
      }
              
      public  VirtualMachine getGoogleWorkload(String dir) throws FileNotFoundException, IOException{
       
           
           File Dir=new File(dir);
            GoogleWorkload w;
       
                       
           if((GoogleFiles==null)||(!GoogleDir.equals(dir))){
           
           if(Dir.isDirectory()){
           
               GoogleDir=dir;
               GoogleFiles=new ArrayList<>(Arrays.asList(Dir.list()));
               inputFile = new FileReader(VmsInfo);
          
          //Instantiate the BufferedReader Class
               specifics = new BufferedReader(inputFile);
           }
           
       }
           if(VmsInfo==null){
               specifics.close();
               throw new FileNotFoundException("The Vms info file is missing ");
           
           }
           
           VmsSpecifics=new File(VmsInfo);
           
           if(VmsInfo.isEmpty()){
               specifics.close();
               throw new FileNotFoundException("The Vms info file is empty ");
           
           }
           
          
           String vM="",vMindex="";
           double cPu=0,
                   mEmory=0;
           
           do {
               
               if((vM=specifics.readLine())==null){
                  
                  specifics.close();
                  throw new FileNotFoundException("The Vms info has no more entries google traces = "+GoogleFiles.size()); 
               }
               
               vMindex=vM.split(",")[2]+"-"+vM.split(",")[3]+".csv";
               
           }while(!GoogleFiles.contains(vMindex));
           
           
            GoogleFiles.remove(vMindex);
            
            cPu=Double.parseDouble(vM.split(",")[9]);
            mEmory=Double.parseDouble(vM.split(",")[10]);
            
            cPu=cPu*64;
            mEmory=mEmory*256;
            
            VirtualMachine vm=VmFactory.Vm(getVMtype(cPu, mEmory), vMindex.split(".csv")[0]);
            
                         
               
              // System.out.println("Created VM- "+vm);
               
               
               w=new GoogleWorkload(GoogleDir+"/"+vMindex,vm);        
     
           
           if(w==null)
                   throw new SimulatedMachine.SimulatedMachineException("google workload null");
           
           vm.setWorkload(w);
           
           return vm;
        }
     
      
       public static String getVMtype(double cpu,double memory){
       
           double cPu=2,
                   mEmory=4,
                   index=0;
           
          
           
           
           for(int power=0;power<6;power++){
           
               index=Math.pow(cPu, power);
               if(cpu<index&&memory<index*4){
               
                   System.out.println(cpu+" "+index);
                   break;
               };
           }
           
           return index>16?"extralarge":index>8?"large":index>2?"medium":index>1?"small":"extrasmall";
         // return "large";
       }
    
       public void closeGoogleStraming() throws IOException{
    
        specifics.close();
    }
    
    public void setGoogleDir(String GoogleDir) {
        this.GoogleDir = GoogleDir;
    }

    public void setVmsInfo(String VmsInfo) {
        this.VmsInfo = VmsInfo;
    }
}


