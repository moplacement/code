package dps.uibk.mooprov.utility;


import dps.uibk.mooprov.comparator.FindTheSolution;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;
import javax.swing.SwingWorker;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.util.Ranking;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnitSource;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ennio
 */
public class ChartCreation {

    //final String charTitle;
    final double refTime;
    final XYSeriesCollection CPUseries,MeMory;
    //private static ChartCreation mySelf=null;
    ChartPanel panel3,panel4;
    private final ArrayList<onUserChoise> myEventQueue;
    
    private final class UpdateGraphsWorkers extends SwingWorker <Double, Double> {

        @Override
        protected Double doInBackground() throws Exception {
            //updateCPUSeries();
            //updateMEMSeries();
           // System.out.println("gfedgd");
            publish(2D);
            return 0D;
        }
        
        @Override
        protected void process(List<Double> chunks) {
                 updateCPUSeries();
                 updateMEMSeries();
            
        
        }
        
    
    
    };
    
    private static class ActualChartData{
            
                private static SolutionSet s;
                private static double x,y,z;
                private final static Object lock=new Object();
                
                public static SolutionSet getS() {
                    synchronized(s){
                        return s;
                    }
                }

                public static void  setS(SolutionSet s) {
                    synchronized(s){
                        ActualChartData.s = s;
                    }
                    
                }

                public static double [] getXYZ() {
                    
                    synchronized(lock)
                    {
                   
                        double ar[]={x,y,z};
                    
                        return ar ;
                    }
                    
                }

                public static void setXY(double x,double y,double z) {
                     synchronized(lock)
                    {
                        ActualChartData.x = x;
                        ActualChartData.y = y;
                        ActualChartData.z = z;
                
                    }
                } 
       
            
            } 
    
    
    public ChartCreation() {
        
                this.myEventQueue = new ArrayList();
   
                
		this.refTime = System.currentTimeMillis();

		CPUseries= new XYSeriesCollection();
		MeMory= new XYSeriesCollection();
               
              //  mySelf=this;
	}
    
    public JPanel prepareAndShowCPUChart(){
    
        ChartPanel panel=prepareAndShowChart(CPUseries,"ParetoFrontA","Migrations","Resources wastage");
        panel.setMouseWheelEnabled(true);
        panel.addChartMouseListener(new MyChartMouseListener(panel));
       
        return panel ;
    
    
    }
    
    public JPanel prepareAndShowMeMChart(){
        
        ChartPanel panel=prepareAndShowChart(MeMory,"ParetoFrontB","Migrations","OvercommitmentRatio");
        panel.setMouseWheelEnabled(true);
        panel.addChartMouseListener(new MyChartMouseListener(panel));
       
        return panel ;
    
    }
    
    public ChartPanel prepareAndShowChart(XYSeriesCollection series,String name,String Xname,String Yname) {

                series.setAutoWidth(true);
		JFreeChart chart = ChartFactory.createScatterPlot(name,Xname,Yname, series, PlotOrientation.VERTICAL, true, true, true);

		final XYPlot plot = chart.getXYPlot();

               
		// ValueAxis axis = plot.getDomainAxis();
		// axis.setAutoRange(true);

		NumberAxis rangeAxis2 = (NumberAxis) plot.getDomainAxis();
                
		 rangeAxis2.setStandardTickUnits(new NumberTickUnitSource());
                 rangeAxis2.setLowerMargin(0);
                 rangeAxis2.setAutoRangeMinimumSize(100, true);
                // rangeAxis2.setLowerMargin(0);
		// axis.setFixedAutoRange(60000.0); // 60 seconds
		//ValueAxis axis = plot.getRangeAxis();
		//axis.setAutoRange(true);

                
                NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
                //rangeAxis.setRange(null);
                
               
                rangeAxis.setStandardTickUnits(new NumberTickUnitSource());
		// axis.setRange(0.0, 10.0);


		final ChartPanel chartPanel = new ChartPanel(chart);
		//chartPanel.setPreferredSize(new java.awt.Dimension());
                

                return chartPanel;
    }
    
    
     private void updateCPUSeries(){
     
        
       
        Solution solution;
        XYSeries newSeries=new XYSeries(ActualChartData.getS().size());
       
        
                
        Iterator<Solution>it=ActualChartData.getS().iterator();
        
       
        
        while(it.hasNext()){
        
            solution=it.next();
            newSeries.add(solution.getObjective(2),solution.getObjective(0));
            
        }
       
        CPUseries.removeAllSeries();
        CPUseries.addSeries(newSeries);
        
        //MeMory.setAutoWidth(true);
       
       
     }
     
     private void updateMEMSeries(){
     
       
      
        Solution solution;
        XYSeries newSeries=new XYSeries(ActualChartData.getS().size());
        
        
        Iterator<Solution>it=ActualChartData.getS().iterator();
        
        while(it.hasNext()){
        
            solution=it.next();
            newSeries.add(solution.getObjective(2),solution.getObjective(1));
            
        }
       
        MeMory.removeAllSeries();
        MeMory.addSeries(newSeries);
        
       // MeMory.setAutoWidth(true);
     }
    
    public void updateGraph(SolutionSet s) {
             Ranking ranking = new Ranking(s);
             ActualChartData.setS(ranking.getSubfront(0));
             UpdateGraphsWorkers w=new UpdateGraphsWorkers();
        try {
            w.execute();
            w.get();
            // updateCPUSeries();
            //  updateMEMSeries();           
        } catch (InterruptedException ex) {
            Logger.getLogger(ChartCreation.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ExecutionException ex) {
            Logger.getLogger(ChartCreation.class.getName()).log(Level.SEVERE, null, ex);
        }
          }

   
    
    public class MyChartMouseListener
        implements ChartMouseListener
    {

        @Override
        public void chartMouseClicked(ChartMouseEvent chartmouseevent)
        {
             System.out.println("2323rererere");
             
            try{ 
            int i = chartmouseevent.getTrigger().getX();
            int j = chartmouseevent.getTrigger().getY();
            Point2D point2d = panel.translateScreenToJava2D(new Point(i, j));
            XYPlot xyplot = (XYPlot)panel.getChart().getPlot();
            //ChartRenderingInfo chartrenderinginfo = panel.getChartRenderingInfo();
            
            java.awt.geom.Rectangle2D rectangle2d = panel.getScreenDataArea();
            //java.awt.geom.Rectangle2D rectangle2d = chartrenderinginfo.getPlotInfo().getDataArea();
            double d = xyplot.getDomainAxis().java2DToValue(point2d.getX(), rectangle2d, xyplot.getDomainAxisEdge());
            double d1 = xyplot.getRangeAxis().java2DToValue(point2d.getY(), rectangle2d, xyplot.getRangeAxisEdge());
            ValueAxis valueaxis = xyplot.getDomainAxis();
           ValueAxis valueaxis1 = xyplot.getRangeAxis();
            double d2 = valueaxis.valueToJava2D(d, rectangle2d, xyplot.getDomainAxisEdge());
           double d3 = valueaxis1.valueToJava2D(d1, rectangle2d, xyplot.getRangeAxisEdge());
          Point point = panel.translateJava2DToScreen(new java.awt.geom.Point2D.Double(d2, d3));
            
            
           if( panel.getChart().getTitle().getText().equals("ParetoFrontA")){
                    System.out.println(panel.getChart().getTitle().getText()+": Mouse coordinates are (" + i + ", " + j + "), in data space = (" + d + ", " + d1 + ").");
                    System.out.println("--> (" + point.getX() + ", " + point.getY() + ")");
                    ActualChartData.x=d1;
                    ActualChartData.z=d;
                    ActualChartData.y=-1;
           }
           else 
           {
           
               System.out.println(panel.getChart().getTitle().getText()+"Mouse coordinates are (" + i + ", " + j + "), in data space = (" + d + ", " + d1 + ").");
               System.out.println("--> (" + point.getX() + ", " + point.getY() + ")");
               ActualChartData.x=-1;
               ActualChartData.z=d;
               ActualChartData.y=d1;
               
               
           }
            
            if(!myEventQueue.isEmpty()){
            
                notifyListener();
            
            }
            
            else{
                //do nothing
            }
            }
            catch(Exception e){
            
                System.out.println("i'm the problem"+e+Arrays.asList(e.getStackTrace()));
            }
            //System.out.println(getParetoFront().best(new FindTheSolution().whithObjectives(d1,-1,d)));
        }

        @Override
        public void chartMouseMoved(ChartMouseEvent chartmouseevent)
        {
        }

        ChartPanel panel;
        

        public MyChartMouseListener(ChartPanel chartpanel)
        {
            System.out.println("rererere");
            panel = chartpanel;
        }
        
        
    }

     public static abstract class onUserChoise{
        
            public abstract void actionToDo(Solution s);
            
            public void Execute(){
                Solution s;
                double x,y,z;
                x=ActualChartData.getXYZ()[0];
                y=ActualChartData.getXYZ()[1];
                z=ActualChartData.getXYZ()[2];
               s=ActualChartData.getS().best(new FindTheSolution().whithObjectives(x,y,z));
               System.out.println("the use choise is -> "+s);
               actionToDo(s);
            }
        
        }

    public void addListener(onUserChoise event) {
         myEventQueue.add(event);
    }
    
    public void removeListener(onUserChoise event) {
         myEventQueue.remove(event);
    }
    
    private void notifyListener(){
    
        for (onUserChoise choise:myEventQueue){
        
            choise.Execute();        
        }
    
    }
    
   /* 
   public static ChartCreation invokeInterface(){
   
       return mySelf ;
   
   } */
}
