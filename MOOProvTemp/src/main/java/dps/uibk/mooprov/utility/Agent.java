/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.utility;

import dps.uibk.mooprov.comparator.FindTheSolution;
import dps.uibk.mooprov.iaas.IaaS;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.util.Ranking;

/**
 *
 * @author ennio
 */
public class Agent {
    
    

    private final static ArrayList<Agent.onUserChoise> myEventQueue=new ArrayList<>();
    
    
     public static class algInfo{
     
         static double  THUP=0.9;
         static double THDOWN=0.6;
         static boolean isOnlyCpu=false;
         
        public static boolean IsOnlyCpu() {
            return isOnlyCpu;
        }

        public static void setIsOnlyCpu(boolean is) {
            isOnlyCpu = is;
        }

        public static double getTHUP() {
            return THUP;
        }

        public static void setTHUP(double tHUP) {
            THUP = tHUP;
        }

        public static double getTHDOWN() {
            return THDOWN;
        }

        public static void setTHDOWN(double tHDOWN) {
            THDOWN = tHDOWN;
        }
        
     
     }
    
    
     public static class ActualChartData{
            
                private static SolutionSet s;
                private static double x,y,z;
                private final static Object lock=new Object();
                
                public static SolutionSet getS() {
                    synchronized(s){
                        return s;
                    }
                }

                public static void  setS(SolutionSet s) {
                    synchronized(s){
                        ActualChartData.s = s;
                    }
                    
                }

                public static double [] getXYZ() {
                    
                    synchronized(lock)
                    {
                   
                        double ar[]={x,y,z};
                    
                        return ar ;
                    }
                    
                }

                public static void setXY(double x,double y,double z) {
                     synchronized(lock)
                    {
                        ActualChartData.x = x;
                        ActualChartData.y = y;
                        ActualChartData.z = z;
                
                    }
                } 
       
            
            } 
     
     
      public static void updateGraph(SolutionSet s) throws InterruptedException {
      
            Ranking ranking = new Ranking(s);
            
            ActualChartData.setS(ranking.getSubfront(0));
            
            Thread t= new ThinkTime();
            t.start();
       
      }
     
     public static abstract class onUserChoise{
        
            public abstract void actionToDo(Solution s);
            
            public void Execute(){
                Solution s;
                double x,y,z;
                x=Agent.ActualChartData.getXYZ()[0];
                y=Agent.ActualChartData.getXYZ()[1];
                z=Agent.ActualChartData.getXYZ()[2];
               s=Agent.ActualChartData.getS().best(new FindTheSolution().whithObjectives(x,y,z));
               System.out.println("the use choise is -> "+s);
               actionToDo(s);
            }
        
        }
     
    public static void addListener(Agent.onUserChoise event) {
         myEventQueue.add(event);
         System.out.println("registered event");
    }
    
    public static void removeListener(Agent.onUserChoise event) {
         myEventQueue.remove(event);
    }
    
    private static void notifyListener(){
    
        for (Agent.onUserChoise choise:myEventQueue){
        
            choise.Execute();        
        }
}


    private static class ThinkTime extends Thread{

        @Override
        public void run() {
            
            try {
                Thread.sleep(3);
            } catch (InterruptedException ex) {
               throw new ExceptionInInitializerError (ex);
            }
            
             if(!myEventQueue.isEmpty()){
            
                notifyListener();
            
            }
        }
    
       
    
    }

}
