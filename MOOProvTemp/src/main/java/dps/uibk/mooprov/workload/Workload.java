/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.workload;

/**
 *
 * @author ennio
 */
public interface Workload {
    public double getMemoryDemand();
    public double getCPUDemand();
}
