/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.workload;

import hu.mta.sztaki.lpds.cloud.simulator.Timed;


/**
 *
 * @author ennio
 */
public class Rect implements Workload{

    private double Amp1, Amp2;
    private final double period;//sec
    private int timestampA=0,timestampB=0;
    private double seed=1;
    private final double MemVariance,CpuVariance;
    private final  UserDefinedFunction amplitudeFunctionCpu,amplitudeFunctionMem;
    private GaussianGenerator genA,genB;
    
    
    public static abstract class UserDefinedFunction {
    
        public abstract double function(double time);
    }
    
    
    
    /**
     * this class generates a periodic semi rectangular
     * demand with a randomly generated amplitude over the given period.
     * The amplitude has a variation determined by the following parameters.
     * @param period the period of the rect
     * @param MemVariability the variance of the Mem demand
     * @param CPUVariability the variance of the CPU demand
     */
    public Rect(double period,double MemVariability,double CPUVariability) {
        
        this.MemVariance=MemVariability;
        this.CpuVariance=CPUVariability;
        
        this.amplitudeFunctionCpu=this.amplitudeFunctionMem=new UserDefinedFunction(){

            @Override
            public double function(double time) {
                return Math.random(); //To change body of generated methods, choose Tools | Templates.
            }
        };
        
        this.period=period;
    }
    
    
    /**
     * this class generates a periodic semi rectangular
     * workload with an amplitude that is determined by the provided function 
     * and a variance related to the following parameters over the given period.
     * @param period the period of the rect
     * @param UserDefinedFunction
     * @param MemVariability the variance of the Mem demand
     * @param CPUVariability the variance of the CPU demand
     */
    public Rect(double period,UserDefinedFunction function,double MemVariability,double CPUVariability) {
        
        this.MemVariance=MemVariability;
        this.CpuVariance=CPUVariability;
        this.amplitudeFunctionCpu=this.amplitudeFunctionMem=function;     
        this.period=period;
    }
    
    
     /**
     * this class generates a periodic semi rectangular
     * workload with an amplitude that is determined by two provided function (one for CPU load and one for memory load) 
     * and a variance related to the following parameters over the given period.
     * @param period the period of the rect
     * @param UserDefinedFunctionMem 
     * @param UserDefinedFunctionCpu
     * @param MemVariability the variance of the Mem demand
     * @param CPUVariability the variance of the CPU demand
     */
    public Rect(double period,UserDefinedFunction functionMem,UserDefinedFunction functionCpu,double MemVariability,double CPUVariability) {
        
        this.MemVariance=MemVariability;
        this.CpuVariance=CPUVariability;
        this.amplitudeFunctionMem=functionMem;
        this.amplitudeFunctionCpu=functionCpu;
        this.period=period;
    }

    @Override
    public double getMemoryDemand() {
        double temp=Timed.getFireCount();
        
        if(((int)(temp/period))>=timestampB){
        
            timestampB=((int)(temp/period))+1;
            Amp1=amplitudeFunctionMem.function(temp);
           // Amp2=Math.random();
            genA=new GaussianGenerator(Amp1, MemVariance, Amp2, CpuVariance);
            //genB=new GaussianGenerator(Amp2, MemVariance, Amp1, CpuVariance); 
        
        } 
        /*
        if(((int)(temp/period))%2==0){
         
            return genA.getMemoryDemand();
            
        }*/
        
        return genA.getMemoryDemand();
    }

    @Override
    public double getCPUDemand() {
        double temp=Timed.getFireCount();
        
         if(((int)(temp/period))>=timestampA){
        
            timestampA=((int)(temp/period))+1;
            //Amp1=Math.random();
           // Amp2=Math.random();
            Amp2=amplitudeFunctionCpu.function(temp);
            //genA=new GaussianGenerator(Amp1, MemVariance, Amp2, CpuVariance);
            genB=new GaussianGenerator(Amp1, MemVariance, Amp2, CpuVariance); 
        
        } 
        
        /*
        if(((int)(temp/period))%2==0){
        
            return genA.getCPUDemand();
        
        }*/
        
        return genB.getCPUDemand();
    }

  
    
    
}
