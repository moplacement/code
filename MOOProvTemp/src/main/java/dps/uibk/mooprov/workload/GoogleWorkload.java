/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.workload;

import dps.uibk.mooprov.iaas.VirtualMachine;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


/**
 *
 * @author ennio
 */
public class GoogleWorkload implements Workload{
    
    private final VirtualMachine vm;
    private final String filename;
    
    private  FileReader inputFilecpu;
    private  FileReader inputFilemem;
    
    private  BufferedReader cpu;
    private  BufferedReader memory;

    public GoogleWorkload(String name,VirtualMachine vm) throws FileNotFoundException, IOException {
        
        this.filename=name;
        this.vm=vm;
        
           inputFilecpu = new FileReader(filename);
           inputFilemem = new FileReader(filename);
          //Instantiate the BufferedReader Class
          cpu = new BufferedReader(inputFilecpu);
          memory = new BufferedReader(inputFilemem);
          
          
   
    }

    @Override
    public double getCPUDemand() {
        try {
            String line = cpu.readLine();
            
            if(line==null){
                
                resetCpuBuffer();
                line = cpu.readLine();
            }
           
            
            line=line.split(",")[5];
            //System.out.println("dsdsc"+line);
            double cpuUsage= 64*Double.parseDouble(line)/(double)(vm.getVCPUs());
            
            if(cpuUsage>1){
               //throw new UnsupportedOperationException("error during cpu usage calcutaion, value greater than 1"+cpuUsage+"in file"+filename);  
            
                cpuUsage=1D;
            
            }
            return cpuUsage;
            
            //return null if the end of the stream has been reached
            
        } catch (IOException ex) {
          throw new UnsupportedOperationException("error during reading of cpu usage from file "+ex);
        }
    }

    @Override
    public double getMemoryDemand() {
       try {
            String line = memory.readLine();
            
            if(line==null){
            
                resetMemoryBuffer();
                line = memory.readLine();
            }
             
            line=line.split(",")[6];
           // System.out.println("dsds"+line);
            double memoryUsage=256*Math.pow(2, 30)*Double.parseDouble(line)/(double)(vm.getVMemory());
            
           if(memoryUsage>1){
               //throw new UnsupportedOperationException("error during memory usage calcutaion, value greater than 1 , "+memoryUsage+ "in file"+filename);  
               memoryUsage=1D;
           }
           return memoryUsage;
            
        } catch (IOException ex) {
          throw new UnsupportedOperationException("error during reading of memory usage from file "+ex);
        }
    }
    
    
    
    public void interruptWorkloadStream() throws IOException{
    
        cpu.close();
        memory.close();
    }
    
   public void resetMemoryBuffer() throws IOException{
   
      memory.close();
      inputFilemem = new FileReader(filename);
      memory = new BufferedReader(inputFilemem);
       
   }
   
    public void resetCpuBuffer() throws IOException{
   
      cpu.close();
      inputFilecpu = new FileReader(filename);
      cpu = new BufferedReader(inputFilecpu);
      
       
   }
    
}
