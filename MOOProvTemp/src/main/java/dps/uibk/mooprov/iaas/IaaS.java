/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.iaas;



import dps.uibk.mooprov.problems.MoeaProvisioning;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import jmetal.core.Solution;
import jmetal.core.Variable;
import org.apache.log4j.Logger;

/**
 *
 * @author ennio
 */
public class IaaS extends SimulatedMachine{
   
    static Logger logger = Logger.getLogger(IaaS.class);
    
    private final List<PhysicalMachine> hOst=new ArrayList<>();

   
    private final List<VirtualMachine> gUest=new ArrayList<>();
    
    private Solution Placement;

   

    public IaaS(Class<? extends IaaS.Scheduler> s) throws SimulatedMachineException{
        
        Scheduler scheduler;
        
        try{
            scheduler=(s.getConstructor(IaaS.class,long.class).newInstance(this,5400)); 
            this.registerEvents(scheduler);
        }
        
        catch(NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | SimulatedMachineException e){
        
            throw new SimulatedMachineException(e);
            
        }
        
        
    }

    public IaaS() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void Shutdown(){
     
         for(VirtualMachine vm:gUest)
             vm.Shutdown();
         
         for(PhysicalMachine pm:hOst)
             pm.Shutdown();
         
         this.deRegisterEvents();
         this.unsubscribe();
     }

    @Override
    public void Suspend() throws SimulatedMachineException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void run() throws SimulatedMachineException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    public static abstract class Scheduler extends SimulatedMachine.MachineEvents{
        
        protected final IaaS myIaas;
        

        public Scheduler(IaaS myIaas, long delay) throws SchedulingException {
            super(delay);
            this.myIaas = myIaas;
        }

        
          public abstract Solution ComputeSchedule() throws SchedulingException;
           
           @Override
        protected void eventAction() {
               System.out.println("ComputeSchedule event action Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean reSubscribe(long freq){
            return isCancelled()? false:subscribe(freq);
        }
        
        
        public static class SchedulingException extends Exception{

        public SchedulingException(Throwable cause) {
            super(cause);
        }

         
        @Override
        public String toString() {
            
            return "<"+Arrays.toString(Thread.getAllStackTraces().get(Thread.currentThread()))+">"+super.toString();
        }        
    
    }
    
    }
    
    
    public void ApplyPlacement(Solution placement) throws PhysicalMachine.MigrationException,Exception{
    
        int vMindex=0,pMindex,indexHost;
        
        ArrayList <VirtualMachine> Allocation []= new ArrayList [hOst.size()];//allocation summary
        HashMap<VirtualMachine,PhysicalMachine> migrationList []= new HashMap [hOst.size()];//vm that have to be migrated
        ArrayList <VirtualMachine> vMincomingRequests []= new ArrayList [hOst.size()];//new vm that are going to be allocated
        
        if(placement.getDecisionVariables()==null)
            throw new Exception("The placement provided is Empty");
            
        
        for(Variable i:placement.getDecisionVariables()){
        
            
            pMindex=(int)i.getValue();
            
            try{
                gUest.get(vMindex);
            }
            catch(ArrayIndexOutOfBoundsException ex) {
                   throw new Exception("the virtual machine with index"+((int)i.getValue())+"does not exist");
            }
            try {
                hOst.get(pMindex);
                
            } catch(ArrayIndexOutOfBoundsException ex) {
                    throw new Exception("the virtual machine with index"+((int)i.getValue())+"does not exist");
             }
            
            
            
            if(Allocation[pMindex]==null){
                Allocation[pMindex]=new ArrayList();
            }
            
           
          
                
                indexHost=hOst.indexOf(getVMhost(gUest.get(vMindex)));
                 System.out.println(IaaS.class+"rere"+getVMhost(gUest.get(vMindex)));
                 //System.out.println(IaaS.class+"rere (host1)"+hOst.get(1));
            
           
           if(indexHost==-1){
               
               if(vMincomingRequests[pMindex]==null){
                vMincomingRequests[pMindex]=new ArrayList();
            }
               
              vMincomingRequests[pMindex].add(gUest.get(vMindex));
              System.out.println(IaaS.class+"new machine added to the list of incoming request");
             
           }
            
            if(indexHost!=pMindex&&indexHost>-1){
                 System.out.println("q never ever enter here"
                      + "");
                 
                  if(migrationList[indexHost]==null){
                     migrationList[indexHost]= new HashMap();
                }
                
                       
                  
               
                    migrationList[indexHost].put(gUest.get(vMindex),hOst.get(pMindex));
                
                    System.out.println(IaaS.class+" migrates the Virtual machine"+gUest.get(vMindex).getIndex()+ "from"+hOst.get(indexHost).getIndex()+"to the server"+hOst.get(pMindex).getIndex());
                
               
            
            
            }
            
            Allocation[pMindex].add(gUest.get(vMindex));
            
            
            vMindex++;
            
        }
    
           
        for (pMindex=0;pMindex<hOst.size();pMindex++){
          //for (HashMap mList:migrationList){
           // if(mList==null){
              
              if(!(migrationList[pMindex]==null)){
                  System.out.println("never ever enter here"
                      + "");
                try{
                
                      System.out.println("migration triggered"
                      + "");
                   
                    if(hOst.get(pMindex).isRunning()){
                        
                       
                     //   System.out.println(IaaS.class+" shutdowns the server"+hOst.get(pMindex));
                        
                       // hOst.get(pMindex).getAllocatedVMs().clear();// clear the VMs list, it has to be Empty otherwise
                        //the shutdown of the PM will also shutdown all the machine in this list.
                        // System.out.println(migrationList[pMindex]);
                        hOst.get(pMindex).migrateVms(migrationList[pMindex]); 
                      
                        System.out.println("migration triggered"
                      + "");
                         
                       // hOst.get(pMindex).Shutdown()
                         // hOst.get(pMindex).Suspend();
                         
                     
                     /*  if(Allocation[pMindex]==null){  
                           System.out.println(IaaS.class+" shutdowns the server"+hOst.get(pMindex));
                           hOst.get(pMindex).Suspend();
                       
                       }*/
                     }
                 // pMindex++;
                 // continue;
                }
                catch( PhysicalMachine.MigrationException e){
                
                   
                    throw e;
                    
                }
                
                    
            }
        
            if((Allocation[pMindex]==null)&&(hOst.get(pMindex).isRunning()) ){  
                           System.out.println(IaaS.class+" shutdowns the server"+hOst.get(pMindex));
                          
                           hOst.get(pMindex).Suspend();
                          // pMindex++;
                          // continue;
                       }
              
             if(Allocation[pMindex]!=null) {
                
                if(!hOst.get(pMindex).isRunning()){
                     System.out.println(IaaS.class+" run again the server"+hOst.get(pMindex));
                    hOst.get(pMindex).run();
                     System.out.println(IaaS.class+" run again the server"+hOst.get(pMindex));
                }
              
                if(vMincomingRequests[pMindex]!=null){
                   System.out.println("new machine on host===="+hOst.get(pMindex));
                   //the new VMs need to be allocated on the same machine with the currently allocated ones.
                  vMincomingRequests[pMindex].addAll(hOst.get(pMindex).getAllocatedVMs());
                  
                  hOst.get(pMindex).allocateVMs(vMincomingRequests[pMindex]);//allocates the new and the old. VMs
                  System.out.println("new machine on host===="+hOst.get(pMindex));
                }
              
                  
               
                
                    logger.debug(IaaS.class+" A total of "+Allocation[pMindex].size()+"VM have been allocated on the host - "+hOst.get(pMindex).getIndex());
                     logger.debug("debugging info host- "+hOst.get(pMindex).getIndex()+
                             "\n Memory requested- "+hOst.get(pMindex).getMemoryRequested()+
                             "\n Cpu requested- "+hOst.get(pMindex).getvCPUsRequested()+
                             "\n Vm allocated- "+hOst.get(pMindex).getNumberofAllocatedVms());
           
                    if(logger.isDebugEnabled())
                     for (VirtualMachine v:hOst.get(pMindex).getAllocatedVMs())
                        logger.debug(hOst.get(pMindex).getVMResourceSharing(v));
              }     
                        
        }
    }
    
    public String PrintIaaSStatus(){
    
        String running="\n-----Running Servers-----\n ",stopped="\n-----Not running Servers-----\n ";
        for (PhysicalMachine pm : hOst) {
            if(pm.isRunning()){
                running+="\n"+pm.getIndex();
            }
            else{
            stopped+="\n"+pm.getIndex();
            }
        }
        
        return running+stopped;
    }

    public List< PhysicalMachine> gethOst() {
        return hOst;
    }

    public List< VirtualMachine> getgUest() {
        return gUest;
    }

    public Solution getPlacement() {
        return Placement;
    }
    
     public void setPlacement(Solution Placement) {
        this.Placement = Placement;
    }
     
     public void addGuest(VirtualMachine vm){
         
             if(!gUest.contains(vm)){
                 gUest.add(vm);
             }
     }
     
     public void removeGuest(VirtualMachine vm) throws Exception{
        int vmIndex,hostIndex;
        PhysicalMachine realPmhost;
        Variable a[];
                
        
           
           vmIndex=gUest.indexOf(vm);
           
           
           hostIndex=(int)Placement.getDecisionVariables()[vmIndex].getValue();
           ArrayList <Variable> temp=new ArrayList<>(Arrays.asList(Placement.getDecisionVariables()));
           temp.remove(vmIndex);
           a=new Variable [temp.size()];
           a=(temp.toArray(a));
           
           System.out.println("new placement size = "+a.length);
           gUest.remove(vm);
           
           Solution newPlacement=new Solution(new MoeaProvisioning("Placement",gUest ,hOst ),a);
           
           for(int i=0;i<Placement.getNumberOfObjectives();i++){
           
               newPlacement.setObjective(i,Placement.getObjective(i));//to maintain data coherent
           
           }
           
           setPlacement(newPlacement);
           //Placement.setDecisionVariables(a);
           
           
           
           
           PhysicalMachine o= getVMhost(vm);
           o.deAllocateVms(Collections.singletonList(vm));
           if(o!=vm.getMyHost())
          {
              throw new Exception ("my host"+vm.getMyHost()+"but founded"+o);
          }
           
        
           vm.Shutdown();
               
          if(Placement.getDecisionVariables().length>this.getgUest().size())
          {
          
              throw new Exception ("error during VM removing procedure,VMs in list= "+gUest.size()+"VMs in Placement= "+Placement.getDecisionVariables().length);
          }
        
            //todo

         //todo
     }
     
     public PhysicalMachine getVMhost(VirtualMachine vm){
         
         for(PhysicalMachine pm: hOst){
         
              if(pm.isThisAllocated(vm))
                  return pm;
         
         }
     
         return null;
     }
     
      public void addHost(PhysicalMachine pm){
         
             if(!hOst.contains(pm)){
              hOst.add(pm);
             }
     }
      
     public boolean isMigrationProcessAlive(){
     
         for(PhysicalMachine pm:hOst){
         
             if(pm.IsMigrationProcessAlive())
                 return true;
         }
         return false;
     
     }
}
