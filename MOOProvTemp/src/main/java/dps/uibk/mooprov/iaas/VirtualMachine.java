/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.iaas;

import dps.uibk.mooprov.workload.Workload;
import hu.mta.sztaki.lpds.cloud.simulator.Timed;

/**
 *
 * @author ennio
 */
public class VirtualMachine extends SimulatedMachine{
    private String index;
    private String pMindex;
    private int vCPUs;
    private double vMemory;            
    private double cPUdemand;
    private double mEMdemand;
    private double dirtingrate;
    private Workload workload;
    

    private PhysicalMachine myHost;

    private MigrationState migrationState;

    
    
    public static enum MigrationState {
    
        none,initial,trasferring,stopAndCopy,activation;
    
    } 
    
    private final SimulatedMachine.MachineEvents runningBehaviour= new SimulatedMachine.MachineEvents(3){

        @Override
        protected void eventAction() {
            updateCPUdemand();
            updateMemdemand();//To change body of generated methods, choose Tools | Templates.
            //System.out.println("update cpu dem fire time ="+ Timed.getFireCount());
        }

            
    

    };
    
    
    private abstract class avgResUtil extends SimulatedMachine.MachineEvents{

        protected Double avgCPUusage=(double) 0;
        protected Double avgMemusage=(double) 0;
        protected long timestamp=0;
        
        public avgResUtil(long delay) {
            super(delay);
        }
    
        public double moovingAVG(double value,Double avg){
        
            double alpha=0.6D;
            avg=alpha*avg+(1-alpha)*value;
            return avg;
        }
        
        public abstract Double getAvgCPUusage();
        public abstract Double getAvgMemusage();
    
    }
    
    
    private final avgResUtil averageUtilization= new avgResUtil(3){

        @Override
        protected void eventAction() {
            
             if(timestamp==0){
            
                avgCPUusage=avgMemusage=0D;
                
            }
            
            avgCPUusage=moovingAVG(getCPUdemand(),avgCPUusage);
            avgMemusage=moovingAVG(getMEMdemand(),avgMemusage);//To change body of generated methods, choose Tools | Templates.
            timestamp+=1;
            
                   //System.out.println("update cpu dem fire time ="+ Timed.getFireCount());
        }

        

        

        @Override
        public Double getAvgCPUusage() {
            timestamp=0;
                    
            return avgCPUusage;
        }

        @Override
        public Double getAvgMemusage() {
            timestamp=0;
            
            return avgMemusage;
        }  
    

        
        
    };

    public VirtualMachine(String index,String PM,int VCPUs,double Vmem) {
       
        this.index=index;
        this.pMindex=PM;
        this.vCPUs=VCPUs;
        this.vMemory=Vmem;
        
        this.registerEvents(runningBehaviour,averageUtilization);
        this.state=State.Running;
        migrationState=MigrationState.none;
    }
    
    public VirtualMachine(String index,String PM,int VCPUs,double Vmem,Workload load) {
        
        this(index,PM,VCPUs,Vmem);
        this.workload=load;
        
    }

    public double getCPUdemand() {
        
         if(State.ShutDown.equals(this.state)||State.Suspended.equals(this.state)||MigrationState.stopAndCopy.equals(this.migrationState)||MigrationState.activation.equals(this.migrationState))
            return 0D;
         
        return cPUdemand;
    }

    public double getMEMdemand() {
        
         if(State.ShutDown.equals(this.state)||State.Suspended.equals(this.state)||MigrationState.stopAndCopy.equals(this.migrationState)||MigrationState.activation.equals(this.migrationState))
            return 0D;
         
        return mEMdemand;
    }

    public int getVCPUs() {
        return vCPUs;
    }

    public String getPMindex() {
        return pMindex;
    }

    public double getDirtingrate() {
        return Math.random()*100E6;
    }

    public double getVMemory() {
        return vMemory;
    }

    public String getIndex() {
        return index;
    }

    
    public void updateCPUdemand() {
        cPUdemand=workload.getCPUDemand();
    }
    
    public void updateMemdemand() {
        mEMdemand=workload.getMemoryDemand();
    }
    
    @Override
    public String toString(){
    
        String out="VM "+this.index+"description-\n"
        +"VCPU"+this.vCPUs +"\n"
        +"VMEM"+this.vMemory +"\n"
        +"CPUdemand"+this.cPUdemand +"\n"
        +"MEMdemand"+this.mEMdemand +"\n";
        
        return out;
    
    }
    
    @Override
     public void Shutdown(){
     
         this.deRegisterEvents();
         this.unsubscribe();
         this.state=State.ShutDown;
     }
     
     
     public Workload getWorkload() {
        return workload;
    }

    public void setWorkload(Workload workload) {
        this.workload = workload;
    }
    
     public PhysicalMachine getMyHost() {
        return myHost;
    }

    public void setMyHost(PhysicalMachine myHost) {
        this.myHost = myHost;
        
        //System.out.println("Set my host++++++++++++++++++++++"+myHost);
    }

    @Override
    public void Suspend() throws SimulatedMachineException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void run() throws SimulatedMachineException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public MigrationState getMigrationState() {
        return migrationState;
    }

    public void setMigrationState(MigrationState migrationState) {
        this.migrationState = migrationState;
    }
    
    
    
    public double forecatsCPUusage() {
             return averageUtilization.getAvgCPUusage();
             
        }

        

    public double forecastMemusage() {
            return averageUtilization.getAvgMemusage();
        }
    
}
