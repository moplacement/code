/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.ac.at.mOeA;

import dps.uibk.ac.at.algorithms.BaseAlgorithm;
import dps.uibk.ac.at.algorithms.MetaNSGAII;
import dps.uibk.ac.at.algorithms.baseNSGAII;

import dps.uibk.ac.at.mutation.ProvMutationFactory;

import dps.uibk.mooprov.crossover.ProvCrossoverFactory;
import dps.uibk.mooprov.problems.MoeaProvisioning;
import dps.uibk.mooprov.problems.MoeaProvisioningII;
import java.util.HashMap;
import jmetal.core.Algorithm;
import jmetal.core.Problem;

import jmetal.core.SolutionSet;
import jmetal.operators.crossover.Crossover;
import jmetal.operators.mutation.Mutation;
import jmetal.operators.selection.Selection;
import jmetal.operators.selection.SelectionFactory;
import jmetal.util.JMException;
import jmetal.util.comparators.ObjectiveComparator;
import org.apache.log4j.Logger;

/**
 *
 * @author ennio
 */
public class MetaNsgaII extends MoEAlgorithm{

     private final Logger logger_=Logger.getLogger(NsgaII.class);
     private final Algorithm algorithm ; // The algorithm to use
     
     // HYPER PARAMETERS
     private final int populationSize =200;
     private final int maxEvaluations=200000; //Meta algorithm iterations
     private final int baseEvaluations=50000; //base algorithms iterations (maxEvaluations=baseEvaluation*N, where N-1 is the number of times the population is merged)
      
    public MetaNsgaII(Problem p) throws JMException {
        
        MoeaProvisioning problem=((MoeaProvisioning)p);
        
        MoeaProvisioning prob1=new MoeaProvisioningII("Placement",problem.getvMList(), problem.getpMList());
        MoeaProvisioning prob2=new MoeaProvisioningII("PlacementII",problem.getvMList(), problem.getpMList());
        
        
        prob1.setComparator(problem.getComparator());
        prob2.setComparator(problem.getComparator());
        
        
        ((MoeaProvisioning)prob1).setInitialSched(problem.getInitialSched());
        ((MoeaProvisioning)prob2).setInitialSched(problem.getInitialSched());
        
        BaseAlgorithm[] algorithms=new BaseAlgorithm[2];
        
        algorithms[0]=new baseNSGAII(prob1);
        algorithms[1]=new baseNSGAII(prob2);
        
        setAlgorithm(algorithms[0]);
        setAlgorithm(algorithms[1]);
        
        ObjectiveComparator [] comparators=new ObjectiveComparator[2];
        // Migration comparison
        comparators[0]=new ObjectiveComparator(2,true); 
        comparators[1]=new ObjectiveComparator(2,false);
        
        /*CPu comparison
        comparators[0]=new ObjectiveComparator(0,false); 
        comparators[1]=new ObjectiveComparator(0,true);
        */
        algorithm = new MetaNSGAII(null);
        
        algorithm.setInputParameter("populationSize",populationSize);
        algorithm.setInputParameter("maxEvaluations",maxEvaluations);
        
        algorithm.setInputParameter("algorithms",algorithms);
        algorithm.setInputParameter("comparators",comparators);
        
         algorithm.setInputParameter("baseEvaluations",baseEvaluations);
        
        algorithm.setInputParameter("indicators",null);
        
        
        
    }

    @Override
    public SolutionSet getComputedPlacements() throws JMException, ClassNotFoundException {
        long initTime = System.currentTimeMillis();
        SolutionSet population;
        
        
             population = algorithm.execute();
        
         
        long estimatedTime = System.currentTimeMillis() - initTime;
    
    // Result messages 
        logger_.info("Total execution time: "+estimatedTime + "ms");
        logger_.info("Variables values have been writen to file VAR");
        population.printVariablesToFile("Pareto/VAR");    
        logger_.info("Objectives values have been writen to file FUN");
        population.printObjectivesToFile("Pareto/FUN");
    
        return population;
    }
    
   
    
     private void setAlgorithm(Algorithm algorithm) throws JMException{
         
        // Algorithm parameters
          algorithm.setInputParameter("populationSize",populationSize);
          algorithm.setInputParameter("maxEvaluations",maxEvaluations);
          
        // algorithm.setInputParameter("archiveSize", 100);
        // Mutation and Crossover for Real codification
        HashMap parameters = new HashMap();
        
        
          parameters.put("probability", 0.9) ;
         
        //parameters.put("distributionIndex", 20.0) ;
        Crossover crossover = ProvCrossoverFactory.getCrossoverOperator("SinglePointCrossoverP", parameters);
        

         parameters = new HashMap() ;
        parameters.put("probability", 1.0/algorithm.getProblem().getNumberOfVariables()) ;
        // parameters.put("probability", 0.0001) ;
        //parameters.put("distributionIndex", 20.0) ;
        Mutation mutation = ProvMutationFactory.getMutationOperator("BitFlipMutationP", parameters);
         

    // Selection Operator 
        parameters = null ;
                                  
        Selection selection = SelectionFactory.getSelectionOperator("BinaryTournament2", parameters);
        

    // Add the operators to the algorithm
        algorithm.addOperator("crossover",crossover);
        algorithm.addOperator("mutation",mutation);
        algorithm.addOperator("selection",selection);
        Object indicators=null;

    // Add the indicator object to the algorithm
        algorithm.setInputParameter("indicators", indicators) ; 
     
     
     }
    
}
