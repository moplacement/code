/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.ac.at.mutation;

import java.util.HashMap;
import jmetal.operators.mutation.Mutation;
import jmetal.operators.mutation.MutationFactory;
import jmetal.util.JMException;

/**
 *
 * @author ennio
 */
public class ProvMutationFactory {
    
   public static Mutation getMutationOperator(String name, HashMap parameters) throws JMException{
        
    if(name.equalsIgnoreCase("BitFlipMutationP"))
      return new BitFlipMutationP(parameters);
    
    else return MutationFactory.getMutationOperator(name,parameters);
   
   }
}
