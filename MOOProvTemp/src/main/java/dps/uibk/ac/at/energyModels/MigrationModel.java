/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.ac.at.energyModels;

import dps.uibk.mooprov.iaas.PhysicalMachine;
import dps.uibk.mooprov.iaas.VirtualMachine;
import static org.apache.commons.math3.util.FastMath.log;
/**
 *
 * @author ennio
 */
public class MigrationModel {
    
    
    private double networkBwt=(3.5E8);
    
    
    private enum Source1{
    
        XeonE52690( 
         //initiation;
         1.71,
         1.41,
         165,
         //transfer;
         2.4,
         1.08E-6,
         200,
         0.4,
         1.41,
         //activation;
         2.37,
         0,
         150),
        Opteron8356(//initiation;
         1.71,
         1.41,
         708.3,
         //transfer;
         2.4,
         1.08E-6,
         421.74,
         0.4,
         1.41,
         //activation;
         2.37,
         0,
         662.5),
        DefaultHost( //initiation;
         1.71,
         1.41,
         400,
         //transfer;
         2.4,
         1.08E-6,
         300,
         0.4,
         1.41,
         //activation;
         2.37,
         0,
         350)
        
        ;
        
        
         private final double Alphai,
                              Betai, 
                              Consti,
                 //initiation;
                              Alphat,
                              Betat,
                              Constt,
                              Lambdat,
                              Gammat,
                 //transfer;
                              Alphaa,
                              Betaa,
                              Consta
                         ;

       
         Source1(double Alphai,
                 double Betai, 
                 double Consti,
                 //initiation;
                 double Alphat,
                 double Betat,
                 double Constt,
                 double Lambdat,
                 double Gammat,
                 //transfer;
                 double Alphaa,
                 double Betaa,
                 double Consta
                //activation;
         )
         {
            this.Alphai=Alphai;
            this.Betai=Betai; 
            this.Consti=Consti;
                 //initiation;
            this.Alphat=Alphat;
            this.Betat=Betat;
            this.Constt=Constt;
            this.Lambdat=Lambdat;
            this.Gammat=Gammat;
                 //transfer;
            this.Alphaa=Alphaa;
            this.Betaa=Betaa;
            this.Consta=Consta;
         } 
         
         
        private double getAlphai() {
            return Alphai;
        }

        private double getBetai() {
            return Betai;
        }

        private double getConsti() {
            return Consti;
        }

        private double getAlphat() {
            return Alphat;
        }

        private double getBetat() {
            return Betat;
        }

        private double getConstt() {
            return Constt;
        }

        private double getLambdat() {
            return Lambdat;
        }

        private double getGammat() {
            return Gammat;
        }

        private double getAlphaa() {
            return Alphaa;
        }

        private double getBetaa() {
            return Betaa;
        }

        private double getConsta() {
            return Consta;
        }
        
        public double getInitializationPower(double Hostutil,double vMUtil){
          
           return (Hostutil*getAlphai()+
                   getBetai()*vMUtil+
                   getConsti());
           
      }
       
       public double getTransferPower(double Hostutil,double vMUtil,double dirtyingPerc,double networkBwt){
       
           return (Hostutil*getAlphat()+
                   getBetat()*networkBwt+
                   getGammat()*dirtyingPerc+
                   getLambdat()*vMUtil+
                   getConstt()
                   );
           
      }
      
        public double getActivationPower(double Hostutil,double vMUtil){
       
           return (Hostutil*getAlphaa()+
                   getBetaa()*vMUtil+
                   getConsta());
           
      }
    
    }
   
    
    private enum Target1{
   
        XeonE52690(//initiation;
          3.18,
         0,
         162,
         //transfer;
         2.56,
         5.49E-7,
         210,
         0.4,
         0,
         //activation;
         1.88,
         17.01,
         100),
         Opteron8356( 
         //initiation;
         3.18,
         0,
         596.06,
         //transfer;
         2.56,
         5.49E-7,
         520.214,
         0.4,
         0,
         //activation;
         1.88,
         17.01,
         100),
        DefaultHost( //initiation;
          3.18,
         0,
         192,
         //transfer;
         2.56,
         5.49E-7,
         260,
         0.4,
         0,
         //activation;
         1.88,
         17.01,
         150)
        
        ;
        
        
         private final double Alphai,
                              Betai, 
                              Consti,
                 //initiation;
                              Alphat,
                              Betat,
                              Constt,
                              Lambdat,
                              Gammat,
                 //transfer;
                              Alphaa,
                              Betaa,
                              Consta
                         ;

       
         Target1(double Alphai,
                 double Betai, 
                 double Consti,
                 //initiation;
                 double Alphat,
                 double Betat,
                 double Constt,
                 double Lambdat,
                 double Gammat,
                 //transfer;
                 double Alphaa,
                 double Betaa,
                 double Consta
                //activation;
         )
         {
            this.Alphai=Alphai;
            this.Betai=Betai; 
            this.Consti=Consti;
                 //initiation;
            this.Alphat=Alphat;
            this.Betat=Betat;
            this.Constt=Constt;
            this.Lambdat=Lambdat;
            this.Gammat=Gammat;
                 //transfer;
            this.Alphaa=Alphaa;
            this.Betaa=Betaa;
            this.Consta=Consta;
         } 
         
         
        private double getAlphai() {
            return Alphai;
        }

        private double getBetai() {
            return Betai;
        }

        private double getConsti() {
            return Consti;
        }

        private double getAlphat() {
            return Alphat;
        }

        private double getBetat() {
            return Betat;
        }

        private double getConstt() {
            return Constt;
        }

        private double getLambdat() {
            return Lambdat;
        }

        private double getGammat() {
            return Gammat;
        }

        private double getAlphaa() {
            return Alphaa;
        }

        private double getBetaa() {
            return Betaa;
        }

        private double getConsta() {
            return Consta;
        }
        
        public double getInitializationPower(double Hostutil,double vMUtil){
          
           return (Hostutil*getAlphai()+
                   getBetai()*vMUtil+
                   getConsti());
           
      }
       
       public double getTransferPower(double Hostutil,double vMUtil,double dirtyingPerc,double networkBwt){
       
           return (Hostutil*getAlphat()+
                   getBetat()*networkBwt+
                   getGammat()*dirtyingPerc+
                   getLambdat()*vMUtil+
                   getConstt()
                   );
           
      }
      
        public double getActivationPower(double Hostutil,double vMUtil){
       
           return (Hostutil*getAlphaa()+
                   getBetaa()*vMUtil+
                   getConsta());
           
      }
    
    }
    
    private enum Source{
       //Source host
        //initiation;
        
         Alphai(1.71),
         Betai(1.41),
         Consti1(708.3),
         Consti2(165),
         
         //transfer;
         Alphat(2.4),
         Betat(1.08E-6),
         Constt1(421.74),
         Constt2(200),
         Lambdat(0.4),
         Gammat(1.41),
         //activation;
         
         Alphaa(2.37),
         Betaa(0),
         Consta1(662.5),
         Consta2(150),
         
         
         //Source host
        //initiation;
        
        
         
         ;
        
        
        private final double value;
    
        Source(double value){
            this.value=value;
        } 

        public double getvalue() {
            return value;
        }
        
        
      
      static double getInitializationPower(double Hostutil,double vMUtil){
          
           return (Hostutil*Alphai.getvalue()+
                   Betai.getvalue()*vMUtil+
                   Consti2.getvalue());
           
      }
       
       static double getTransferPower(double Hostutil,double vMUtil,double dirtyingPerc,double networkBwt){
       
           return (Hostutil*Alphat.getvalue()+
                   Betat.getvalue()*networkBwt+
                   Gammat.getvalue()*dirtyingPerc+
                   Lambdat.getvalue()*vMUtil+
                   Constt2.getvalue()
                   );
           
      }
      
        static double getActivationPower(double Hostutil,double vMUtil){
       
           return (Hostutil*Alphaa.getvalue()+
                   Betaa.getvalue()*vMUtil+
                   Consta2.getvalue());
           
      }
        
       double getPowerModel(){
       
           return 3;
           
      }
      
     
       
     // private final double value;
    }
    
    
    private enum Target{
       //target host
        //initiation;
        
         Alphai(3.18),
         Betai(0),
         Consti1(596.06),
         Consti2(162),
         
         //transfer;
         Alphat(2.56),
         Betat(5.49E-7),
         Constt1(520.214),
         Constt2(210),
          Lambdat(0.4),
         Gammat(0),
         //activation;
         
         Alphaa(1.88),
         Betaa(17.01),
         Consta1(499.56),
         Consta2(100),
             
         ;
        Target(double value){
         this.value=value;
        } 

        public double getvalue() {
            return value;
        }
        
        private final double value;
        
         static double  getInitializationPower(double Hostutil,double vMUtil){
       
           return (Hostutil*Alphai.getvalue()+
                   Betai.getvalue()*vMUtil+
                   Consti2.getvalue());
           
      }
       
       static double getTransferPower(double Hostutil,double vMUtil,double dirtyingPerc,double networkBwt){
       
           return (Hostutil*Alphat.getvalue()+
                   Betat.getvalue()*networkBwt+
                   Gammat.getvalue()*dirtyingPerc+
                   Lambdat.getvalue()*vMUtil+
                   Constt2.getvalue()
                   );
           
      }
      
      static  double getActivationPower(double Hostutil,double vMUtil){
       
           return (Hostutil*Alphaa.getvalue()+
                   Betaa.getvalue()*vMUtil+
                   Consta2.getvalue());
           
      }
    }
    
    private double getNumberofRounds(double initialVmMemorySize, double dirtyingRate,double networkBWT){
    
        double arg,res,base;
        
        arg=networkBWT/initialVmMemorySize;
        
    //    System.out.println("arg"+ arg);
        
        base=dirtyingRate/networkBWT;
        
     //   System.out.println("base"+ base);
        
        res=log(base,arg);
        
     //    System.out.println("rounds"+ res);
         
         res=Math.round(res);
         
         return res>3?(res<10?res:10):3;
    }
    
    private double getMigrationTime(double initialVmMemorySize, double dirtyingRate,double networkBWT){
       
    
        double var,res,lambda,n;
        
        var=(initialVmMemorySize)/networkBWT;
        
        //System.out.println("initialmemory"+ var);
        
        lambda=dirtyingRate/networkBWT;
        
     //    System.out.println("migration time"+ lambda);
        
        n=getNumberofRounds(initialVmMemorySize, dirtyingRate, networkBWT);
        
      //   System.out.println("migration time"+ n);
        
        res=var*((1-Math.pow(lambda, n+1))/(1-lambda));
        
      //  System.out.println("migration time"+ res);
        
        return Math.nextUp(res); //sec
    }
    
    
    
    public double getPowerConsumption(VirtualMachine vm,PhysicalMachine source,PhysicalMachine target){
    
         double dirtyPagesPerc=(vm.getDirtingrate()*1)/(8*vm.getVMemory());
    
         
       return vm.getMigrationState().equals(VirtualMachine.MigrationState.initial)?
        
               Source.getInitializationPower(source.getOverallMachineCPUUtilization(), source.getRealCPUUtilization(vm))+
               Target.getInitializationPower(target.getOverallMachineCPUUtilization(), 0)

               :vm.getMigrationState().equals(VirtualMachine.MigrationState.activation)?//+
               Source.getActivationPower(source.getOverallMachineCPUUtilization(),0 )+
               Target.getActivationPower(target.getOverallMachineCPUUtilization(), target.getRealCPUUtilization(vm))//+
               
               :vm.getMigrationState().equals(VirtualMachine.MigrationState.trasferring)||
                vm.getMigrationState().equals(VirtualMachine.MigrationState.stopAndCopy)?
               Source.getTransferPower(source.getOverallMachineCPUUtilization(), source.getRealCPUUtilization(vm), dirtyPagesPerc, networkBwt)//+
               +Target.getTransferPower(target.getOverallMachineCPUUtilization(), 0, dirtyPagesPerc, networkBwt)
               
               :0;
    
       //watt/sec
    
    }
    
   /* public double getEnergyConsumption(VirtualMachine vm,PhysicalMachine source,PhysicalMachine target){
    
        double initTime,TransferTime,activationTime,dirtyPagesPerc;
        double initialVmMemorySize=vm.getMEMdemand()*vm.getVMemory()*8; //in bit not bytes
        
        initTime=Math.nextUp(initialVmMemorySize/networkBwt);
        
        activationTime=1;
        
      //  System.out.println("sadsdsdsa"+initialVmMemorySize);
        double migrationDuration=getMigrationTime(initialVmMemorySize, vm.getDirtingrate(),networkBwt);
        
        //migrationDuration=migrationDuration;
        
        TransferTime=migrationDuration-initTime-activationTime;
        
        
        
        //sum 
        
        double energyConsumptionInit=0,energyConsumptionTransfer=0,energyConsumptionActivation=0;
        
        for(int t=0;t<=initTime;t++){
        
            energyConsumptionInit+=( Source.getInitializationPower(source.getOverallMachineCPUUtilization(), source.getCPUUtilization(vm))+
                    
                                     Target.getInitializationPower(target.getOverallMachineCPUUtilization(),0));
                   
            
        }
        
        dirtyPagesPerc=(vm.getDirtingrate()*TransferTime)/(8*vm.getVMemory());
        
        for(int t=0;t<=TransferTime;t++){
        
            energyConsumptionTransfer+=( Source.getTransferPower(source.getOverallMachineCPUUtilization(), source.getCPUUtilization(vm),dirtyPagesPerc,networkBwt)+
                    
                                     Target.getTransferPower(target.getOverallMachineCPUUtilization(), 0,0,networkBwt));
                   
            
        }
        
         for(int t=0;t<=activationTime;t++){
        
            energyConsumptionActivation+=( Source.getActivationPower(source.getOverallMachineCPUUtilization(), 0)+
                    
                                     Target.getActivationPower(target.getOverallMachineCPUUtilization(),vm.getCPUdemand()));
                   
            
        }
        
        return energyConsumptionInit+energyConsumptionTransfer+energyConsumptionActivation; //Joule 
    }*/
    
    
    public double getEnergyConsumption(VirtualMachine vm,PhysicalMachine source,PhysicalMachine target) throws Exception{
    
        double initTime,TransferTime,activationTime,dirtyPagesPerc,mDrByte;
        double initialVmMemorySize=vm.getMEMdemand()*vm.getVMemory(); //in bytes
        
        Source1 src=getSourcePMMigrationModel(source);
        Target1 trg=getTargetPMMigrationModel(target);
        
        
        initTime=(8*initialVmMemorySize)/networkBwt;//in sec
        
        activationTime=1;// 1sec
        
        mDrByte=vm.getDirtingrate()/8;//in Byte
        
      // System.out.println("init TIme"+initTime);
        double migrationDuration=getMigrationTime(initialVmMemorySize, mDrByte,networkBwt/8);//BWT in bytes
        
        //migrationDuration=migrationDuration;
        
        TransferTime=migrationDuration-initTime;
        
        if (TransferTime<0)
            throw new Exception("tx time <0"+ migrationDuration+"-"+initTime);
        
        //sum 
        
        double energyConsumptionInit=0,energyConsumptionTransfer=0,energyConsumptionActivation=0;
        
      
        
            energyConsumptionInit=initTime*( src.getInitializationPower(source.getOverallMachineCPUUtilization(), source.getCPUUtilization(vm))+
                    
                                     trg.getInitializationPower(target.getOverallMachineCPUUtilization(),0));
                   
            
        
        dirtyPagesPerc=(vm.getDirtingrate()*TransferTime)<(8*vm.getMEMdemand()*vm.getVMemory())?
                (vm.getDirtingrate()*TransferTime):(8*vm.getMEMdemand()*vm.getVMemory());
        
        dirtyPagesPerc=(vm.getDirtingrate()*TransferTime)/(8*vm.getVMemory());
        
      //  System.out.println("dirty "+dirtyPagesPerc);
        
            energyConsumptionTransfer=TransferTime*( src.getTransferPower(source.getOverallMachineCPUUtilization(), source.getCPUUtilization(vm),dirtyPagesPerc,networkBwt/8)+
                    
                                     trg.getTransferPower(target.getOverallMachineCPUUtilization(), 0,0,networkBwt/8));//BWT in bytes
                   
            
        
        
         
        
            energyConsumptionActivation=activationTime*( src.getActivationPower(source.getOverallMachineCPUUtilization(), 0)+
                    
                                     trg.getActivationPower(target.getOverallMachineCPUUtilization(),vm.getCPUdemand()));
                   
            
        
        
        return energyConsumptionInit+energyConsumptionTransfer+energyConsumptionActivation; //Joule 
    }
    
    
    private Source1 getSourcePMMigrationModel(PhysicalMachine pm){
    
        String type=pm.getIndex().split("-")[0];
        
        switch(type){
    
        case"Xeon":
            return Source1.XeonE52690; 
        case"Opteron":
            return Source1.Opteron8356;
        default:
            return Source1.DefaultHost;
                
        }
    
    }
    
    private Target1 getTargetPMMigrationModel(PhysicalMachine pm){
    
        String type=pm.getIndex().split("-")[0];
        
        switch(type){
    
        case"Xeon":
            return Target1.XeonE52690; 
        case"Opteron":
            return Target1.Opteron8356;
        default:
            return Target1.DefaultHost;
                
        }
    
    }
}
