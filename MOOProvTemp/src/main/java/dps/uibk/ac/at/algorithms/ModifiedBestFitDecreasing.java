/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.ac.at.algorithms;

import dps.uibk.ac.at.energyModels.MigrationModel;
import dps.uibk.ac.at.energyModels.PMpowerModel;
import dps.uibk.mooprov.iaas.PhysicalMachine;
import dps.uibk.mooprov.iaas.VirtualMachine;
import dps.uibk.mooprov.problems.MoeaProvisioning;
import dps.uibk.mooprov.problems.MoeaProvisioningII;
import dps.uibk.mooprov.utility.Agent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import jmetal.core.Algorithm;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.core.Variable;
import jmetal.encodings.variable.Int;
import jmetal.util.JMException;
import static org.apache.commons.math3.util.FastMath.log;
import org.apache.log4j.Logger;

/**
 *
 * @author ennio
 */
public class ModifiedBestFitDecreasing extends Algorithm{
    
    
    private final    MoeaProvisioning   problem   ; // The problem to solve
    private final    Solution oldPlacement ; // The old placement to consider
    private ArrayList<PM> bins;
    private ArrayList<VM> items; //not to be sorted
    private final PMpowerModel powermodel;
    private MigrationModel migModel;
    private HashMap  parameters ; // Operator parameters
    private final double THRESH_UP=Agent.algInfo.getTHUP(),THRESH_LOW=Agent.algInfo.getTHDOWN();
    private boolean IsOnlyCpu=Agent.algInfo.IsOnlyCpu();
    private  double CAP_MAX=1.0;
    
    private final static List listOfValidProblems=Arrays.asList(MoeaProvisioning.class,MoeaProvisioningII.class);
    static Logger logger = Logger.getLogger(ModifiedBestFitDecreasing.class);
    
   private class VM{

        public VM(double cpu, double memory,int index) {
            
            this.memory = memory;
            this.cpu = cpu;
            this.index=index;
           // cost=ratio=0;
            cpuUtil=problem.getvMList().get(index).getCPUdemand()*
                    cpu;
           // System.out.println("okokokoko"+cpuUtil);
            memoryUtil=problem.getvMList().get(index).getMEMdemand()*
                    memory;
        }
    
        public double ratio; //benefit cost ratio (see the PMap algorithm )
       // public double cost;
        public double memory;
        public double cpu;
        public int index;
        public double cpuUtil;
        public double memoryUtil;
        //public int source=-1;//not allocated
        //public int target=-1;
    }
    
    private class PM{

        public PM(double cpu, double memory,int index) {
            this.memory = memory;
            this.cpu = cpu;
            this.index=index;
            
            slope=0;
        }
        
        public double slope;
        public double memory;
        public double cpu;
        public int index;
        public double idlePower;
        public double maxPower;
        public double mig;//used in iFFD

    }
    
    
    public ModifiedBestFitDecreasing(Problem problem) throws JMException {
        super(problem);
       
        if(!listOfValidProblems.contains(problem.getClass())){
            throw new JMException("The problem :"+problem.getClass()+"is not a valid one for this algorithm");
        }
        
       this.problem=(MoeaProvisioning)problem;
       this.oldPlacement=this.problem.getInitialSched();
       
        if(problem instanceof MoeaProvisioning){
        
            this.items=new ArrayList();
            this.bins=new ArrayList();
            
            List <VirtualMachine>Items=((MoeaProvisioning)problem).getvMList();
            List <PhysicalMachine>bIns=((MoeaProvisioning)problem).getpMList();
            
            VirtualMachine vm;
            PhysicalMachine pm;
            VM vmItem;
            PM pmItem;
           
            
            for (int i=0;i<Items.size();i++){
            
                vm=Items.get(i);
              
                
                this.items.add(new VM(vm.getVCPUs(),vm.getVMemory(),i));

                  
            }
            
            
            
            for (int i=0;i<bIns.size();i++){
            
                pm=bIns.get(i);
                pmItem=new PM(pm.getVCPUs(),pm.getMemoryCapacity(),i);
                pmItem.maxPower=pm.getMaxPWR();
                pmItem.idlePower=pm.getIdlePWR();
                
                this.bins.add(pmItem);          
            
            }
        }
        
       this.powermodel=new PMpowerModel();
       this.migModel=new MigrationModel();
    }

    @Override
    public SolutionSet execute() throws JMException, ClassNotFoundException {
        
        //inputs
        ArrayList<VM>VMlist=getlistofnewVM(items, oldPlacement);
        ArrayList<PM>binsTmp=new ArrayList(bins);
        
        Solution allocation0=new Solution(this.oldPlacement);
        
        //compute the list of migrations adding the new incoming VMs 
        Solution allocationN=MBFD(binsTmp,VMlist,allocation0).get(0);
        
        System.out.println("Mpp- original placement: "+Arrays.toString(allocation0.getDecisionVariables()));
        System.out.println("Mpp- computed placement: "+Arrays.toString(allocationN.getDecisionVariables()));
        
        ArrayList <VM>migrations;
        /*
        System.out.println("Mpp- computed migrationList: "+migrations.size());
        
        updateCostBenefit(migrations);
        
        
        
        while(migrations.size()>0){
    
            Collections.sort(migrations,new DecreasingBnefitCostOrder());
            VM vM=migrations.get(0);
        
            if(computeProfit(vM)>vM.cost){//migrate
            
               
                migrations.remove(vM);
                updateCostBenefit(migrations);
            
            }
            else{//not migrate
            
                
                
                allocationN.getDecisionVariables()[vM.index].setValue(vM.source);
                migrations.remove(vM);
                updateCostBenefit(migrations);
            
            }
    
        }
        
        
       */ 
        
        migrations=MinimizationOfmigration(binsTmp,allocationN);
        
        System.out.println("migrations size list"+migrations.size());
        
        SolutionSet result=MBFD(binsTmp, migrations,allocationN);
        
        //result.add(allocationN);
        
        return result; //To change body of generated methods, choose Tools | Templates.
    }
    
    
     private class IncreasingOrder implements Comparator<VM> {//compare the items for decreasing order sorting
    @Override
    public int compare( VM vm1, VM vm2) {
        if (vm1.cpu < vm2.cpu) return -1;
        if (vm1.cpu > vm2.cpu) return 1;
        return 0;
    }    
    }
    
     private class IncreasingSlopeOrder implements Comparator<PM> {//compare the items for decreasing order sorting
    @Override
    public int compare( PM pm1, PM pm2) {
        if (pm1.slope < pm2.slope) return -1;
        if (pm1.slope > pm2.slope) return 1;
        return 0;
    }    
    }
    
    private class DecreasingUtilOrder implements Comparator<VM> {//compare the items for increasing order sorting
    @Override
    public int compare(VM vm1, VM vm2) {
        if (vm1.cpuUtil < vm2.cpuUtil) return 1;
        if (vm1.cpuUtil > vm2.cpuUtil) return -1;
        return 0;
    }    
    }
     
    private class DecreasingSizeOrder implements Comparator<VM> {//compare the items for increasing order sorting
    @Override
    public int compare(VM vm1, VM vm2) {
        if (vm1.cpu < vm2.cpu) return 1;
        if (vm1.cpu > vm2.cpu) return -1;
        return 0;
    }    
    }
    
    /*pMapper only
    private class DecreasingBnefitCostOrder implements Comparator<VM> {//compare the items for increasing order sorting
    @Override
    public int compare(VM vm1, VM vm2) {
        if (vm1.ratio < vm2.ratio) return 1;
        if (vm1.ratio> vm2.ratio) return -1;
        return 0;
    }    
    }*/
    
    /*
    set the machine slope;
    This value is used to determine the pm with the least power increase per unit increase in capacity.
    see pMapper.
    */
    private double estimatePower(PM p, VM neue) throws JMException{
        
        PhysicalMachine pm=problem.getpMList().get(p.index);
        VirtualMachine vm=problem.getvMList().get(neue.index);
        
       //System.out.println("vm with index 0"+p.index);
        
       // System.out.println("vm with index 10"+neue.index);
        //double nextUsage=pm.getOverallMachineCPUUtilization();
        /* Verma states :"We pick an arbitrary traffic mix to model 
        the power for each application and use this model in the selection process
        
        "*/
         //double nextUsage=pm.getCPUUtilization(vm);
        
        double nextUsage=pm.forecastCPUUtilization(vm);
        // System.out.println("idle power"+p.idlePower );
         
        if(nextUsage>1){
        
            throw new JMException("cpu utilization >1");
        } 
        return this.powermodel.getPowerConsumption(p.idlePower, p.maxPower,nextUsage);
        //System.out.println("VM machine"+vm+"has a slope on the pm "+pm+"of = "+p.slope );
    }
    
    private void allocateVm(PM p,VM vm,Solution old) throws JMException{
    
        old.getDecisionVariables()[vm.index].setValue(p.index);
        
    }
    
    private ArrayList<VM> getlistofnewVM(ArrayList<VM> vmS,Solution plac){
    
        ArrayList<VM> res=new ArrayList();
        
        for (VM vm:vmS){
        
            if(vm.index>(plac.getDecisionVariables().length-1)){
                res.add(vm);
            } 
        }
    
        return res;
    }
    
    private Solution initNewSolution(Solution old,ArrayList<VM> vmS) throws JMException{
    
        int size=0;
        
        for (VM vm:vmS){
        
            if(vm.index>(old.getDecisionVariables().length-1)){
                size++;
            } 
        
        }
       
         System.out.println("size "+size);
         
        if(size<vmS.size()&&size>0){
        
            System.out.println("something wrong, only few of the all new VM seems to be new allocations");
        
        }
        
        Variable [] variables=new Variable[size];
        
        
        for (int i=0;i<size;i++){
        
             variables[i]=new Int(-1,
                        (int)problem.getLowerLimit(0),
                        (int)problem.getUpperLimit(0));
        }
        
        ArrayList<Variable> temp=new ArrayList(Arrays.asList(old.getDecisionVariables()));
        temp.addAll(Arrays.asList(variables));
        
        variables=temp.toArray(variables);
        
        System.out.println("old size 1= "+old.getDecisionVariables().length+"old size= "+old.numberOfVariables()+"new size = "+temp.size());
        
        Solution res=new Solution(problem, variables);
        
        return res;
    }
    
    public SolutionSet MBFD(ArrayList<PM>binsTmp,ArrayList<VM> vmS,Solution old) throws JMException{
        
        Solution Placement=initNewSolution(old, vmS);
        
       
        
        Collections.sort(vmS,new DecreasingUtilOrder());
      
         double power=0;
         double vmUtil[]={0,0},pmUtil[]={0,0};
         
        for (int i=0;i<vmS.size();i++){

            
            double minPower=Double.MAX_VALUE;
            PM allocatedHost=null;
            
            
            
                for(PM nextPM:binsTmp){
                
                 /*    vmUtil=problem.getpMList().get(nextPM.index)
                         .getCPUUtilization(problem.getvMList()
                                 .get(vmS.get(i).index));*/
                     
                    
                    vmUtil[0]=problem.getpMList().get(nextPM.index)
                         .forecastCPUUtilization(problem.getvMList()
                                 .get(vmS.get(i).index));
                    
                    vmUtil[1]=problem.getpMList().get(nextPM.index)
                         .forecastMEMUtilization(problem.getvMList()
                                 .get(vmS.get(i).index));
                    
                    
                     pmUtil=getResourcesUtilization(nextPM, Placement);
                   //  pmUtil[1]=getResourcesUtilization(nextPM, Placement)[1];
                     
                   //  System.out.println("util"+(vmUtil+pmUtil)+"total res"+(vmUtil+pmUtil)*nextPM.cpu);
                     
                if((CAP_MAX*nextPM.cpu)-((vmUtil[0]+pmUtil[0])*nextPM.cpu)>0
                        &&(((CAP_MAX*nextPM.memory)-((vmUtil[1]+pmUtil[1])*nextPM.memory))>0||IsOnlyCpu)
                          ){
                
                     power=estimatePower(nextPM,vmS.get(i));
                   
                     if(power<minPower){
                     
                         allocatedHost=nextPM;
                         minPower=power;
                         
                     }
                }
                
                
            }
                if(allocatedHost!=null){
                
                    allocateVm(allocatedHost, vmS.get(i), Placement);
                }
                else
                   throw 
               new JMException("VM"+problem.getvMList().get(vmS.get(i).index)+
                       "cannot be allocated to any server"
                       + "-there are no sufficient resources"); 
       
        }
        SolutionSet set=new SolutionSet(1);
        /*
           Solution allocationN=new Solution(problem_, variables);
           
           
           ArrayList<PM>toBeOptimize=new ArrayList<>(bins);
           ArrayList<VM>OptimizedMigrationList=MinimizationOfmigration(toBeOptimize, oldPlacement);
           System.out.println("list"+OptimizedMigrationList.size());
           System.out.println("list"+Arrays.toString(allocationN.getDecisionVariables()));
           int vmId=0;
           int cntr=0;
      
           for(Variable v :oldPlacement.getDecisionVariables()){
               
                for(VM vm:OptimizedMigrationList){
               
                   if((vmId==vm.index)){
                   
                      cntr=1; 
                   }
               
               }
              if(cntr==0){
                  
              allocationN.getDecisionVariables()[vmId].setValue(v.getValue());}
              cntr=0;
              vmId++; 
           } 
          */
           set.add(Placement);
           
           return set;
    }
        
    private double[] getResourcesUtilization(PM p,Solution pl) throws JMException{
    
        //Variable [] variables=new Variable[vMlist.size()];
       // System.out.println(problem.getNumberOfVariables());
         int pMindex=p.index;
         double [] usage={0,0};
         int vCPUallocated=0;
         ArrayList<VM> vMlist=getVMList(p, pl);
         
        for(VM vm:vMlist){           
           
          // cPUusage+=problem.getpMList().get(pMindex).getCPUUtilization(problem.getvMList().get(vm.index));
             usage[0]+=problem.getpMList().get(pMindex).forecastCPUUtilization(problem.getvMList().get(vm.index));
             usage[1]+=problem.getpMList().get(pMindex).forecastMEMUtilization(problem.getvMList().get(vm.index));
           
           //vMeMallocated[pMindex]+=pMList.get(pMindex).getMEMUtilization(vMList.get(vMindex));
           //vCPUallocated+=((problem.getvMList().get(vm.index).getVCPUs()));
           
           //TotalCpusUsage+=pMList.get(pMindex).getCPUUtilization(vMList.get(vMindex));
           //TotalMemUsage+=pMList.get(pMindex).getMEMUtilization(vMList.get(vMindex));
        }
        
         //cPUusage+=problem.getpMList().get(pMindex).getSchedulingOverhead(vCPUallocated);
         
         return usage;
    }
    
    
    
    private ArrayList<VM> getVMList(PM p,Solution Placement) throws JMException{
    
        ArrayList<VM> list=new ArrayList();
        int i=0;
        for(Variable v:Placement.getDecisionVariables()){
        
            if(v.getValue()==p.index){
                
                list.add(items.get(i));
            }
            
            i++;
        }
    
        return list;
    }
    
    private ArrayList<VM> MinimizationOfmigration(ArrayList<PM> hostList,Solution Placement) throws JMException{
    
        ArrayList<VM>vMlist=null;
        ArrayList<VM>migrationsList=new ArrayList();
        
        double hUtil=0,vmUtil=0,t=0,bestFitUtil=0;
        VM bestFitVM=null;
     for(PM pm:hostList){
     
         vMlist=getVMList(pm, Placement);
         Collections.sort(vMlist,new DecreasingUtilOrder());
         hUtil=getResourcesUtilization(pm, Placement)[0];
         
      //   System.out.println("host"+problem.getpMList().get(pm.index).getIndex()+" util"+hUtil);
         
         bestFitUtil=Double.MAX_VALUE;
         while(hUtil>THRESH_UP){
         
         
             for(VM vm:vMlist){
             
              /*  vmUtil=problem.getpMList().get(pm.index)
                         .getCPUUtilization(problem.getvMList()
                                 .get(vm.index));*/
                 
                 vmUtil=problem.getpMList().get(pm.index)
                         .forecastCPUUtilization(problem.getvMList()
                                 .get(vm.index));
                 
                 if(vmUtil>hUtil-THRESH_UP){
                 
                     t=vmUtil-hUtil+THRESH_UP;
                 
                     if(t<bestFitUtil){
                     
                         bestFitUtil=t;
                         bestFitVM=vm;
                     }
                 }
                 else if(bestFitUtil==Double.MAX_VALUE){
                 
                     bestFitVM=vm;
                     
                 }
                // break;
             }
         
            /* hUtil=hUtil-problem.getpMList().get(pm.index)
                         .getCPUUtilization(problem.getvMList()
                                 .get(bestFitVM.index));*/
             
             hUtil=hUtil-problem.getpMList().get(pm.index)
                         .forecastCPUUtilization(problem.getvMList()
                                 .get(bestFitVM.index));
             
             migrationsList.add(bestFitVM);
             vMlist.remove(bestFitVM);
                     
         }
         
         if(hUtil<THRESH_LOW){
         
             migrationsList.addAll(vMlist);
             vMlist.clear();
         }
        
        
     }
     
     CAP_MAX=THRESH_UP;
     
     return migrationsList; 
    } 
    
    
    /* obsolete code -pmapper
    private ArrayList<VM> getMigrationList (Solution old,Solution neu) throws JMException{
    
        ArrayList <VM> migrations=new ArrayList<>();
        Variable variables1[]=old.getDecisionVariables();
        Variable variables2[]=neu.getDecisionVariables();
        boolean swap=false;
       
       
       if(variables1==null||variables2==null)
           return migrations;
        
        if(variables1.length>variables2.length){
          Variable temp[]=variables2;
            variables2=variables1;
            variables1=temp;
            swap=true;
        }
         
        int i;
        for(i=0;i<variables1.length;i++){
        
            try {
                if(!(variables1[i].getValue()==variables2[i].getValue())){
                    
                    migrations.add(items.get(i));
                    if(!swap){
                       items.get(i).source=(int)variables1[i].getValue();
                       items.get(i).target=(int)variables2[i].getValue();     
                    }
                    else{
                       items.get(i).source=(int)variables2[i].getValue();
                       items.get(i).target=(int)variables1[i].getValue(); 
                    
                    }
                }
            } catch (JMException ex) {
                logger.warn("Comparator has failed to compare the two solutions");
               return null;
            }
              
        }
        
        for(;i<variables2.length;i++){
        
            migrations.add(items.get(i));
                    if(!swap){
                       items.get(i).source=-1;
                       items.get(i).target=(int)variables2[i].getValue();     
                    }
                    else{
                       items.get(i).source=-1;
                       items.get(i).target=(int)variables1[i].getValue(); 
                    
                    }
        
        }
        
        return migrations;
        
    }
    
   /* for Pmapper only (obsolete code) 
    private void updateCostBenefit(ArrayList<VM> mig){
    
        double cost;double benefit;
        
       
        for (int i=0;i<mig.size();i++){
            
           // pm=bins.get(mig.get(i).target);
            
            benefit=computeProfit(mig.get(i));
           //  System.out.println("VM machine index"+mig.get(i).index+"has an updated benefit ratio of = "+ benefit);
            if(mig.get(i).source<0)
                {
                    
                    cost=-1.0;
                    mig.get(i).cost=-1.0;
                }//the new allocations has no migration cost
            else{
                
                cost= migModel.getEnergyConsumption(problem.getvMList().get(mig.get(i).index),
                    problem.getpMList().get(mig.get(i).source), problem.getpMList().get(mig.get(i).target));
                
                cost=log(1000,cost);
                
                mig.get(i).cost=cost;
            }
         //   System.out.println("VM machine index"+mig.get(i).index+"has an updated cost ratio of = "+ cost);
            mig.get(i).ratio=benefit/cost;
          // System.out.println("VM machine index"+mig.get(i).index+"has an updated benefit/cost ratio of = "+ mig.get(i).ratio);
    }
        
    
    }
    
    private double computeProfit(VM vm){
        
        if(vm.source<0)
         return 1;
        
        PM  source=bins.get(vm.source);
        PM  target=bins.get(vm.target);
        
          
        
        PhysicalMachine pmSource=problem.getpMList().get(source.index);
        PhysicalMachine pmtarget=problem.getpMList().get(target.index);
        
        VirtualMachine vmMig=problem.getvMList().get(vm.index);
        
        double nextUsage=pmSource.getOverallMachineCPUUtilization();
        //System.out.println("nextUsage "+nextUsage);
        double sourcePWRbefore=this.powermodel.getPowerConsumption(source.idlePower, source.maxPower,nextUsage);
         
        nextUsage=nextUsage-pmSource.getCPUUtilization(vmMig);
       // System.out.println("nextUsage "+nextUsage);
        double sourcePWRafter=this.powermodel.getPowerConsumption(source.idlePower, source.maxPower,nextUsage);
       
        nextUsage=pmtarget.getOverallMachineCPUUtilization();
        double targetPWRbefore=this.powermodel.getPowerConsumption(target.idlePower, target.maxPower,nextUsage);
      //  System.out.println("nextUsage "+nextUsage);
        nextUsage=nextUsage+pmtarget.getCPUUtilization(vmMig);
        double targetPWRafter=this.powermodel.getPowerConsumption(target.idlePower, target.maxPower,nextUsage);
       
       // System.out.println("nextUsage "+nextUsage);
        double profTarget=(targetPWRafter-targetPWRbefore);
      //  System.out.println("prfT is here "+profTarget);
        double profSource=sourcePWRbefore-sourcePWRafter;
     //    System.out.println("prfS is here "+profSource);
        double powerProfit=profSource-profTarget;
       
        
        return powerProfit;
    
    } */
}
