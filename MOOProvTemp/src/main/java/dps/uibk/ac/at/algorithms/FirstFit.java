/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.ac.at.algorithms;

import dps.uibk.mooprov.iaas.PhysicalMachine;
import dps.uibk.mooprov.iaas.VirtualMachine;
import dps.uibk.mooprov.problems.MoeaProvisioning;
import dps.uibk.mooprov.problems.MoeaProvisioningII;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import jmetal.core.Algorithm;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.core.Variable;
import jmetal.encodings.variable.Int;
import jmetal.util.JMException;

/**
 *
 * @author ennio
 */
public class FirstFit extends Algorithm {
    
    private ArrayList<PM> bins;
    private ArrayList<VM> items;
    
    private class VM{

        public VM(double cpu, double memory,int index) {
            
            this.memory = memory;
            this.cpu = cpu;
            this.index=index;
        }
    
        public double memory;
        public double cpu;
        public int index;
    }
    
    private class PM{

        public PM(double cpu, double memory,int index) {
            this.memory = memory;
            this.cpu = cpu;
            this.index=index;
        }
    
        public double memory;
        public double cpu;
        public int index;
    }
    
    private class IncreasingOrder implements Comparator<VM> {//compare the items for decreasing order sorting
    @Override
    public int compare(VM vm1, VM vm2) {
        if (vm1.cpu < vm2.cpu) return -1;
        if (vm1.cpu > vm2.cpu) return 1;
        return 0;
    }    
    }
    
    private class DecreasingOrder implements Comparator<VM> {//compare the items for increasing order sorting
    @Override
    public int compare(VM vm1, VM vm2) {
        if (vm1.cpu < vm2.cpu) return 1;
        if (vm1.cpu > vm2.cpu) return -1;
        return 0;
    }    
    }
    
    
    public FirstFit Sort(String order){
    
        if(order.compareTo("decreasing")==0)
            Collections.sort(items, new DecreasingOrder());
        else if(order.compareTo("increasing")==0)
            Collections.sort(items, new IncreasingOrder());
        
        return this;
            
    }
    
    
    public FirstFit(Problem problem) {
        super(problem);
        
        if((problem instanceof MoeaProvisioning)){
        
            this.items=new ArrayList();
            this.bins=new ArrayList();
            
            List <VirtualMachine>Items=((MoeaProvisioning)problem).getvMList();
            List <PhysicalMachine>bIns=((MoeaProvisioning)problem).getpMList();
            
            VirtualMachine vm;
            PhysicalMachine pm;
            
            for (int i=0;i<Items.size();i++){
            
                vm=Items.get(i);
                this.items.add(new VM(vm.getVCPUs(),vm.getVMemory(),i));

            }
            
            for (int i=0;i<bIns.size();i++){
            
                pm=bIns.get(i);
                this.bins.add(new PM(pm.getVCPUs(),pm.getMemoryCapacity(),i));          
            
            }
        }
        
        
    }

    @Override
    public SolutionSet execute() throws JMException, ClassNotFoundException {
       
        int populationSize=1;
        SolutionSet population = new SolutionSet(populationSize);
        
        population.add(firstFit(1,0));
        return population;
    }
    
    
    
    private Solution firstFit(int countr, int jobIndex) throws ClassNotFoundException
{
 
        int theCounter = countr;
       
        Variable [] variables=new Variable[items.size()];
       
 
        do {
            if ((items.get(jobIndex).cpu > bins.get(theCounter-1).cpu)||
                    (items.get(jobIndex).memory > bins.get(theCounter-1).memory))
        {  
            theCounter += 1;
        }
        else
        { 
        System.out.println("----------------------------------");
        System.out.println("VM " + (jobIndex) + " of cpu size "  + items.get(jobIndex).cpu + " has been loaded into memory block:" + theCounter);
        System.out.println("VM " + (jobIndex) + " of memory size "  + items.get(jobIndex).memory + " has been loaded into memory block:" + theCounter);
        bins.get(theCounter-1).cpu = (bins.get(theCounter-1).cpu-items.get(jobIndex).cpu);
        bins.get(theCounter-1).memory = (bins.get(theCounter-1).memory-items.get(jobIndex).memory);
        variables[items.get(jobIndex).index]=new Int(bins.get(theCounter-1).index,
                (int)problem_.getLowerLimit(items.get(jobIndex).index),
                (int)problem_.getUpperLimit(items.get(jobIndex).index));
        
        
        System.out.println("The size of memory block " + (theCounter-1) + " is now " + bins.get(theCounter-1).memory);
        //theCounter = 1;
        jobIndex += 1;
        }
 
  }   while (theCounter <= bins.size() && jobIndex < items.size());
 
  System.out.println("----------------------------------");
  int itemsInQueue = jobIndex;
  if (itemsInQueue < items.size()) {
  System.out.println("Job " + (itemsInQueue+1) + " of size " + items.get(jobIndex).cpu + " is sent to waiting queue!");
  
  
}
 return new Solution(problem_,variables);
}
    
}
