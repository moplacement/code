/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.ac.at.Factories;

import dps.uibk.mooprov.iaas.VirtualMachine;


/**
 *
 * @author ennio
 */
public class  VmFactory {
    
    public static VirtualMachine Vm(String type){
    
        VirtualMachine vm;
        
        switch(type){
            case "extralarge":
                return new VirtualMachine(type+getindex(), type, 32, 128*Math.pow(2, 30));
            case "large":
                return new VirtualMachine(type+getindex(), type, 16, 64*Math.pow(2, 30));
            case "medium":
                return new VirtualMachine(type+getindex(), type, 8, 32*Math.pow(2, 30));
            case "small":
                return new VirtualMachine(type+getindex(), type, 2, 8*Math.pow(2, 30));
            case "extrasmall":
                return new VirtualMachine(type+getindex(), type, 1, 4*Math.pow(2, 30));
                
            default: return new VirtualMachine(type+getindex(), type, 4, 8*Math.pow(2, 30));
        }
        
       
    }
    
    public static VirtualMachine Vm(String type,String index){
    
        VirtualMachine vm;
        
        switch(type){
            case "extralarge":
                return new VirtualMachine(type+index, type, 32, 128*Math.pow(2, 30));
            case "large":
                return new VirtualMachine(type+index, type, 16, 64*Math.pow(2, 30));
            case "medium":
                return new VirtualMachine(type+index, type, 8, 32*Math.pow(2, 30));
            case "small":
                return new VirtualMachine(type+index, type, 2, 8*Math.pow(2, 30));
            case "extrasmall":
                return new VirtualMachine(type+index, type, 1, 4*Math.pow(2, 30));
                
            default: return new VirtualMachine(type+index, type, 4, 8*Math.pow(2, 30));
        }
        
       
    }
    
     private static String getindex(){
        int i=4;
        String index="";
        char s[]={'a','b','2','r','5','7','g','h','i','o','a','f','4','v','2','s','t','r','v','a','e','u','o'};
        while((i--)>0)
            index+=s[(int)((Math.random()*100)%23)];
        return index;
            
    }
}
