/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.ac.at.mOeA;

import dps.uibk.ac.at.algorithms.FirstFit;
import dps.uibk.ac.at.algorithms.ModifiedBestFitDecreasing;
import dps.uibk.mooprov.problems.MoeaProvisioning;
import jmetal.core.Algorithm;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.util.JMException;

/**
 *
 * @author ennio
 */
public class SoMBDF extends MoEAlgorithm{
    

    private final    Problem   problem   ; // The problem to solve
    private final    Algorithm algorithm ; // The algorithm to use
    
    public SoMBDF(Problem p) throws JMException {
        
        this.problem=p;
        algorithm=new ModifiedBestFitDecreasing(p);
        
    }

    
    
    @Override
    public SolutionSet getComputedPlacements() throws JMException, ClassNotFoundException {
        
        SolutionSet result=algorithm.execute();
        Solution sltn=result.get(0);
        
       // ((MoeaProvisioning)problem).setInitialSched(sltn);
        
        problem.evaluate(sltn);
        problem.evaluateConstraints(sltn);
        
        return result; //To change body of generated methods, choose Tools | Templates.
    }
  
}
