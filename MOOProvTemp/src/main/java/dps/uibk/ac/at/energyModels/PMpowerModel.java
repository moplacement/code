/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.ac.at.energyModels;

import dps.uibk.mooprov.iaas.PhysicalMachine;
import dps.uibk.mooprov.iaas.VirtualMachine;

/**
 *
 * @author ennio
 */
public class PMpowerModel {
        public double getPowerConsumption(PhysicalMachine pm){
    
    
        
       return (pm.getMachineState().equals(PhysicalMachine.State.Running)||
              pm.getMachineState().equals(PhysicalMachine.State.Shutdowning)||
              pm.getMachineState().equals(PhysicalMachine.State.Suspension))
              ?
               getPowerConsumption(pm.getIdlePWR(), pm.getMaxPWR(), pm.getOverallMachineCPUUtilization())
              //(pm.getMaxPWR()-pm.getIdlePWR())*pm.getOverallMachineCPUUtilization()+pm.getIdlePWR()
             // :
            //  pm.getMachineState().equals(PhysicalMachine.State.Suspended)?10:
               :0;
       //watt/sec
    
    }
    
    public double getPowerConsumption(double idlePwr,double MaxPwr,double utilization){
    
        return  (MaxPwr-idlePwr)*utilization+idlePwr;
    
    };
}
