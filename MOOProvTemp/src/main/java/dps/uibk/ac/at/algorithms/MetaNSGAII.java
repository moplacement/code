/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.ac.at.algorithms;


import java.util.Comparator;
import jmetal.core.Algorithm;
import jmetal.core.Problem;
import jmetal.core.SolutionSet;
import jmetal.qualityIndicator.QualityIndicator;
import jmetal.util.JMException;
import jmetal.util.Ranking;

/**
 *
 * @author ennio
 */
public class MetaNSGAII extends Algorithm{

    /**
     * @param populationSize  self explicative
     * @param maxEvaluations  number of "Meta" iterations
     * @param baseEvaluations number of base algorithms sub-iterations 
     * @param algorithms      list of base algorithms
     * @param comparators     list of comparators used to order the Pareto set produced by each of the base algorithms
     * @param problem         the problem to be solved
     */
    public MetaNSGAII(Problem problem) {
        super(problem);
    }

    @Override
    public SolutionSet execute() throws JMException, ClassNotFoundException {
    int populationSize;
    int maxEvaluations;
    int evaluations;
    int baseEvaluations;
    int algPopulationSize;

    QualityIndicator indicators; // QualityIndicator object
    int requiredEvaluations; // Use in the example of use of the
    // indicators object (see below)

    //SolutionSet population;
    //SolutionSet offspringPopulation;
    //SolutionSet union;

    //Operator mutationOperator;
    //Operator crossoverOperator;
    //Operator selectionOperator;

    //Distance distance = new Distance();
    
    BaseAlgorithm [] baseAlgorithms;
    Comparator[] objectsComparators;
    
    //Read the parameters
    populationSize = ((Integer) getInputParameter("populationSize")).intValue();
    maxEvaluations = ((Integer) getInputParameter("maxEvaluations")).intValue();
    indicators = (QualityIndicator) getInputParameter("indicators");
    baseAlgorithms=(BaseAlgorithm[]) getInputParameter("algorithms");
    objectsComparators=(Comparator []) getInputParameter("comparators");
    

    //Initialize the variables
   
    SolutionSet population = new SolutionSet(populationSize);
    
    
    baseEvaluations = ((Integer) getInputParameter("baseEvaluations")).intValue();;

    
    evaluations=0;
    
    requiredEvaluations = 0;

    //NO operators
    //mutationOperator = operators_.get("mutation");
    //crossoverOperator = operators_.get("crossover");
    //selectionOperator = operators_.get("selection");
    
    SolutionSet algPopulation=null,
            tempPopulation=new SolutionSet(populationSize),
            front=null;
    
    int currentSize=0,frontCurrentIndex=0;
    
     while (evaluations < maxEvaluations) {
     
        
        
        
        for(int algID=0;algID<baseAlgorithms.length;algID++){
            
            currentSize=0;
            
            algPopulationSize=(algID<baseAlgorithms.length-1)?
                     (populationSize/baseAlgorithms.length):
                     ((populationSize/baseAlgorithms.length)+(populationSize%baseAlgorithms.length));
            
           // System.out.println("the population size of the alg "+algID+" is "+algPopulationSize);
            
            baseAlgorithms[algID].setPopulation(population);
                        
            baseAlgorithms[algID].setInputParameter("maxEvaluations",baseEvaluations);
            
            algPopulation=baseAlgorithms[algID].execute();
            
            
            Ranking ranking = new Ranking(algPopulation);
            
            // System.out.println(algID+"has computed a population of size: "+algPopulation.size());
             // System.out.println("front"+ranking.getNumberOfSubfronts());
             
             
            for (int rank=0; rank<ranking.getNumberOfSubfronts()
                    &&algPopulationSize-currentSize>0 ;rank++){
                  // System.out.println(algID+"has computed a population of size: "+algPopulation.size());
                   front=ranking.getSubfront(rank);
                   front.sort(objectsComparators[algID]);
                   
                   frontCurrentIndex=0;
                   
               // System.out.println(algID+"adding of front"+rank+" to the final population of execution number :"+evaluations);
                while (algPopulationSize-currentSize>0&&
                        front.size()-frontCurrentIndex>0){
                
                    
                    
                    tempPopulation.add(front.get(frontCurrentIndex));
                    frontCurrentIndex++;
                    currentSize++;
                }
              //currentSize=+frontCurrentIndex;
            //  System.out.println(algID+" fronts added in iteration :"+evaluations+"population Size:"+currentSize);
            }
        }
        population.clear();
       // System.out.println("population size"+population.size());
        
       // System.out.println("tempPopulation"+tempPopulation.size());
        population=population.union(tempPopulation);
       // System.out.println("population size"+population.size());
        
        tempPopulation.clear();
      //  System.out.println("tempPopulation"+tempPopulation.size());
        evaluations+=baseEvaluations;
     }
    
       // Return the first non-dominated front
       Ranking ranking = new Ranking(population);
       ranking.getSubfront(0).printFeasibleFUN("Pareto/FUN_MetaNSGAII") ;
    
       return ranking.getSubfront(0);
    }
    
}
