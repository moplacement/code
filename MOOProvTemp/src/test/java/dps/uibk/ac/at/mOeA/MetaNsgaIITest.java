/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.ac.at.mOeA;

import dps.uibk.ac.at.algorithms.MetaNSGAII;
import dps.uibk.ac.at.algorithms.BaseAlgorithm;
import dps.uibk.ac.at.algorithms.baseNSGAII;
import dps.uibk.ac.at.energyModels.MigrationModel;
import dps.uibk.ac.at.mutation.ProvMutationFactory;
import dps.uibk.mooprov.comparator.MigrationEnergyComparator;
import dps.uibk.mooprov.crossover.ProvCrossoverFactory;
import static dps.uibk.mooprov.exec.MoScheduler.GetVMs;
import dps.uibk.mooprov.iaas.IaaS;
import dps.uibk.mooprov.iaas.PhysicalMachine;
import dps.uibk.mooprov.iaas.VirtualMachine;
import dps.uibk.mooprov.problems.MoeaProvisioning;
import dps.uibk.mooprov.schedulers.DummyScheduler;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import jmetal.core.Algorithm;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.operators.crossover.Crossover;
import jmetal.operators.mutation.Mutation;
import jmetal.operators.selection.Selection;
import jmetal.operators.selection.SelectionFactory;
import jmetal.util.JMException;
import jmetal.util.comparators.ObjectiveComparator;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ennio
 */
public class MetaNsgaIITest {
    
    private final static ArrayList<PhysicalMachine> pmList=new ArrayList<>();
    private final static ArrayList<VirtualMachine> vmList=new ArrayList<>();
    private static IaaS Iaas;
    
    public MetaNsgaIITest() {
    }

    @Test
    public void testExecute() throws Exception {
        System.out.println("execute");
        
        Iaas=new IaaS(DummyScheduler.class);
        init();
        
        System.out.println("total nuber of PMs ("+Iaas.gethOst().size()+") ");
        
        System.out.println("IaaS ("+Iaas.getgUest().size()+") VMs");
        
        Problem prob=new MoeaProvisioning("Int",vmList, pmList);
        
        
        Problem prob1=new MoeaProvisioning("Placement",vmList, pmList);
        Problem prob2=new MoeaProvisioning("PlacementII",vmList, pmList);
        
        Solution initialP=new Solution(prob);
        
        ((MoeaProvisioning)prob1).setComparator(new MigrationEnergyComparator(Iaas,new MigrationModel()));
         ((MoeaProvisioning)prob2).setComparator(new MigrationEnergyComparator(Iaas,new MigrationModel()));
        
        
        ((MoeaProvisioning)prob1).setInitialSched(initialP);
        ((MoeaProvisioning)prob2).setInitialSched(initialP);
        
        BaseAlgorithm[] algorithms=new BaseAlgorithm[2];
        
        algorithms[0]=new baseNSGAII(prob1);
        algorithms[1]=new baseNSGAII(prob2);
        
        setAlgorithm(algorithms[0]);
        setAlgorithm(algorithms[1]);
        
        ObjectiveComparator [] comparators=new ObjectiveComparator[2];
        
        comparators[0]=new ObjectiveComparator(2,true); 
        comparators[1]=new ObjectiveComparator(2,false);
                
        MetaNSGAII instance = new MetaNSGAII(prob1);
        
        instance.setInputParameter("populationSize",1000);
        instance.setInputParameter("maxEvaluations",100000);
        
        instance.setInputParameter("algorithms",algorithms);
        instance.setInputParameter("comparators",comparators);
        
         instance.setInputParameter("baseEvaluations",50000);
        
        instance.setInputParameter("indicators",null);
        //SolutionSet expResult = null;
        SolutionSet result = instance.execute();
       // assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    
     private void init() throws IOException{
    
     System.out.println("pm in list"+pmList.size());
    
     for (int i=0;i<50;i++){
        pmList.add(new PhysicalMachine());//add 3 servers with the same characterisics
     }   
       
         System.out.println("pm in list"+pmList.size());
        for(PhysicalMachine pm:pmList)
            Iaas.addHost(pm);
     
         
       vmList.addAll(GetVMs(true,1));
       
        for(VirtualMachine vm:vmList)
            Iaas.addGuest(vm);
        System.out.println("vm in list"+vmList.size());
        
         
         //vmList=GetVMs(true);
        
        //for(VirtualMachine vm:vmList)
         //   Iaas.addGuest(vm);
        
        
    }
     
     private void setAlgorithm(Algorithm algorithm) throws JMException{
         
        // Algorithm parameters
          algorithm.setInputParameter("populationSize",1000);
          algorithm.setInputParameter("maxEvaluations",100000);
          
        // algorithm.setInputParameter("archiveSize", 100);
        // Mutation and Crossover for Real codification
        HashMap parameters = new HashMap();
        
        
          parameters.put("probability", 0.9) ;
         
        //parameters.put("distributionIndex", 20.0) ;
        Crossover crossover = ProvCrossoverFactory.getCrossoverOperator("SinglePointCrossoverP", parameters);
        

         parameters = new HashMap() ;
        parameters.put("probability", 1.0/algorithm.getProblem().getNumberOfVariables()) ;
        // parameters.put("probability", 0.0001) ;
        //parameters.put("distributionIndex", 20.0) ;
        Mutation mutation = ProvMutationFactory.getMutationOperator("BitFlipMutationP", parameters);
         

    // Selection Operator 
        parameters = null ;
                                  
        Selection selection = SelectionFactory.getSelectionOperator("BinaryTournament2", parameters);
        

    // Add the operators to the algorithm
        algorithm.addOperator("crossover",crossover);
        algorithm.addOperator("mutation",mutation);
        algorithm.addOperator("selection",selection);
        Object indicators=null;

    // Add the indicator object to the algorithm
        algorithm.setInputParameter("indicators", indicators) ; 
     
     
     }
    
}
