/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.ac.at.algorithms;

import static dps.uibk.mooprov.exec.MoScheduler.GetVMs;
import dps.uibk.mooprov.iaas.IaaS;
import dps.uibk.mooprov.iaas.PhysicalMachine;
import dps.uibk.mooprov.iaas.VirtualMachine;
import dps.uibk.mooprov.problems.MoeaProvisioning;
import dps.uibk.mooprov.schedulers.DummyScheduler;
import dps.uibk.mooprov.solutionType.Placement;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ennio
 */
public class FirstFitTest {
    private final static ArrayList<PhysicalMachine> pmList=new ArrayList<>();
    private final static ArrayList<VirtualMachine> vmList=new ArrayList<>();
    private IaaS Iaas;
    
    public FirstFitTest() {
    }
    
    private void init() throws IOException{
    
     System.out.println("pm in list"+pmList.size());
    
     for (int i=0;i<50;i++){
        pmList.add(new PhysicalMachine());//add 3 servers with the same characterisics
     }   
       
         System.out.println("pm in list"+pmList.size());
        for(PhysicalMachine pm:pmList)
            Iaas.addHost(pm);
     
         
       vmList.addAll(GetVMs(true,1));
       
        for(VirtualMachine vm:vmList)
            Iaas.addGuest(vm);
        System.out.println("vm in list"+vmList.size());
        
         
         //vmList=GetVMs(true);
        
        //for(VirtualMachine vm:vmList)
         //   Iaas.addGuest(vm);
        
        
    }

    @Test
    public void testSort() {
        System.out.println("Sort");
        String order = "";
        FirstFit instance = null;
        FirstFit expResult = null;
//        FirstFit result = instance.Sort(order);
      //  assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      //  fail("The test case is a prototype.");
    }

    @Test
    public void testExecute() throws Exception {
        System.out.println("execute");
        Iaas=new IaaS(DummyScheduler.class);
        
        init();
        
        System.out.println("total nuber of PMs ("+Iaas.gethOst().size()+") ");
        
        System.out.println("IaaS ("+Iaas.getgUest().size()+") VMs");
        
      
        
        Problem prob=new MoeaProvisioning("Int",vmList, pmList);
        
        
        FirstFit instance = new FirstFit(prob).Sort("decreasing");
        
        Solution initialP=new Solution(prob);
        
        
        SolutionSet expResult = null;
        SolutionSet result = instance.execute();
        System.out.println(Arrays.toString(result.get(0).getDecisionVariables()));
        //assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
