/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.ac.at.algorithms;

import dps.uibk.ac.at.Factories.PmFactory;
import static dps.uibk.mooprov.exec.MoScheduler.GetVMs;
import dps.uibk.mooprov.iaas.IaaS;
import dps.uibk.mooprov.iaas.PhysicalMachine;
import dps.uibk.mooprov.iaas.VirtualMachine;
import dps.uibk.mooprov.problems.MoeaProvisioning;
import dps.uibk.mooprov.schedulers.DummyScheduler;
import hu.mta.sztaki.lpds.cloud.simulator.Timed;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ennio
 */
public class MBDFTest {
    
    private final static ArrayList<PhysicalMachine> pmList=new ArrayList<>();
    private final static ArrayList<VirtualMachine> vmList=new ArrayList<>();
    private IaaS Iaas;
    
    public MBDFTest() {
    }

    
     private void init() throws IOException{
    
     System.out.println("pm in list"+pmList.size());
    
     for (int i=0;i<5;i++){
       pmList.add(PmFactory.Pm("Opteron-8356"));//add 3 servers with the same characterisics
    } 
    // pmList.add(PmFactory.Pm("Xeon-E-2690"));
    // pmList.add(PmFactory.Pm("Xeon-E-2690"));
       /*
         pmList.add(PmFactory.Pm("Xeon-E-2690"));//add 3 servers with the same characterisics
         pmList.add(PmFactory.Pm("Opteron-8356"));
         pmList.add(PmFactory.Pm("Xeon-E-2690"));
         pmList.add(PmFactory.Pm("Opteron-8356"));
         pmList.add(PmFactory.Pm("Opteron-8356"));
         pmList.add(PmFactory.Pm("Xeon-E-2690"));
         pmList.add(PmFactory.Pm("Opteron-8356"));
         pmList.add(PmFactory.Pm("Xeon-E-2690"));
         pmList.add(PmFactory.Pm("Opteron-8356"));
         pmList.add(PmFactory.Pm("Opteron-8356"));
         pmList.add(PmFactory.Pm("Xeon-E-2690"));
         pmList.add(PmFactory.Pm("Xeon-E-2690"));
         pmList.add(PmFactory.Pm("Opteron-8356"));
         pmList.add(PmFactory.Pm("Opteron-8356"));
         pmList.add(PmFactory.Pm("Xeon-E-2690"));
       */  
        System.out.println("pm in list"+pmList.size());
        for(PhysicalMachine pm:pmList)
            Iaas.addHost(pm);
     
         
       vmList.addAll(GetVMs(true,1));
       
        for(VirtualMachine vm:vmList)
            Iaas.addGuest(vm);
      
        System.out.println("vm in list"+vmList.size());
        
        System.out.println("vm with index 0"+vmList.get(0));
        
        System.out.println("vm with index 10"+vmList.get(10));
         
         //vmList=GetVMs(true);
        
        //for(VirtualMachine vm:vmList)
         //   Iaas.addGuest(vm);
        
        
    }
    
    @Test
    public void testExecute() throws Exception {
        System.out.println("execute");
        
        
         Iaas=new IaaS(DummyScheduler.class);
        
        init();
        
        System.out.println("total nuber of PMs ("+Iaas.gethOst().size()+") ");
        
        System.out.println("IaaS ("+Iaas.getgUest().size()+") VMs");
        
      
       
        
        
        Problem prob=new MoeaProvisioning("Int",vmList, pmList);
        Solution initialP=new Solution(new MoeaProvisioning("Int",vmList, pmList));
        
        ((MoeaProvisioning)prob).setInitialSched(initialP);
        
        Iaas.ApplyPlacement(initialP);
        Iaas.setPlacement(initialP);
        
         Timed.simulateUntil(60);
        
        ModifiedBestFitDecreasing instance = new ModifiedBestFitDecreasing(prob);
        
        SolutionSet expResult = instance.execute();
        
        
        
        System.out.println(Arrays.toString(expResult.get(0).getDecisionVariables()));
        System.out.println(Arrays.toString(initialP.getDecisionVariables()));
    
    }
    
}
