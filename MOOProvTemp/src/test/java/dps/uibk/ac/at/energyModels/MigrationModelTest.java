/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.ac.at.energyModels;

import dps.uibk.ac.at.Factories.PmFactory;
import dps.uibk.ac.at.Factories.VmFactory;
import dps.uibk.mooprov.iaas.PhysicalMachine;
import dps.uibk.mooprov.iaas.VirtualMachine;
import dps.uibk.mooprov.workload.GaussianGenerator;
import dps.uibk.mooprov.workload.Workload;
import hu.mta.sztaki.lpds.cloud.simulator.Timed;
import java.util.Collections;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ennio
 */
public class MigrationModelTest {
    
    public MigrationModelTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetPowerConsumption() {
        
        System.out.println("getPowerConsumption");
        
        Timed.resetTimed();
        
        VirtualMachine vm = VmFactory.Vm("small");
        Workload w=new GaussianGenerator();
        vm.setWorkload(w);
        
        
        PhysicalMachine source = PmFactory.Pm("Xeon-E-2690");
        PhysicalMachine target = PmFactory.Pm("Xeon-E-2690");
        
        source.allocateVMs(Collections.singletonList(vm));
        target.allocateVMs(Collections.EMPTY_LIST);
        
        double time = 0.0;
        MigrationModel instance = new MigrationModel();
        double expResult = 0.0;
        Timed.simulateUntil(10);
        double result = instance.getPowerConsumption(vm, source, target);
        
        
        System.out.println("total Power consumption"+result);
        //assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    @Test
    public void testGetEnergyConsumption() throws Exception {
        System.out.println("getEnergyConsumption");
        
        Timed.resetTimed();
        
        VirtualMachine vm = VmFactory.Vm("small");
        Workload w=new GaussianGenerator(0.6, 0.5, 0.7, 0.5); //high memory load;
        vm.setWorkload(w);
        
        
        PhysicalMachine source = PmFactory.Pm("Opteron-8356");
        PhysicalMachine target = PmFactory.Pm("Opteron-8356");
        
        source.allocateVMs(Collections.singletonList(vm));
        target.allocateVMs(Collections.EMPTY_LIST);
        
        MigrationModel instance = new MigrationModel();
        
        
        
        double expResult = 0.0;
        Timed.simulateUntil(10);
        double result = instance.getEnergyConsumption(vm, source, target);
         System.out.println("total Energy consumption"+result);
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }
    
}
