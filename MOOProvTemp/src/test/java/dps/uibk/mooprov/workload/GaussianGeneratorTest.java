/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.workload;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ennio
 */
public class GaussianGeneratorTest {
    
    public GaussianGeneratorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of UpdateMemoryDemand method, of class GaussianGenerator.
     */
    @Test
    public void testCPUDemand() {
        System.out.println("getCPUDemand");
        double expAVG=0.9;
        double expError=0.12;
        GaussianGenerator instance = new GaussianGenerator(0.9,0.3,expAVG,0.2);
        double expResult = 0.6;
        double result=0,resultTemp=0;
        int counter=0;
        while ((++counter)<10000){
        //System.out.println(resultTemp);
        resultTemp = instance.getCPUDemand();
        result+=resultTemp;
        }
        
        assertEquals("The average cpu demand differs by the expected ",expAVG,(result/counter),expAVG*expError);
    }

    /**
     * Test of getMemoryDemand method, of class GaussianGenerator.
     */
    @Test
    public void testGetMemoryDemand() {
        System.out.println("getMemoryDemand");
        double expAVG = 0.6;
        double expError=0.1;
        GaussianGenerator instance = new GaussianGenerator(expAVG,0.2,512,128);
        double result=0,resultTemp=0;
        int counter=0;
        while ((++counter)<10000){
        //System.out.println(resultTemp);
        resultTemp = instance.getMemoryDemand();
        result+=resultTemp;
        }
        
        assertEquals("The average memory demand differs by the expected ",expAVG,(result/counter),expAVG*expError);
        
    }
    
}
