/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dps.uibk.mooprov.workload;

import dps.uibk.ac.at.Factories.PmFactory;
import dps.uibk.ac.at.Factories.VmFactory;
import dps.uibk.mooprov.iaas.PhysicalMachine;
import dps.uibk.mooprov.iaas.VirtualMachine;
import dps.uibk.mooprov.meters.PerformanceMeter;
import dps.uibk.mooprov.workload.Rect.UserDefinedFunction;
import hu.mta.sztaki.lpds.cloud.simulator.Timed;
import java.util.Collections;
import org.junit.Test;


/**
 *
 * @author ennio
 */
public class RectTest {
    
    public RectTest() {
        
        
    }

    @Test
    public void testRectDemand() {
        System.out.println("getMemoryDemand");
        Rect instance = new Rect(50,0.01,0.01);
        VirtualMachine vm=VmFactory.Vm("medium");
        vm.setWorkload(instance);
        PhysicalMachine pm=PmFactory.Pm("");
        pm.allocateVMs(Collections.singletonList(vm));
        //PerformanceMeter meter1=new PerformanceMeter(vm,7);
       // meter1.runSlaMeter(5);
        double expResult = 0.0;
        double result = instance.getMemoryDemand();
        //Timed.simulateUntil(500);
       // assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

   
    
    @Test
    public void testRectDemandWithUserDefinedFunction() {
        System.out.println("getMemoryDemandWithUserDefinedFunction");
         
         Rect.UserDefinedFunction func=new UserDefinedFunction(){

            @Override
            public double function(double time) {
               double codom=(1D/500)*time;
              return (codom)<1?codom:1 ; //To change body of generated methods, choose Tools | Templates.
            }
        };
         
       
        Rect instance = new Rect(50,func,0.01,0.01);
        VirtualMachine vm=VmFactory.Vm("medium");
        vm.setWorkload(instance);
        PhysicalMachine pm=PmFactory.Pm("");
        pm.allocateVMs(Collections.singletonList(vm));
        PerformanceMeter meter1=new PerformanceMeter(vm,7);
        
        meter1.runSlaMeter(5);
        double result = instance.getMemoryDemand();
          Timed.simulateUntil(500);
       // assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }

    
    
    
}
